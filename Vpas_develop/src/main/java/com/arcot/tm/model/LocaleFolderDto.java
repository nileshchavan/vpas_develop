package com.arcot.tm.model;

public class LocaleFolderDto {

	private Integer localeId;
	private Integer folderId;
	
	
	public LocaleFolderDto(Integer localeId, Integer folderId) {
		this.localeId = localeId;
		this.folderId = folderId;
	}
	/**
	 * @return the localeId
	 */
	public Integer getLocaleId() {
		return localeId;
	}
	/**
	 * @param localeId the localeId to set
	 */
	public void setLocaleId(Integer localeId) {
		this.localeId = localeId;
	}
	/**
	 * @return the folderId
	 */
	public Integer getFolderId() {
		return folderId;
	}
	/**
	 * @param folderId the folderId to set
	 */
	public void setFolderId(Integer folderId) {
		this.folderId = folderId;
	}
	
	
}
