package com.arcot.tm.model;

import java.sql.Date;
import java.util.List;

import com.arcot.tm.entities.ArAdminBankPk;
import com.arcot.tm.entities.ArLocale;

public class ArBankInfoDto {
	
	
	private String  bankName;
	private String  bankDirName;
	private String userEncoding;
	private String cvvDesKeyA;
	private String cvvDesKeyB;
	private String cvvKeyInd;
	private Integer doNotPromptBehavior;
	private Integer country;
	private Date dateCreated;
	private String issuerDisplayName;
	private Integer localeId;
	private String processorData;
	private String processorInfo;
	private String processorName;
	private String subProcessorName;
	private String reportTimeZoneLocal;
	private String customerId;
	private List<ArLocale> selectedLocaleList;
	private String  bankKey;
	private Integer twoStepLoginEnabled;
	private Integer dualAuthRequiredAtXCommit;
	private List<Admins> selectedAdminList;
	private String  uploadKey;
	private AdminDto adminDto;
	
	private ArAdminBankPk adminBankPk;
	
	
	/**
	 * @return the adminDto
	 */
	public AdminDto getAdminDto() {
		return adminDto;
	}
	/**
	 * @param adminDto the adminDto to set
	 */
	public void setAdminDto(AdminDto adminDto) {
		this.adminDto = adminDto;
	}
	/**
	 * @return the selectedAdminList
	 */
	public List<Admins> getSelectedAdminList() {
		return selectedAdminList;
	}
	/**
	 * @return the bankKey
	 */
	public String getBankKey() {
		return bankKey;
	}
	/**
	 * @param bankKey the bankKey to set
	 */
	public void setBankKey(String bankKey) {
		this.bankKey = bankKey;
	}
	/**
	 * @return the uploadKey
	 */
	public String getUploadKey() {
		return uploadKey;
	}
	/**
	 * @param uploadKey the uploadKey to set
	 */
	public void setUploadKey(String uploadKey) {
		this.uploadKey = uploadKey;
	}
	/**
	 * @param selectedAdminList the selectedAdminList to set
	 */
	public void setSelectedAdminList(List<Admins> selectedAdminList) {
		this.selectedAdminList = selectedAdminList;
	}

	/**
	 * @return the bankName
	 */
	public String getBankName() {
		return bankName;
	}
	/**
	 * @param bankName the bankName to set
	 */
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	/**
	 * @return the country
	 */
	public Integer getCountry() {
		return country;
	}
	/**
	 * @param country the country to set
	 */
	public void setCountry(Integer country) {
		this.country = country;
	}
	/**
	 * @return the dateCreated
	 */
	public Date getDateCreated() {
		return dateCreated;
	}
	/**
	 * @param dateCreated the dateCreated to set
	 */
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	/**
	 * @return the bankDirName
	 */
	public String getBankDirName() {
		return bankDirName;
	}
	/**
	 * @param bankDirName the bankDirName to set
	 */
	public void setBankDirName(String bankDirName) {
		this.bankDirName = bankDirName;
	}
	/**
	 * @return the userEncoding
	 */
	public String getUserEncoding() {
		return userEncoding;
	}
	/**
	 * @param userEncoding the userEncoding to set
	 */
	public void setUserEncoding(String userEncoding) {
		this.userEncoding = userEncoding;
	}
	/**
	 * @return the cvvDesKeyA
	 */
	public String getCvvDesKeyA() {
		return cvvDesKeyA;
	}
	/**
	 * @param cvvDesKeyA the cvvDesKeyA to set
	 */
	public void setCvvDesKeyA(String cvvDesKeyA) {
		this.cvvDesKeyA = cvvDesKeyA;
	}
	/**
	 * @return the cvvDesKeyB
	 */
	public String getCvvDesKeyB() {
		return cvvDesKeyB;
	}
	/**
	 * @param cvvDesKeyB the cvvDesKeyB to set
	 */
	public void setCvvDesKeyB(String cvvDesKeyB) {
		this.cvvDesKeyB = cvvDesKeyB;
	}
	/**
	 * @return the cvvKeyInd
	 */
	public String getCvvKeyInd() {
		return cvvKeyInd;
	}
	/**
	 * @param cvvKeyInd the cvvKeyInd to set
	 */
	public void setCvvKeyInd(String cvvKeyInd) {
		this.cvvKeyInd = cvvKeyInd;
	}
	/**
	 * @return the doNotPromptBehavior
	 */
	public Integer getDoNotPromptBehavior() {
		return doNotPromptBehavior;
	}
	/**
	 * @param doNotPromptBehavior the doNotPromptBehavior to set
	 */
	public void setDoNotPromptBehavior(Integer doNotPromptBehavior) {
		this.doNotPromptBehavior = doNotPromptBehavior;
	}
	/**
	 * @return the reportTimeZoneLocal
	 */
	public String getReportTimeZoneLocal() {
		return reportTimeZoneLocal;
	}
	/**
	 * @param reportTimeZoneLocal the reportTimeZoneLocal to set
	 */
	public void setReportTimeZoneLocal(String reportTimeZoneLocal) {
		this.reportTimeZoneLocal = reportTimeZoneLocal;
	}
	/**
	 * @return the customerId
	 */
	public String getCustomerId() {
		return customerId;
	}
	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	/**
	 * @return the selectedLocaleList
	 */
	public List<ArLocale> getSelectedLocaleList() {
		return selectedLocaleList;
	}
	/**
	 * @param selectedLocaleList the selectedLocaleList to set
	 */
	public void setSelectedLocaleList(List<ArLocale> selectedLocaleList) {
		this.selectedLocaleList = selectedLocaleList;
	}
	/**
	 * @return the twoStepLoginEnabled
	 */
	public Integer getTwoStepLoginEnabled() {
		return twoStepLoginEnabled;
	}
	/**
	 * @param twoStepLoginEnabled the twoStepLoginEnabled to set
	 */
	public void setTwoStepLoginEnabled(Integer twoStepLoginEnabled) {
		this.twoStepLoginEnabled = twoStepLoginEnabled;
	}
	/**
	 * @return the dualAuthRequiredAtXCommit
	 */
	public Integer getDualAuthRequiredAtXCommit() {
		return dualAuthRequiredAtXCommit;
	}
	/**
	 * @param dualAuthRequiredAtXCommit the dualAuthRequiredAtXCommit to set
	 */
	public void setDualAuthRequiredAtXCommit(Integer dualAuthRequiredAtXCommit) {
		this.dualAuthRequiredAtXCommit = dualAuthRequiredAtXCommit;
	}
	
	/**
	 * @return the issuerDisplayName
	 */
	public String getIssuerDisplayName() {
		return issuerDisplayName;
	}
	/**
	 * @param issuerDisplayName the issuerDisplayName to set
	 */
	public void setIssuerDisplayName(String issuerDisplayName) {
		this.issuerDisplayName = issuerDisplayName;
	}
	/**
	 * @return the localeId
	 */
	public Integer getLocaleId() {
		return localeId;
	}
	/**
	 * @param localeId the localeId to set
	 */
	public void setLocaleId(Integer localeId) {
		this.localeId = localeId;
	}
	/**
	 * @return the processorData
	 */
	public String getProcessorData() {
		return processorData;
	}
	/**
	 * @param processorData the processorData to set
	 */
	public void setProcessorData(String processorData) {
		this.processorData = processorData;
	}
	/**
	 * @return the processorInfo
	 */
	public String getProcessorInfo() {
		return processorInfo;
	}
	/**
	 * @param processorInfo the processorInfo to set
	 */
	public void setProcessorInfo(String processorInfo) {
		this.processorInfo = processorInfo;
	}
	/**
	 * @return the processorName
	 */
	public String getProcessorName() {
		return processorName;
	}
	/**
	 * @param processorName the processorName to set
	 */
	public void setProcessorName(String processorName) {
		this.processorName = processorName;
	}
	/**
	 * @return the subProcessorName
	 */
	public String getSubProcessorName() {
		return subProcessorName;
	}
	/**
	 * @param subProcessorName the subProcessorName to set
	 */
	public void setSubProcessorName(String subProcessorName) {
		this.subProcessorName = subProcessorName;
	}
	public ArAdminBankPk getAdminBankPk() {
		return adminBankPk;
	}
	public void setAdminBankPk(ArAdminBankPk adminBankPk) {
		this.adminBankPk = adminBankPk;
	}
	

}
