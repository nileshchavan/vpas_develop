package com.arcot.tm.model;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.arcot.tm.entities.ArAdminLevel;
import com.arcot.tm.repository.ArAdminLevelRepository;

public class AdminOperations {

	@Autowired
	private static ArAdminLevelRepository arAdminLevelRepository;
	
	private static AdminLevels _adminLevelCache;

	public static void setAdminLevelCache(AdminLevels adminLevelCache) {
		AdminOperations._adminLevelCache = adminLevelCache;
	}

	public static int  populateAdminLevels(AdminLevels al) {

		
		int result = 1;
		AdminLevelParams alp = new AdminLevelParams();
		
		List<ArAdminLevel> arAdminLevelList = (List<ArAdminLevel>) arAdminLevelRepository.findAll();

		for(ArAdminLevel arAdminLevel : arAdminLevelList) {
			
		
			alp.AdminLevel = arAdminLevel.getArAdminLevelPk().getAdminLevel();
			alp.BankId = arAdminLevel.getArAdminLevelPk().getBankId();
			alp.MaxTriesPerSession = arAdminLevel.getMaxTriesAcrossSession();
			alp.MaxTriesAcrossSession = arAdminLevel.getMaxTriesPerSession();
			alp.PasswordRenewalFrequency = arAdminLevel.getPasswordChangeVELOCITY();
			alp.PasswordUnlockDuration = arAdminLevel.getPasswordUnlockDuration();
			alp.MaxInactivityPeriod = arAdminLevel.getMaxInActivityPeriod();
			alp.AllowPasswordReset = arAdminLevel.getAllowPasswordReset();
			alp.parseParams(arAdminLevel.getPasswordParams());
			alp.loginSessionTimeout = arAdminLevel.getLoginSessionTimeOut();
			alp.setPasswordStorageAlgorithm(arAdminLevel.getPasswordStorageAlgorithm());
			alp.setRetainOldFormatPassword(arAdminLevel.getRetainOldFormatPassword());
			alp.setCollectAdminContactDetails(arAdminLevel.getCollectAdminContactDetails());
			alp.passwordChangeVelocity = arAdminLevel.getPasswordChangeVELOCITY();
			alp.setTwoFactorDeliveryChannelType(arAdminLevel.getDeliveryChannel());
			
		
		}
		return result;
	}
	
	
}
