package com.arcot.tm.model;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;

public class AdminLevels
{
	private Hashtable levels = null;
	public AdminLevels()
	{
		levels = new Hashtable();
	}
	public void addLevel(int level, Hashtable adminLevel)
	{
		levels.put(String.valueOf(level), adminLevel);
	}
	public Hashtable getLevelBanks(int level)
	{
		return (Hashtable)levels.get(String.valueOf(level));
	}
	public void addLevelParamObject(AdminLevelParams alp)
	{
		Hashtable levelparams = (Hashtable)levels.get(String.valueOf(alp.AdminLevel));
		if ( levelparams == null )
			levelparams = new Hashtable();
		levelparams.put(String.valueOf(alp.BankId), alp);
		levels.put(String.valueOf(alp.AdminLevel), levelparams);

	}
	
	public void removeAllLevelParamObjects()
	{
		levels.clear();
	}
	public AdminLevelParams getLevelParamObject(int level, int bankId)
	{
		Hashtable levelBanks = getLevelBanks(level);

		if ( levelBanks == null )
			return null;

		AdminLevelParams alp = (AdminLevelParams)levelBanks.get(String.valueOf(bankId));
		return alp;
	}
	public int [] getLevels()
	{
		Set set = levels.keySet();
		int size = set.size();
		int [] levels = new int[size];
		Iterator iter = set.iterator();
		int ctr = 0;
		while ( iter.hasNext() )
		{
			int level = Integer.parseInt((String)iter.next());
			levels[ctr++] = level;
		}
		return levels;
	}

}