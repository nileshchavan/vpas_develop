package com.arcot.tm.model;

import java.sql.Date;

import javax.persistence.Column;

import com.arcot.tm.entities.ArBrandInfoPk;


public class ArBranddto {

	private ArBrandInfoPk arBrandInfoPk;

	private Integer rangeId;

	private String chipKeyIn;
	
	private Integer eAccessRequired;
	
	private String acsurL1;
	
	private Integer enablesCa;
	
	private Integer enrollOptions;
	
	private String hsmVariant;
	
	private String cvvDesKeyB;
	
	private Integer maxDecline;
	
	private Integer maxWelcome;
	
	private Integer trustedBenificiary;
	
	private String cvvDesKeyA;
	
	private String acsurL2;
	
	private String acsurL3;
	
	private String acsurL4;
	
	private String acsurL5;
	
	private String receiptURL;
	
	private Integer authMethod;
	
	private Integer authFallBack;
	
	private Integer arqcReequired;
	
	private Integer enableAttemptsAfterMaxDeclines;
	
	private Integer configId;
	
	private String fiBin;
	
	private Integer maxAuthorities;
	
	private Integer mobileEnabled;
	
	private String fiBusinessId;
	
	private Integer authselectedAllowed;
	
	private Integer  binkKeyId;
	
	private Integer chPasswordExpiryDuration;
	
	private String filtermPolicyVersion;

	private String cardtype;

	private String pliginName;

	private Integer nstrikesAcrossSessions;

	private Integer panLength;

	private Integer status;

	private Date dateCreated;
	
	private String receiptUrl2;
	
	private String cardRangeName;
	
	private String pluginName;
	
	private String pluginUrl;
	
	private String authResultKeyIn;
	
	private String aavAlgorithm;
	
	private String brandInGurl1;
	
	private String brandInGurl2;
	
	private String signingKeyIn;
	
	private String pluginVersion;
	
	
	public Integer geteAccessRequired() {
		return eAccessRequired;
	}

	public void seteAccessRequired(Integer eAccessRequired) {
		this.eAccessRequired = eAccessRequired;
	}

	public Integer getEnablesCa() {
		return enablesCa;
	}

	public void setEnablesCa(Integer enablesCa) {
		this.enablesCa = enablesCa;
	}

	public Integer getEnrollOptions() {
		return enrollOptions;
	}

	public void setEnrollOptions(Integer enrollOptions) {
		this.enrollOptions = enrollOptions;
	}

	public String getHsmVariant() {
		return hsmVariant;
	}

	public void setHsmVariant(String hsmVariant) {
		this.hsmVariant = hsmVariant;
	}

	public String getCvvDesKeyB() {
		return cvvDesKeyB;
	}

	public void setCvvDesKeyB(String cvvDesKeyB) {
		this.cvvDesKeyB = cvvDesKeyB;
	}

	public Integer getMaxDecline() {
		return maxDecline;
	}

	public void setMaxDecline(Integer maxDecline) {
		this.maxDecline = maxDecline;
	}

	public Integer getMaxWelcome() {
		return maxWelcome;
	}

	public void setMaxWelcome(Integer maxWelcome) {
		this.maxWelcome = maxWelcome;
	}

	public Integer getTrustedBenificiary() {
		return trustedBenificiary;
	}

	public void setTrustedBenificiary(Integer trustedBenificiary) {
		this.trustedBenificiary = trustedBenificiary;
	}

	public String getCvvDesKeyA() {
		return cvvDesKeyA;
	}

	public void setCvvDesKeyA(String cvvDesKeyA) {
		this.cvvDesKeyA = cvvDesKeyA;
	}

	public String getAcsurL2() {
		return acsurL2;
	}

	public void setAcsurL2(String acsurL2) {
		this.acsurL2 = acsurL2;
	}

	public String getAcsurL3() {
		return acsurL3;
	}

	public void setAcsurL3(String acsurL3) {
		this.acsurL3 = acsurL3;
	}

	public String getAcsurL4() {
		return acsurL4;
	}

	public void setAcsurL4(String acsurL4) {
		this.acsurL4 = acsurL4;
	}

	public String getAcsurL5() {
		return acsurL5;
	}

	public void setAcsurL5(String acsurL5) {
		this.acsurL5 = acsurL5;
	}

	public String getReceiptURL() {
		return receiptURL;
	}

	public void setReceiptURL(String receiptURL) {
		this.receiptURL = receiptURL;
	}

	public Integer getAuthMethod() {
		return authMethod;
	}

	public void setAuthMethod(Integer authMethod) {
		this.authMethod = authMethod;
	}

	public Integer getAuthFallBack() {
		return authFallBack;
	}

	public void setAuthFallBack(Integer authFallBack) {
		this.authFallBack = authFallBack;
	}

	public String getAuthResultKeyIn() {
		return authResultKeyIn;
	}

	public void setAuthResultKeyIn(String authResultKeyIn) {
		this.authResultKeyIn = authResultKeyIn;
	}

	public String getAavAlgorithm() {
		return aavAlgorithm;
	}

	public void setAavAlgorithm(String aavAlgorithm) {
		this.aavAlgorithm = aavAlgorithm;
	}

	public String getBrandInGurl1() {
		return brandInGurl1;
	}

	public void setBrandInGurl1(String brandInGurl1) {
		this.brandInGurl1 = brandInGurl1;
	}

	public String getBrandInGurl2() {
		return brandInGurl2;
	}

	public void setBrandInGurl2(String brandInGurl2) {
		this.brandInGurl2 = brandInGurl2;
	}

	public String getPluginName() {
		return pluginName;
	}

	public void setPluginName(String pluginName) {
		this.pluginName = pluginName;
	}

	public String getPluginUrl() {
		return pluginUrl;
	}

	public void setPluginUrl(String pluginUrl) {
		this.pluginUrl = pluginUrl;
	}

	public String getPluginVersion() {
		return pluginVersion;
	}

	public void setPluginVersion(String pluginVersion) {
		this.pluginVersion = pluginVersion;
	}

	private byte[] signingCertificateFileArray;

	public ArBrandInfoPk getArBrandInfoPk() {
		return arBrandInfoPk;
	}

	public void setArBrandInfoPk(ArBrandInfoPk arBrandInfoPk) {
		this.arBrandInfoPk = arBrandInfoPk;
	}

	public String getAcsurL1() {
		return acsurL1;
	}

	public void setAcsurL1(String acsurL1) {
		this.acsurL1 = acsurL1;
	}

	public Integer getArqcReequired() {
		return arqcReequired;
	}

	public void setArqcReequired(Integer arqcReequired) {
		this.arqcReequired = arqcReequired;
	}

	public Integer getEnableAttemptsAfterMaxDeclines() {
		return enableAttemptsAfterMaxDeclines;
	}

	public void setEnableAttemptsAfterMaxDeclines(Integer enableAttemptsAfterMaxDeclines) {
		this.enableAttemptsAfterMaxDeclines = enableAttemptsAfterMaxDeclines;
	}

	public Integer getConfigId() {
		return configId;
	}

	public void setConfigId(Integer configId) {
		this.configId = configId;
	}

	public String getFiBin() {
		return fiBin;
	}

	public void setFiBin(String fiBin) {
		this.fiBin = fiBin;
	}

	public Integer getMaxAuthorities() {
		return maxAuthorities;
	}

	public void setMaxAuthorities(Integer maxAuthorities) {
		this.maxAuthorities = maxAuthorities;
	}

	public Integer getMobileEnabled() {
		return mobileEnabled;
	}

	public void setMobileEnabled(Integer mobileEnabled) {
		this.mobileEnabled = mobileEnabled;
	}


	public String getFiBusinessId() {
		return fiBusinessId;
	}

	public void setFiBusinessId(String fiBusinessId) {
		this.fiBusinessId = fiBusinessId;
	}

	public Integer getAuthselectedAllowed() {
		return authselectedAllowed;
	}

	public void setAuthselectedAllowed(Integer authselectedAllowed) {
		this.authselectedAllowed = authselectedAllowed;
	}

	public Integer getBinkKeyId() {
		return binkKeyId;
	}

	public void setBinkKeyId(Integer binkKeyId) {
		this.binkKeyId = binkKeyId;
	}

	public Integer getChPasswordExpiryDuration() {
		return chPasswordExpiryDuration;
	}

	public void setChPasswordExpiryDuration(Integer chPasswordExpiryDuration) {
		this.chPasswordExpiryDuration = chPasswordExpiryDuration;
	}

	public String getFiltermPolicyVersion() {
		return filtermPolicyVersion;
	}

	public void setFiltermPolicyVersion(String filtermPolicyVersion) {
		this.filtermPolicyVersion = filtermPolicyVersion;
	}

	public String getCardtype() {
		return cardtype;
	}

	public void setCardtype(String cardtype) {
		this.cardtype = cardtype;
	}

	public String getPliginName() {
		return pliginName;
	}

	public void setPliginName(String pliginName) {
		this.pliginName = pliginName;
	}

	public Integer getNstrikesAcrossSessions() {
		return nstrikesAcrossSessions;
	}

	public void setNstrikesAcrossSessions(Integer nstrikesAcrossSessions) {
		this.nstrikesAcrossSessions = nstrikesAcrossSessions;
	}

	public Integer getPanLength() {
		return panLength;
	}

	public void setPanLength(Integer panLength) {
		this.panLength = panLength;
	}



	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getReceiptUrl2() {
		return receiptUrl2;
	}

	public void setReceiptUrl2(String receiptUrl2) {
		this.receiptUrl2 = receiptUrl2;
	}

	public String getCardRangeName() {
		return cardRangeName;
	}

	public void setCardRangeName(String cardRangeName) {
		this.cardRangeName = cardRangeName;
	}

	public byte[] getSigningCertificateFileArray() {
		return signingCertificateFileArray;
	}

	public void setSigningCertificateFileArray(byte[] signingCertificateFileArray) {
		this.signingCertificateFileArray = signingCertificateFileArray;
	}

	public String getChipKeyIn() {
		return chipKeyIn;
	}

	public void setChipKeyIn(String chipKeyIn) {
		this.chipKeyIn = chipKeyIn;
	}

	public String getSigningKeyIn() {
		return signingKeyIn;
	}

	public void setSigningKeyIn(String signingKeyIn) {
		this.signingKeyIn = signingKeyIn;
	}

	public Integer getRangeId() {
		return rangeId;
	}

	public void setRangeId(Integer rangeId) {
		this.rangeId = rangeId;
	}
	


}
