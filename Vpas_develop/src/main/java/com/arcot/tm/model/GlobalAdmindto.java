package com.arcot.tm.model;

import java.util.List;

import com.arcot.tm.entities.ArAdminCountry;
import com.arcot.tm.entities.ArBankInfo;

public class GlobalAdmindto {

	private String name;
	
	private String firstName;
	
	private String description;
	
	private String password;
	
	private String rePassword;
	
	private int maxValForLastPwdList;
	
	private List<ArBankInfo> selectedIssuersList;
	
	private List<ArAdminCountry> selectedCountriesList;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the rePassword
	 */
	public String getRePassword() {
		return rePassword;
	}

	/**
	 * @param rePassword the rePassword to set
	 */
	public void setRePassword(String rePassword) {
		this.rePassword = rePassword;
	}

	/**
	 * @return the maxValForLastPwdList
	 */
	public int getMaxValForLastPwdList() {
		return maxValForLastPwdList;
	}

	/**
	 * @param maxValForLastPwdList the maxValForLastPwdList to set
	 */
	public void setMaxValForLastPwdList(int maxValForLastPwdList) {
		this.maxValForLastPwdList = maxValForLastPwdList;
	}

	/**
	 * @return the selectedIssuersList
	 */
	public List<ArBankInfo> getSelectedIssuersList() {
		return selectedIssuersList;
	}

	/**
	 * @param selectedIssuersList the selectedIssuersList to set
	 */
	public void setSelectedIssuersList(List<ArBankInfo> selectedIssuersList) {
		this.selectedIssuersList = selectedIssuersList;
	}

	/**
	 * @return the selectedCountriesList
	 */
	public List<ArAdminCountry> getSelectedCountriesList() {
		return selectedCountriesList;
	}

	/**
	 * @param selectedCountriesList the selectedCountriesList to set
	 */
	public void setSelectedCountriesList(List<ArAdminCountry> selectedCountriesList) {
		this.selectedCountriesList = selectedCountriesList;
	}
	
	//add list of privilages for global and issuer

}
