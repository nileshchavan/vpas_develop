package com.arcot.tm.model;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.StringTokenizer;

public class AdminLevelParams
{
	public static int LOGIN_SESSION_TIMEOUT_MIN = 0;
	public static int LOGIN_SESSION_TIMEOUT_DEFAULT = 0;
	public static int LOGIN_SESSION_TIMEOUT_MAX = 999;
	public static int PASSWORD_CHANGE_VELOCITY_MIN = 0;
	public static int PASSWORD_CHANGE_VELOCITY_MAX = 9999;
	
	public int AdminLevel;
	public int BankId;
	public int MaxTriesPerSession ; // 0 => infinit tries
	public int MaxTriesAcrossSession ; // 0 => infinit tries
	public long PasswordRenewalFrequency ; // specified in msec, 0 => Password Never Expires
	public long PasswordUnlockDuration ; // specified in msec, 0 => Not to be Unlocked automatically
	public long MaxInactivityPeriod ; // specified in msec, 0 => infinite Inactivity period
	public int AllowPasswordReset ; // -ve => No constraint on Min Numeric
	public int MaxLength ; // -ve => No constraint on Max length
	public int MinLength ; // -ve => No constraint on Min length
	public int MinAlphabet ; // -ve => No constraint on Min Alphabet
	public int MinNumeric ; // -ve => No constraint on Min Numeric
	public int MinSpecialCharacters ; // -ve => No constraint on Min Numeric
	public int loginSessionTimeout; // Min 1; No constraint on Max (in minutes)
	public int MinLowerCaseCharacters; // Minimum lower case character
	public int MinUpperCaseCharacters;// Mininum uppar case character
    private int collectAdminContactDetails;
    
    private int twoFactorDeliveryChannelType;
	public int passwordChangeVelocity;
	
	public static String DELIVERY_CHANNEL_SMS="SMS";
	public static String DELIVERY_CHANNEL_EMAIL="EMAIL";
	public static String DELIVERY_CHANNEL_VOICE="VOICE";	
	public static String DELIVERY_CHANNEL_EMAIL_AND_SMS="EMAIL_AND_SMS";
	public static String DELIVERY_CHANNEL_EMAIL_AND_VOICE="EMAIL_AND_VOICE";
	
	public static String COLLECTION_POLICY_NONE= "NONE";
	public static String COLLECTION_POLICY_EMAIL="EMAIL";
	public static String COLLECTION_POLICY_SMS= "PHONE";
	public static String COLLECTION_POLICY_EMAIL_SMS="EMAIL_ANS_SMS";
	public static String COLLECTION_POLICY_EMAIL_PHONE="EMAIL_AND_PHONE";
	
	
	
	
	private PasswordStorageAlgorithm passwordStorageAlgorithm = PasswordStorageAlgorithm.SHA1;
	private int retainOldFormatPassword = 1;

	public AdminLevelParams()
	{
		AdminLevel = -1;
		BankId = -1;
		MaxTriesPerSession = 0; // 0 => infinit tries
		MaxTriesAcrossSession = 0; // 0 => infinit tries
		PasswordRenewalFrequency = 0; // specified in msec, 0 => Password Never Expires
		PasswordUnlockDuration = 0; // specified in msec, 0 => Not to be Unlocked automatically
		MaxInactivityPeriod = 0; // specified in msec, 0 => infinite Inactivity period
		AllowPasswordReset = 0; // 0 means don't allow, that is the default behaviour before ths feature
		MaxLength = -1; // -ve => No constraint on Max length
		MinLength = -1; // -ve => No constraint on Min length
		MinAlphabet = -1; // -ve => No constraint on Min Alphabet
		MinNumeric = -1; // -ve => No constraint on Min Numeric
		MinSpecialCharacters = -1; // -ve => No constraint on Min Numeric
		loginSessionTimeout = LOGIN_SESSION_TIMEOUT_DEFAULT;
		MinLowerCaseCharacters = -1; // -ve => No constraint on Min lower case
		MinUpperCaseCharacters = -1; // -ve => No constraint on Min upper case
		collectAdminContactDetails = 0;
		passwordChangeVelocity = 0; // zero means it is not applicable.
		twoFactorDeliveryChannelType=0;
	}

	public void parseParams( String paramString )
	{
		if ( paramString == null )
			return;
		StringTokenizer st = new StringTokenizer( paramString, "|" );
		while ( st.hasMoreTokens() )
		{
			String token = st.nextToken();
			token = token.toUpperCase();
			int value = Integer.parseInt( token.substring(token.indexOf("=") + 1, token.length()) );
			if ( token.startsWith("MAXLENGTH") )
			{
				MaxLength = value;
			}
			else if ( token.startsWith("MINLENGTH") )
			{
				MinLength = value;
			}
			else if ( token.startsWith("MINALPHABET") )
			{
				MinAlphabet = value;
			}
			else if ( token.startsWith("MINNUMERIC") )
			{
				MinNumeric = value;
			}
			else if ( token.startsWith("MINSPECIALCHARS") )
			{
				MinSpecialCharacters = value;
			}
			else if ( token.startsWith("MINLOWERCASECHARS") )
			{
				MinLowerCaseCharacters = value;
			}
			else if ( token.startsWith("MINUPPERCASECHARS") )
			{
				MinUpperCaseCharacters = value;
			}
		}
	}
		
	public int getCollectAdminContactDetails(){
		return collectAdminContactDetails;
	}
	
	public void setCollectAdminContactDetails(int collectAdminContactDetails){
		this.collectAdminContactDetails = collectAdminContactDetails;
	}

	public PasswordStorageAlgorithm getPasswordStorageAlgorithm() {
		return passwordStorageAlgorithm;
	}

	public int getRetainOldFormatPassword() {
		return retainOldFormatPassword;
	}

	public void setPasswordStorageAlgorithm(int passwordStorageAlgorithm) {
		this.passwordStorageAlgorithm = PasswordStorageAlgorithm.getPasswordStorageAlgorithm(passwordStorageAlgorithm);
	}

	public void setRetainOldFormatPassword(int retainOldFormatPassword) {
		this.retainOldFormatPassword = retainOldFormatPassword;
	}
	
	public int getTwoFactorDeliveryChannelType() {
		return twoFactorDeliveryChannelType;
	}

	public void setTwoFactorDeliveryChannelType(int twoFactorDeliveryChannelType) {
		this.twoFactorDeliveryChannelType = twoFactorDeliveryChannelType;
	}
	
	
	
	public String getCollectionPolicyTypeString(){	
		
		int temp= this.collectAdminContactDetails;	
		
		if(temp == 0)
			return this.COLLECTION_POLICY_NONE;
		else if(temp == 1)
			return this.COLLECTION_POLICY_EMAIL;
		else if(temp == 2)
			return this.COLLECTION_POLICY_SMS;
		else if(temp == 3)
			return this.COLLECTION_POLICY_EMAIL_SMS;
		else
			return "CollectionPolicyDisabled";		
	}
	
	public static String getCollectionPolicyTypeString(int temp){	
		
		
		if(temp == 0)
			return COLLECTION_POLICY_NONE;
		else if(temp == 1)
			return COLLECTION_POLICY_EMAIL;
		else if(temp == 2)
			return COLLECTION_POLICY_SMS;
		else if(temp == 3)
			return COLLECTION_POLICY_EMAIL_PHONE;
		else
			return "CollectionPolicyDisabled";		
	}
	
	
	public String getDeliveryChannelTypeString(){		
		int temp= this.twoFactorDeliveryChannelType;		
		if(temp == 1)
			return this.DELIVERY_CHANNEL_SMS;
		else if(temp == 2)
			return this.DELIVERY_CHANNEL_EMAIL;
		else if(temp == 3)
			return this.DELIVERY_CHANNEL_VOICE;
		else if(temp == 4)
			return this.DELIVERY_CHANNEL_EMAIL_AND_SMS;
		else if(temp == 5)
			return this.DELIVERY_CHANNEL_EMAIL_AND_VOICE;
		else
			return "DeliveryChannelDisabled";		
	}


	public static String getDeliveryChannelTypeString(int temp){			
		if(temp == 1)
			return DELIVERY_CHANNEL_SMS;
		else if(temp == 2)
			return DELIVERY_CHANNEL_EMAIL;
		else if(temp == 3)
			return DELIVERY_CHANNEL_VOICE;
		else if(temp == 4)
			return DELIVERY_CHANNEL_EMAIL_AND_SMS;
		else if(temp == 5)
			return DELIVERY_CHANNEL_EMAIL_AND_VOICE;
		else
			return "DeliveryChannelDisabled";		
	}

	
	public enum PasswordStorageAlgorithm {
		SHA1(0), SHA2(1), ADMIN_TO_DECIDE(2);
		
		private int value;
		
		private PasswordStorageAlgorithm(int value) {
			this.value = value;
		}
		
		public int getValue() {
			return value;
		}
		
		public PasswordStorageAlgorithm valueOf(int value) {
			return PasswordStorageAlgorithm.valueOf(String.valueOf(value));
		}
		
		private static HashMap<Integer, PasswordStorageAlgorithm> allPasswordStorageAlgorithms = new HashMap<Integer, PasswordStorageAlgorithm>();
		
		static {
			for(PasswordStorageAlgorithm passwordStorageAlgorithm : EnumSet.allOf(PasswordStorageAlgorithm.class)) {
				allPasswordStorageAlgorithms.put(passwordStorageAlgorithm.getValue(), passwordStorageAlgorithm);
			}
		}
		
		public static PasswordStorageAlgorithm getPasswordStorageAlgorithm(int value) {
			return allPasswordStorageAlgorithms.get(value);
		}
		
		public String getLabelKey() {
			switch(this) {
			case SHA1 :
				return "S3792";
			case SHA2 :
				return "S3793";
			case ADMIN_TO_DECIDE :
				return "S3794";
			default :
				return name();
			}
		}
	}
}