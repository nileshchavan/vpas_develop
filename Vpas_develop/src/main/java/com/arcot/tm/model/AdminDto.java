package com.arcot.tm.model;

public class AdminDto {

	private Integer deliveryChannel;
	private Integer collectAdminContactDetails;
	/**
	 * @return the deliveryChannel
	 */
	public Integer getDeliveryChannel() {
		return deliveryChannel;
	}
	/**
	 * @param deliveryChannel the deliveryChannel to set
	 */
	public void setDeliveryChannel(Integer deliveryChannel) {
		this.deliveryChannel = deliveryChannel;
	}
	/**
	 * @return the collectAdminContactDetails
	 */
	public Integer getCollectAdminContactDetails() {
		return collectAdminContactDetails;
	}
	/**
	 * @param collectAdminContactDetails the collectAdminContactDetails to set
	 */
	public void setCollectAdminContactDetails(Integer collectAdminContactDetails) {
		this.collectAdminContactDetails = collectAdminContactDetails;
	}
	
	
}
