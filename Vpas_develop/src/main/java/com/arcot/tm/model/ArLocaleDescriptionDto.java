package com.arcot.tm.model;

public class ArLocaleDescriptionDto {

	private Integer localeId;
	private String description;
	/**
	 * @return the localeId
	 */
	public Integer getLocaleId() {
		return localeId;
	}
	/**
	 * @param localeId the localeId to set
	 */
	public void setLocaleId(Integer localeId) {
		this.localeId = localeId;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
