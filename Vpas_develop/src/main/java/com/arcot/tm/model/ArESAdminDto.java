package com.arcot.tm.model;

import java.sql.Date;
import java.util.List;

import com.arcot.tm.entities.ArCountry;
import com.arcot.tm.entities.ArESAdminPK;


public class ArESAdminDto {
	
	private ArESAdminPK aresadminpk;
	private String firstName;
	private String lastName;
	private String middleName;
	private int adminLevel;
	private int status;
	private Date lastLongTime;
	private String password;
	private List<ArBankInfoDto> issuers;
	private String accessControlList;
	private Integer maxValForLastPwdList;
	
	private Integer secondaryBankId;

	private List<ArCountry> selectedCountriesList;
	
	
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ArESAdminDto [aresadminpk=" + aresadminpk + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", middleName=" + middleName + ", adminLevel=" + adminLevel + ", status=" + status + ", lastLongTime="
				+ lastLongTime + ", issuers=" + issuers + ", selectedCountriesList=" + selectedCountriesList + "]";
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public int getAdminLevel() {
		return adminLevel;
	}

	public void setAdminLevel(int adminLevel) {
		this.adminLevel = adminLevel;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getLastLongTime() {
		return lastLongTime;
	}

	public void setLastLongTime(Date lastLongTime) {
		this.lastLongTime = lastLongTime;
	}

	public List<ArBankInfoDto> getIssuers() {
		return issuers;
	}

	public void setIssuers(List<ArBankInfoDto> issuers) {
		this.issuers = issuers;
	}


	public List<ArCountry> getSelectedCountriesList() {
		return selectedCountriesList;
	}

	public void setSelectedCountriesList(List<ArCountry> selectedCountriesList) {
		this.selectedCountriesList = selectedCountriesList;
	}

	

	public ArESAdminPK getAresadminpk() {
		return aresadminpk;
	}

	public void setAresadminpk(ArESAdminPK aresadminpk) {
		this.aresadminpk = aresadminpk;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAccessControlList() {
		return accessControlList;
	}

	public void setAccessControlList(String accessControlList) {
		this.accessControlList = accessControlList;
	}

	public Integer getMaxValForLastPwdList() {
		return maxValForLastPwdList;
	}

	public void setMaxValForLastPwdList(Integer maxValForLastPwdList) {
		this.maxValForLastPwdList = maxValForLastPwdList;
	}

	public Integer getSecondaryBankId() {
		return secondaryBankId;
	}

	public void setSecondaryBankId(Integer secondaryBankId) {
		this.secondaryBankId = secondaryBankId;
	}


}
