package com.arcot.tm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * Main class to run the server
 * @author Vizag Team
 *
 */
@SpringBootApplication
public class VpasDevelopApplication {

	public static void main(String[] args) {
		SpringApplication.run(VpasDevelopApplication.class, args);
	}

}