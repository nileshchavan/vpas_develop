//package com.arcot.tm.crypto;
//
//import com.arcot.tm.entities.ArESAdmin;
//
//public class EncryptedPasswordHash {
//	
//	private String passwordClear;
//	private String userIdClear;
//	private int bankId;
//	private String encryptedHash;
//	private String salt;
//	private int iterationCount;
//	
//	@Override
//	public String toString() {
//		return "PasswordHash [userIdClear=" + getUserIdClear() + ", passwordClear=" + passwordClear + ", bankId=" + getBankId() + ", encryptedHash=" + encryptedHash + ", salt=" + salt + ", iterationCount=" + iterationCount + "]";
//	}
//
//	public EncryptedPasswordHash(String passwordClear, String userId, int bankId, String encryptedHash, String salt, int iterationCount) {
//		this.passwordClear = passwordClear;
//		this.setUserIdClear(userId);
//		this.setBankId(bankId);
//		this.encryptedHash = encryptedHash;
//		this.salt = salt;
//		this.iterationCount = iterationCount;
//	}
//
//	public String getPasswordClear() {
//		return passwordClear;
//	}
//
//	public void setPasswordClear(String passwordClear) {
//		this.passwordClear = passwordClear;
//	}
//
//	public String getEncryptedHash() {
//		return encryptedHash;
//	}
//
//	public String getCompleteEncryptedHash() {
//		StringBuilder completeHash = new StringBuilder();
//		
//		completeHash.append(encryptedHash);
//		completeHash.append(ArESAdmin.PASSWORD_COMPONENT_SEPERATOR);
//		completeHash.append(salt);
//		completeHash.append(ArESAdmin.PASSWORD_COMPONENT_SEPERATOR);
//		completeHash.append(iterationCount);
//
//		return completeHash.toString();
//	}
//
//	public void setEncryptedHash(String encryptedHash) {
//		this.encryptedHash = encryptedHash;
//	}
//
//	public String getSalt() {
//		return salt;
//	}
//
//	public void setSalt(String salt) {
//		this.salt = salt;
//	}
//
//	public String getUserIdClear() {
//		return userIdClear;
//	}
//
//	public void setUserIdClear(String userIdClear) {
//		this.userIdClear = userIdClear;
//	}
//
//	public int getBankId() {
//		return bankId;
//	}
//
//	public void setBankId(int bankId) {
//		this.bankId = bankId;
//	}
//
//	public int getIterationCount() {
//		return iterationCount;
//	}
//
//	public void setIterationCount(int iterationCount) {
//		this.iterationCount = iterationCount;
//	}
//
//}
