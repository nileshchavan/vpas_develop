package com.arcot.tm.crypto;


public class CryptoUtil {

	public static native synchronized String hashDataB64(String data);

	public static native synchronized String hash(String data, int algorithm);
	
	public static native String encryptpbkdf2(String data, String salt, String keyName, int bankId, boolean softSign, int iterationCount, int encryptionAlgorithm, int hashAlgorithm);
	
	public static native synchronized String getProxyPAN(String data);

	public static native synchronized String getAttemptsPAN(String data);

	/**
	 * Keep the 'synchronized' for genRandom(). Observed JNI crash when 'synchronized' is removed 
	 */
	public static native synchronized void genRandom(byte[] buf);

	public static native synchronized int genRandomInteger();

	public static native synchronized int InitSigner(String pin, int sessions, String device);

	/**
	 * @deprecated use encrypt3DES64Ex instead
	 */
	public static native synchronized String encrypt3DES64(String keyName, String msg, boolean softSign);

	/**
	 * @deprecated use decrypt3DES64Ex instead
	 */
	public static native synchronized String decrypt3DES64(String keyName, String encodedMsg, boolean softSign);
	
	public static native String encrypt3DES64Ex(String keyName, String msg, boolean softSign, int bankId);

	public static native String decrypt3DES64Ex(String keyName, String encodedMsg, boolean softSign, int bankId);

	public static native synchronized String decrypt3DES64NonDb(String keyName, String encodedMsg, boolean softSign, boolean padData);

	public static native synchronized String encryptPKI( byte[] cert, String msg );

	public static native synchronized String decryptPKI( byte[] cert, byte[] key, String encrMsg );

	public static native synchronized String getVersion();

	/*
	 * This method is called whenever a value from vpassword.ini
	 * file is to be read.
	 */

	public static native synchronized boolean initVPASCryptoUtil();


	/*
	 * This method returns values that are in vpassword.ini file.
	 * User need to pass two parameters for it.
	 * First being a string containing the word "NCipher" for
	 * obtaining NCipher pin or database id for obtaining database
	 * password.
	 * Second parameter has to be a boolean that would 'false' for
	 * all except for obtaining MasterKey .
	 */

	public static native synchronized String getKeyValue(String key, boolean firstKey);

}
