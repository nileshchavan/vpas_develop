//package com.arcot.tm.crypto;
//
//import java.io.UnsupportedEncodingException;
//import java.util.Set;
//
//import com.arcot.crypto.beans.CryptoBean;
//import com.arcot.logger.ArcotLogger;
//import com.arcot.util.ArcotException;
//import com.arcot.vpas.enroll.cache.ESCache;
//import com.arcot.vpas.pool.ThreadPool;
//import com.arcot.vpas.pool.ThreadPoolFactory;
//
//public class CryptoService {
//
//	private boolean doubleHashingSupportedByJNI = false;
//
//	private static CryptoService cryptoService;;
//
//	protected boolean softSign = true;
//
//	public static String EMPTY_HASH_SHA256 = "2jmj7l5rSw0yVb/vlWAYkK/YBwk=";
//	public static String EMPTY_HASH_SHA1 = "2jmj7l5rSw0yVb/vlWAYkK/YBwk=";
//
//	protected CryptoService() {
//	}
//
//	public static CryptoService getInstance() {
//		return cryptoService;
//	}
//
//	public EncryptedPasswordHash generateEncryptedPasswordHash(String passwordClear, String userId, String keyName, int bankId) {
//		return generateEncryptedPasswordHash(passwordClear, userId, keyName, bankId, CryptoAlgorithm.SHA256);
//	}
//
//	public EncryptedPasswordHash generateEncryptedPasswordHash(String passwordClear, String salt, int iterationCount, String userId, String keyName, int bankId) {
//		return generateEncryptedPasswordHash(passwordClear, salt, iterationCount, userId, keyName, bankId, CryptoAlgorithm.SHA256);
//	}
//
//	public EncryptedPasswordHash generateEncryptedPasswordHash(String passwordClear, String userId, String keyName, int bankId, int algorithm) {
//		String salt;
//		byte bytes[] = new byte[20];
//		CryptoUtil.genRandom(bytes);
//		try {
//			salt = new String(Base64new.encode(bytes), "UTF-8");
//		} catch (UnsupportedEncodingException e) {
//			// this should never happen
//			throw new RuntimeException(e);
//		}
//		return generateEncryptedPasswordHash(passwordClear, salt, getIterationCount(), userId, keyName, bankId, algorithm);
//	}
//
//	private int getIterationCount() {
//		int iteration;
//		String iterationCount = ESCache.esc.getValue("PBKDF2IterationCount");
//		if (iterationCount == null || iterationCount.isEmpty()) {
//			iterationCount = "5000";
//		}
//		try {
//			iteration = Integer.parseInt(iterationCount);
//		} catch (NumberFormatException e) {
//			// If the parsing of the string fails, then reset it to the default values
//			iteration = 5000;
//		}
//
//		return iteration;
//	}
//
//	public EncryptedPasswordHash generateEncryptedPasswordHash(String passwordClear, String salt, int iteration, String userId, String keyName, int bankId, int algorithm) {
//		if (!doubleHashingSupportedByJNI && algorithm == CryptoAlgorithm.SHA256_OF_SHA1) { // Condition to be removed after patching latest JNI
////			ArcotLogger.logDebug("Algorithm for hashing :" + algorithm + ". SHA1 hash generated before generating SHA2.");
//			passwordClear = hash(passwordClear, CryptoAlgorithm.SHA1);
//			algorithm = CryptoAlgorithm.SHA256;
//		}
//
//		long t1 = System.currentTimeMillis();
//
//		// Ignoring userId and sending only the password as this will break the copy card / replace card operations using DUT
//		String encryptedHash = CryptoUtil.encryptpbkdf2(passwordClear, salt, keyName, bankId, softSign, iteration, CryptoAlgorithm.TDES, algorithm);
//		long t2 = System.currentTimeMillis();
////		ArcotLogger.logDebug("Time taken for hashing :" + (t2 - t1) + " milli seconds");
//		return new EncryptedPasswordHash(passwordClear, userId, bankId, encryptedHash, salt, iteration);
//	}
//
//	public String hash(String input, int algorithm) {
//		return CryptoUtil.hash(input, algorithm);
//	}
//
//	public String encrypt3DES64Ex(String keyName, String msg, int bankId) {
//		return CryptoUtil.encrypt3DES64Ex(keyName, msg, softSign, bankId);
//	}
//
//	public String encrypt3DES64(String keyName, String msg) {
//		return CryptoUtil.encrypt3DES64(keyName, msg, softSign);
//	}
//
//	// Used by DUT for encrypt operations using Upload Key
//	public String encrypt3DES64(String keyName, String msg, boolean softSign) {
//		return CryptoUtil.encrypt3DES64(keyName, msg, softSign);
//	}
//
//	public String decrypt3DES64Ex(String keyName, String msg, int bankId) {
//		return CryptoUtil.decrypt3DES64Ex(keyName, msg, softSign, bankId);
//	}
//
//	public String decrypt3DES64(String keyName, String msg) {
//		return CryptoUtil.decrypt3DES64(keyName, msg, softSign);
//	}
//
//	// Used by DUT for decrypt operations using Upload Key
//	public String decrypt3DES64(String keyName, String msg, boolean softSign) {
//		return CryptoUtil.decrypt3DES64(keyName, msg, softSign);
//	}
//
//	protected static CryptoService initCryptoService(boolean softSign) throws Exception {
//		if (cryptoService == null) {
//			synchronized (CryptoService.class) {
//				if (cryptoService == null) {
//					cryptoService = new CryptoService();
//					cryptoService.init(softSign);
//				}
//			}
//		}
//		return cryptoService;
//	}
//
//	protected static CryptoService initCryptoService(String pin, int numOfSession, boolean softSign, String deviceName) throws Exception {
//		if (cryptoService == null) {
//			synchronized (CryptoService.class) {
//				if (cryptoService == null) {
//					cryptoService = new CryptoService();
//					cryptoService.init(pin, numOfSession, softSign, deviceName);
//				}
//			}
//		}
//		return cryptoService;
//	}
//
//	private void init(boolean softSign) {
//		this.softSign = softSign;
//	}
//
//	private void init(String pin, int numOfSession, boolean softSign, String deviceName) throws Exception {
//
//		loadCryptoLibrary();
//
//		this.softSign = softSign;
//
//		String logMsg = "Invoking initVPASCryptoUtil...";
//		System.out.println(logMsg);
//		ArcotLogger.logInfo(logMsg);
//		if (!CryptoUtil.initVPASCryptoUtil()) {
//			logMsg = "initVPASCryptoUtil JNI call returned false";
//			System.out.println(logMsg);
//			ArcotLogger.logError(logMsg);
//			throw new ArcotException("INIT_CRYPTO_UTIL_FAILED", logMsg);
//		}
//		logMsg = "initVPASCryptoUtil done";
//		System.out.println(logMsg);
//		ArcotLogger.logInfo(logMsg);
//
//		if (!softSign) {
//			logMsg = "Invoking InitSigner...";
//			System.out.println(logMsg);
//			ArcotLogger.logInfo(logMsg);
//
//			int status = CryptoUtil.InitSigner(pin, numOfSession, deviceName);
//			if (status != 0) {
//				logMsg = "InitSigner() JNI call failed. Status value [" + status + "]";
//				System.out.println(logMsg);
//				ArcotLogger.logError(logMsg);
//				throw new ArcotException("INIT_SIGNER_FAILED", logMsg);
//			}
//			logMsg = "InitSigner done";
//			System.out.println(logMsg);
//			ArcotLogger.logInfo(logMsg);
//		}
//	}
//
//	private void loadCryptoLibrary() {
//
//		String logMsg = "Loading Arcot JNI [arcotjni]...";
//		System.out.println(logMsg);
////		ArcotLogger.logInfo(logMsg);
//
//		System.loadLibrary("arcotjni");
//
//		logMsg = "Arcot JNI [arcotjni] loaded";
//		System.out.println(logMsg);
////		ArcotLogger.logInfo(logMsg);
//
//		logMsg = "Arcot JNI Version: ";
////		ArcotLogger.logInfo(logMsg + CryptoUtil.getVersion());
//		System.out.println(logMsg + CryptoUtil.getVersion());
//	}
//
//	public static final String ENCRYPT3DES64 = "encrypt3DES64";
//	public static final String DECRYPT3DES64 = "decrypt3DES64";
//
//	public void encrypt3DES64(Set<? extends CryptoBean> beans) {
//		process(ENCRYPT3DES64, beans);
//	}
//
//	public void decrypt3DES64(Set<? extends CryptoBean> beans) {
//		process(DECRYPT3DES64, beans);
//	}
//
//	private void process(final String methodName, Set<? extends CryptoBean> beans) {
//		long startTime = System.currentTimeMillis();
//
//		if (beans == null) {
//			ArcotLogger.logError("No beans provided for encryption/decryption.");
//			return;
//		}
//
//		ThreadPool threadPool = null;
//		boolean useThreadPool = false;
//		try {
//			threadPool = ThreadPoolFactory.getInstance().getThreadPoolByUsage(methodName);
//			useThreadPool = true;
//		} catch (ArcotException e) {
//			if (ThreadPoolFactory.ERROR_THREADPOOL_BUSY.equals(e.getErrorCode())) {
//				ArcotLogger.logTrace("Thread pool with usage name " + methodName + " is busy, executing the tasks in parent thread until the thread pool is free.");
//				useThreadPool = true;
//			} else {
//				ArcotLogger.logError("ArcotException while accessing the global thread pool with usage name " + methodName + ", all tasks will be executed in parent thread :"
//						+ e.getErrorCode());
//			}
//		} catch (Exception e) {
//			ArcotLogger.logError("Exception while accessing the global thread pool with usage name " + methodName + ", all tasks will be executed in parent thread :"
//					+ e.getMessage());
//		}
//
//		for (final CryptoBean bean : beans) {
//			if (useThreadPool) {
//				Runnable runnable = new Runnable() {
//					@Override
//					public void run() {
//						process(methodName, bean);
//					}
//				};
//
//				try {
//					threadPool = ThreadPool.executeTask(threadPool, methodName, runnable);
//				} catch (ArcotException e) {
//					ArcotLogger.logError("ArcotException while executing a task :", e);
//					throw new RuntimeException("ArcotException while executing a task :" + e.getMessage());
//				}
//			} else {
//				process(methodName, bean);
//			}
//		}
//
//		if (threadPool != null) {
//			long threadPoolUsageCount = threadPool.getFutures().size();
//			try {
//				threadPool.finishExecutionAndReturn();
//			} catch (ArcotException e) {
//				ArcotLogger.logError("ArcotException while returning the thread pool :", e);
//				throw new RuntimeException("ArcotException while returning the thread pool :" + e.getMessage());
//			} catch (Exception e) {
//				ArcotLogger.logError("Exception while returning the thread pool", e);
//				throw new RuntimeException("Exception while returning the thread pool :" + e.getMessage());
//			}
//			ArcotLogger.logDebug("Time taken for " + methodName + " a batch of " + beans.size() + " with thread pool " + threadPool.getPoolName() + "(" + threadPoolUsageCount
//					+ "):" + (System.currentTimeMillis() - startTime));
//		} else {
//			ArcotLogger.logDebug("Time taken for " + methodName + " a batch of " + beans.size() + " :" + (System.currentTimeMillis() - startTime));
//		}
//	}
//
//	private void process(String methodName, CryptoBean bean) {
//		bean.setProcessed(true);
//
//		if (ENCRYPT3DES64.equals(methodName)) {
//			bean.setTextEnc(encrypt3DES64(ESCache.bic.getBank(bean.getBankId()).BANKKEY_CLEAR, bean.getTextClear()));
//		} else if (DECRYPT3DES64.equals(methodName)) {
//			bean.setTextClear(decrypt3DES64(ESCache.bic.getBank(bean.getBankId()).BANKKEY_CLEAR, bean.getTextEnc()));
//		} else {
//			bean.setProcessed(false);
//		}
//	}
//
//}
