package com.arcot.tm.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.arcot.tm.entities.ArAdminBank;
import com.arcot.tm.entities.ArBankInfo;
import com.arcot.tm.entities.ArBrandInfo;
import com.arcot.tm.entities.ArCountry;
import com.arcot.tm.entities.ArESAdmin;
import com.arcot.tm.model.ArBankInfoDto;
import com.arcot.tm.model.ArBranddto;
import com.arcot.tm.model.ArLocaleDescriptionDto;
import com.arcot.tm.payload.ResponseObject;
import com.arcot.tm.repository.ArBrandInfoRepository;
import com.arcot.tm.service.AdminService;
import com.arcot.tm.service.GlobalAdminService;
import com.arcot.tm.service.MasterAdminService;

@RestController
@CrossOrigin
@RequestMapping("/api/admin/global")
/**
 * This class is used to provide implementations for the global admin
 * @author Vizag Team
 *
 */
public class GlobalController {
	
	@Autowired
	private AdminService adminService;
	@Autowired
	private GlobalAdminService globalAdminService;
	@Autowired
	private MasterAdminService masterAdminService;
	@Autowired
	private ArBrandInfoRepository arBrandInfoRepository;

	/**
	 * This Method is used to fetch all records of admin by admin level
	 * @param admin
	 * @return list of admins
	 */

	@PostMapping("/getAllAdminsOfIssuer")
	public ResponseEntity<ResponseObject> getAllAdminsOfIssuer(@RequestBody ArESAdmin admin){
		
		List<ArAdminBank> adminList = adminService.getAllAdminsOfIssuer(admin.getAresadminpk().getBankId(),admin.getAresadminpk().getName());
		ResponseObject responseObject = new ResponseObject();
		
		if(adminList != null) {
			
			
			responseObject.setResponse(adminList);
            responseObject.setStatusCode(1);
		}else {
			responseObject.setResponse("No admin present");
            responseObject.setStatusCode(-1);
		}
		 return new ResponseEntity<ResponseObject>(responseObject,HttpStatus.OK);
	}
	/**
	 * This Method is used to save the issuer object into data base
	 * @param bankInfo
	 * @return Response object
	 */
	@PostMapping("/createIssuer")
	public ResponseEntity<ResponseObject> createIssuer(@RequestBody ArBankInfoDto bankInfoDto){

		
		ArBankInfo savedBankInfo = globalAdminService.createIssuer(bankInfoDto);
//		CryptoUtil.encrypt3DES64(masterKey, strEncryptionKey.trim(), dbParams.softsign);
		
		ResponseObject responseObject = new ResponseObject();
		
		if(savedBankInfo != null) {
			
			responseObject.setResponse(savedBankInfo);
            responseObject.setStatusCode(1);
		}else {
			responseObject.setResponse("Something wrong");
            responseObject.setStatusCode(-1);
		}
		
		 return new ResponseEntity<ResponseObject>(responseObject,HttpStatus.OK);
		
	}


	
	/**
	 * This Method is used to fetch all records of countries
	 * @return list of countries
	 */
	@GetMapping("/getAllCountry")
	public ResponseEntity<ResponseObject> getAllCountry(){

		//Getting the list of country according to listed countries for global admin
		List<ArCountry> countryList = adminService.getAllCountry();
		
		//Invoking the response Object to send the list of countries
		ResponseObject responseObject = new ResponseObject();
		
		if(countryList != null) {
			
			responseObject.setResponse(countryList);
            responseObject.setStatusCode(1);
		}else {
			responseObject.setResponse("No country present");
            responseObject.setStatusCode(-1);
		}
		
		 return new ResponseEntity<ResponseObject>(responseObject,HttpStatus.OK);
	}
	
	/**
	 * This Method is used to fetch all records of locales
	 * @return list of locales
	 */
	@GetMapping("/getAllLocale")
	public ResponseEntity<ResponseObject> getAllLocale(){
		//Getting the list of locales according to listed locales for global admin
		List<ArLocaleDescriptionDto> localeList = adminService.getAllLocale();
		
		//Invoking the response Object to send the list of localess
		ResponseObject responseObject = new ResponseObject();
		
		if(localeList != null) {
			responseObject.setResponse(localeList);
            responseObject.setStatusCode(1);
		}else {
			responseObject.setResponse("No locales present");
            responseObject.setStatusCode(-1);
		}
		
		 return new ResponseEntity<ResponseObject>(responseObject,HttpStatus.OK);
	}

	@PostMapping("/createCardRangeForIssuer")
	public ResponseEntity<ResponseObject> createCardRangeForIssuer(@RequestBody ArBranddto arBrandInfo){
		int savedCardRangeForIssuer = globalAdminService.createCardRangeForIssuer(arBrandInfo);
		
		ResponseObject responseObject = new ResponseObject();
		
		if(savedCardRangeForIssuer == 1 ) {
			
			responseObject.setResponse(savedCardRangeForIssuer);
            responseObject.setStatusCode(1);
		}else  if(savedCardRangeForIssuer == -1){
			responseObject.setResponse("Invalid pan length");
            responseObject.setStatusCode(-1);
		}
		else  if(savedCardRangeForIssuer == -2){
			responseObject.setResponse("Invalid BusinessId:businessId must be of 8 digits");
            responseObject.setStatusCode(-2);
		}
		else  if(savedCardRangeForIssuer == -3){
			responseObject.setResponse("Invalid FiBinId:fiBinId must be of 6 digits");
            responseObject.setStatusCode(-3);
		}
		else  if(savedCardRangeForIssuer == -4){
			responseObject.setResponse("Invalid begin range or end range");
            responseObject.setStatusCode(-1);
		}
		else  if(savedCardRangeForIssuer == -5){
			responseObject.setResponse("Invalid pan length");
            responseObject.setStatusCode(-1);
		}
		
		 return new ResponseEntity<ResponseObject>(responseObject,HttpStatus.OK);
		
	}
	
	@PostMapping(value="/saveFileforCardRange")
	public ResponseEntity<ResponseObject> uploadSigningCertificate(@RequestParam("signingCertificate") MultipartFile signingCertificate , @RequestParam("rangeId") Integer rangeId){
		ResponseObject responseObject = new ResponseObject();
		int savingFileResult = globalAdminService.uploadSigningCertificate(rangeId,signingCertificate);
		if(savingFileResult == 1) {
			responseObject.setResponse("File saved successfully");
            responseObject.setStatusCode(1);
		}
		else if(savingFileResult == -1){
				responseObject.setResponse("File is empty");
	            responseObject.setStatusCode(-1);
		}
		else if(savingFileResult == -2){
			responseObject.setResponse("Object not found with that range id");
            responseObject.setStatusCode(-2);
		}	
		else
		{
			responseObject.setResponse("Failed to store file");
            responseObject.setStatusCode(-3);
		}
		
		 return new ResponseEntity<ResponseObject>(responseObject,HttpStatus.OK);
		
	}
	
	@GetMapping("/getAllIssuerBanks")
	public ResponseEntity<ResponseObject> getAllIssuers(){
		List<ArBankInfoDto> issuersList = masterAdminService.getAllIssuers();
		
		ResponseObject responseObject = new ResponseObject();
		
		if(!issuersList.isEmpty()) {
			
			responseObject.setResponse(issuersList);
            responseObject.setStatusCode(1);
		}else {
			responseObject.setResponse("Something wrong");
            responseObject.setStatusCode(-1);
		}
		
		 return new ResponseEntity<ResponseObject>(responseObject,HttpStatus.OK);
		
	}
	
	@GetMapping("/getAllCardRangeGroup")
	public ResponseEntity<ResponseObject> getAllCardRangeGroup(@RequestParam int bankId){
		List<ArBrandInfo> issuersList = globalAdminService.getAllCardRangeGroup(bankId);
		
		ResponseObject responseObject = new ResponseObject();
		
		if(!issuersList.isEmpty()) {
			
			responseObject.setResponse(issuersList);
            responseObject.setStatusCode(1);
		}else {
			responseObject.setResponse("Something wrong");
            responseObject.setStatusCode(-1);
		}
		
		 return new ResponseEntity<ResponseObject>(responseObject,HttpStatus.OK);
		
	}
	
}
