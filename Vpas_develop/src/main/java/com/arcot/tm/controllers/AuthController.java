package com.arcot.tm.controllers;

import java.net.URI;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.arcot.tm.entities.ArESAdmin;
import com.arcot.tm.model.ArESAdminDto;
import com.arcot.tm.payload.JwtAuthenticationResponse;
import com.arcot.tm.payload.LoginRequest;
import com.arcot.tm.payload.ResponseObject;
import com.arcot.tm.repository.AdminRepository;
//import com.arcot.tm.security.JwtTokenProvider;
import com.arcot.tm.service.GlobalAdminService;
import com.arcot.tm.service.AuthService;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/auth")
/**
 * This class is used to provide authentication for the application
 * @author Vizag Team
 *
 */
public class AuthController {

//    @Autowired
//    AuthenticationManager authenticationManager;

    @Autowired
    private GlobalAdminService adminService;
    
    @Autowired
    AdminRepository adminRepository;

    @Autowired
    AuthService authService;
    
//    @Autowired
//    JwtTokenProvider tokenProvider;

    /**
     * This Method is used to provide implementation for login
     * @param loginRequest
     * @return Response Object
     */
    @PostMapping("/signin")
    @CrossOrigin("*")
    public ResponseEntity<ResponseObject> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
    	//Invoking model mapper object to map the dto object and entity class
    	ModelMapper modelMapper = new ModelMapper();
    	
    	//Invoking the response Object to authenticate the admin
        ResponseObject responseObject = new ResponseObject();;
    	
//        Authentication authentication = authenticationManager.authenticate(
//                new UsernamePasswordAuthenticationToken(
//                        loginRequest.getUsernameOrEmail(),
//                        loginRequest.getPassword()
//                )
//        );
        
//        SecurityContextHolder.getContext().setAuthentication(authentication);
        //method call to service to validate the credentials
        int res= authService.login(loginRequest);
        
        //validating the responses  from the service class
        if(res==1)
        {
        	//Getting the admin object from db to send the response of admin object
        	ArESAdmin admin = adminService.getUserByUsername(loginRequest.getUsernameOrEmail());
        	
        	//Mapping the dto object and entity class using model mapper
        	ArESAdminDto userDto = modelMapper.map(admin, ArESAdminDto.class);
        	
            responseObject.setResponse(userDto);
            responseObject.setStatusCode(1);
        }	else if(res == -1)
        	{
        		responseObject.setResponse("Password or username cannot be null");
                responseObject.setStatusCode(-1);
        	}
        
        else if(res == -2)
        {
        	responseObject.setResponse("Account doesn't exist");
            responseObject.setStatusCode(-2);
        }
        else if(res==-3){
        	responseObject.setResponse("Account Blocked");
            responseObject.setStatusCode(-3);
        }
        else if(res==-4){
        	responseObject.setResponse("password doesnt match");
            responseObject.setStatusCode(-4);
        }
        	
       
        
//        String jwt = tokenProvider.generateToken(authentication);
        
//        responseObject.setJwtAuthenticationResponse(new JwtAuthenticationResponse(jwt));
        return new ResponseEntity<ResponseObject>(responseObject,HttpStatus.OK);
    }

}
