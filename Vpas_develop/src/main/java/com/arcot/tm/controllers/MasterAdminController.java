package com.arcot.tm.controllers;

import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.arcot.tm.entities.ArCountry;
import com.arcot.tm.entities.ArESAdmin;
import com.arcot.tm.model.ArBankInfoDto;
import com.arcot.tm.model.ArESAdminDto;
import com.arcot.tm.payload.MasterLoginRequest;
import com.arcot.tm.payload.ResponseObject;
import com.arcot.tm.service.AuthService;
import com.arcot.tm.service.MasterAdminService;

@RestController
@RequestMapping("/api/admin/master")
@CrossOrigin
/**
 * This class is used to provide implementation for master admin
 * @author  Vizag Team
 *
 */
public class MasterAdminController {

	@Autowired
	private MasterAdminService masterAdminService;

	@Autowired
	private AuthService authService;

	/**
	 * This method is used to save the admin object into the database
	 * @param globalAdmin
	 * @return Response Object
	 */
	@PostMapping("/createGlobalAdmin")
	public ResponseEntity<ResponseObject> createGlobalAdmin(@RequestBody ArESAdminDto globalAdmin){
		
		//Method call for create global admin 
		int result = masterAdminService.createGlobalAdmin(globalAdmin);
		
		//Invoking the response Object to send the data and status 
		ResponseObject responseObject = new ResponseObject();

		if(result == 1) {

			responseObject.setResponse("Saved Successfully");
			responseObject.setStatusCode(1);
		}else {
			responseObject.setResponse("Something wrong");
			responseObject.setStatusCode(-1);
		}

		return new ResponseEntity<ResponseObject>(responseObject,HttpStatus.OK);

	}

	/**
	 * This method is used to fetch all records of issuers from the database
	 * @return list of issuers
	 */
	@GetMapping("/getAllIssuers")
	public ResponseEntity<ResponseObject> getAllIssuers( ){
		
		
		//Getting the list of all the issuers
		List<ArBankInfoDto> issuers= masterAdminService.getAllIssuers();

		//Invoking the response Object to send the list of issuers and status 
		ResponseObject responseObject = new ResponseObject();

		//Validating the data from the data and sending the response according to it
		if(issuers.isEmpty()) {

			responseObject.setResponse("No data!!");
			responseObject.setStatusCode(-1);
		}else {
			responseObject.setResponse(issuers);
			responseObject.setStatusCode(1);
		}

		return new ResponseEntity<ResponseObject>(responseObject,HttpStatus.OK);

	}

	/**
	 * This Method is used to fetch all records of countries
	 * @return list of countries
	 */
	@GetMapping("/getAllCountries")
	public ResponseEntity<ResponseObject> getAllCountries( ){
		

		//Getting the list of all the countries
		List<ArCountry> getAllCountries = masterAdminService.getAllCountries();
		
		//Invoking the response Object to send the list of countries and status 
		ResponseObject responseObject = new ResponseObject();

		//Validating the data from the data and sending the response according to it
		if(!getAllCountries.isEmpty()) {

			responseObject.setResponse(getAllCountries);
			responseObject.setStatusCode(1);
		}else {
			responseObject.setResponse("Something wrong");
			responseObject.setStatusCode(-1);
		}

		return new ResponseEntity<ResponseObject>(responseObject,HttpStatus.OK);

	}

	/**
	 * This method is used to provide login implementation for master admin
	 * @param masterLoginRequest
	 * @return
	 */
	@PostMapping("/signin")
	public ResponseEntity<ResponseObject> authenticateMasterUser(@Valid @RequestBody MasterLoginRequest masterLoginRequest) {

		int res = 0;

		ModelMapper modelMapper = new ModelMapper();
		
		ResponseObject responseObject = new ResponseObject();;


		//        Authentication authentication = authenticationManager.authenticate(
		//                new UsernamePasswordAuthenticationToken(
		//                        loginRequest.getUsernameOrEmail(),
		//                        loginRequest.getPassword()
		//                )
		//        );

		//        SecurityContextHolder.getContext().setAuthentication(authentication);


		ArESAdmin admin= masterAdminService.getMasterAdminByLevel();

		if(admin!=null) {

			res=authService.masterAdminLogin(masterLoginRequest,admin);
		}
		else {
			responseObject.setResponse("Master Admin not Found");
			responseObject.setStatusCode(-5);
		}


		if(res==1)
		{
			ArESAdminDto userDto = modelMapper.map(admin, ArESAdminDto.class);

			responseObject.setResponse(userDto);
			responseObject.setStatusCode(1);
		}	else if(res == -1)
		{
			responseObject.setResponse("Password cannot be null");
			responseObject.setStatusCode(-1);
		}

		else if(res == -2)
		{
			responseObject.setResponse("Account doesn't exist");
			responseObject.setStatusCode(-2);
		}
		else if(res==-3){
			responseObject.setResponse("Account Blocked");
			responseObject.setStatusCode(-3);
		}
		else if(res==-4){
			responseObject.setResponse("password doesnt match");
			responseObject.setStatusCode(-4);
		}

		//        String jwt = tokenProvider.generateToken(authentication);

		//        responseObject.setJwtAuthenticationResponse(new JwtAuthenticationResponse(jwt));
		return new ResponseEntity<ResponseObject>(responseObject,HttpStatus.OK);
	}

}
