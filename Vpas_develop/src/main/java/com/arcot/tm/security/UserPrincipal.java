//package com.arcot.tm.security;
//
//import java.util.ArrayList;
//import java.util.Collection;
//import java.util.List;
//import java.util.Objects;
//
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.AuthorityUtils;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.userdetails.UserDetails;
//
//import com.fasterxml.jackson.annotation.JsonIgnore;
//import com.arcot.tm.entities.ArESAdmin;
//import com.arcot.tm.util.AdminRole;
//
//public class UserPrincipal implements UserDetails {
//    
//	
//	private static final long serialVersionUID = 1L;
//
//	private Long  bankId;
//	
//
//	@JsonIgnore
//    private String name;
//
//
//    @JsonIgnore
//    private String password;
//    
//
//    private Collection<? extends GrantedAuthority> authorities;
//
//    public UserPrincipal(Long bankId,String name, String password,Collection<? extends GrantedAuthority> authorities) {
//        this.bankId = bankId;
//        this.name = name;
//        this.password = password;
//        this.authorities = authorities;
//    }
//
//    public static UserPrincipal create(ArESAdmin admin) {
////       List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
////        authorities.add(1, new SimpleGrantedAuthority(user.getRole()));
//    	
//    	int value=admin.getAdminLevel();
//    	String adminRole=AdminRole.values()[value].name();
//    	
//        List<GrantedAuthority> authorities = AuthorityUtils.commaSeparatedStringToAuthorityList(adminRole);
//   
//        return new UserPrincipal(
//        		admin.getBankId(),
//        		admin.getName(),
//        		admin.getPassword(),
//        		
//                authorities
//               
//        );
//    }
//
//
//	@Override
//	public String getUsername() {
//		// TODO Auto-generated method stub
//		return this.name;
//	}
//
//
//    @Override
//    public String getPassword() {
//        return password;
//    }
//
//    @Override
//    public Collection<? extends GrantedAuthority> getAuthorities() {
//        return authorities;
//    }
//
//    @Override
//    public boolean isAccountNonExpired() {
//        return true;
//    }
//
//    @Override
//    public boolean isAccountNonLocked() {
//        return true;
//    }
//
//    @Override
//    public boolean isCredentialsNonExpired() {
//        return true;
//    }
//
//    @Override
//    public boolean isEnabled() {
//        return true;
//    }
//    
//
//    /**
//	 * @return the bankId
//	 */
//	public Long getBankId() {
//		return bankId;
//	}
//
//	/**
//	 * @param bankId the bankId to set
//	 */
//	public void setBankId(Long bankId) {
//		this.bankId = bankId;
//	}
//
//	/**
//	 * @return the name
//	 */
//	public String getName() {
//		return name;
//	}
//
//	/**
//	 * @param name the name to set
//	 */
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	/**
//	 * @param password the password to set
//	 */
//	public void setPassword(String password) {
//		this.password = password;
//	}
//
//	/**
//	 * @param authorities the authorities to set
//	 */
//	public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
//		this.authorities = authorities;
//	}
//
//	@Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        UserPrincipal that = (UserPrincipal) o;
//        return Objects.equals(bankId, that.bankId);
//    }
//
//    @Override
//    public int hashCode() {
//
//        return Objects.hash(bankId);
//    }
//}