//package com.arcot.tm.security;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import com.arcot.tm.entities.ArESAdmin;
//import com.arcot.tm.repository.AdminRepository;
//
//@Service
//public class CustomUserDetailsService implements UserDetailsService {
//
//    @Autowired
//    AdminRepository adminRepository;
//
//    @Override
//    @Transactional
//    public UserDetails loadUserByUsername(String name)
//            throws UsernameNotFoundException {
//        // Let people login with either username or email
//        ArESAdmin admin = adminRepository.findByName(name)
//                .orElseThrow(() -> 
//                        new UsernameNotFoundException("User not found with username or email : " + name)
//        );
//
//        return UserPrincipal.create(admin);
//    }
//
//    // This method is used by JWTAuthenticationFilter
//    @Transactional
//    public UserDetails loadUserById(String name) {
//    	ArESAdmin admin = adminRepository.findByName(name).orElseThrow(
//            () -> new UsernameNotFoundException("User not found with id : " + name)
//        );
//
//        return UserPrincipal.create(admin);
//    }
//}
