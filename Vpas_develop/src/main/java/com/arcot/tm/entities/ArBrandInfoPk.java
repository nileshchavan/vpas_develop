package com.arcot.tm.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ArBrandInfoPk  implements Serializable{
  
	@Column(name="BANKID")
	private Integer bankId;
	
	
	@Column(name="BEGINRANGE")
	private String beginRange;
	
	@Column(name="ENDRANGE")
	private String endRange;

	public Integer getBankId() {
		return bankId;
	}

	public void setBankId(Integer bankId) {
		this.bankId = bankId;
	}

	public String getBeginRange() {
		return beginRange;
	}

	public void setBeginRange(String beginRange) {
		this.beginRange = beginRange;
	}

	public String getEndRange() {
		return endRange;
	}

	public void setEndRange(String endRange) {
		this.endRange = endRange;
	}
	
	
	
}
