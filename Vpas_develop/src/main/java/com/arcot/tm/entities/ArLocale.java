package com.arcot.tm.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ARLOCALE")
public class ArLocale {
	
	@Id
	@Column(name = "LOCALEID")
	private Integer localeId;
	
	@Column(name = "LOCALE")
	private String locale;
	
	@Column(name = "DESCRIPTION")
	private String description;

	/**
	 * @return the localeId
	 */
	public Integer getLocaleId() {
		return localeId;
	}

	/**
	 * @param localeId the localeId to set
	 */
	public void setLocaleId(Integer localeId) {
		this.localeId = localeId;
	}

	/**
	 * @return the locale
	 */
	public String getLocale() {
		return locale;
	}

	/**
	 * @param locale the locale to set
	 */
	public void setLocale(String locale) {
		this.locale = locale;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	
	
}
