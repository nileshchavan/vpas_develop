package com.arcot.tm.entities;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Table(name = "ARBRANDINFO" ,uniqueConstraints=@UniqueConstraint(columnNames ={"RANGEID"}))
@Entity
public class ArBrandInfo {

	
	/**
	 * @return the rangeId
	 */
	public Integer getRangeId() {
		return rangeId;
	}

	/**
	 * @param rangeId the rangeId to set
	 */
	public void setRangeId(Integer rangeId) {
		this.rangeId = rangeId;
	}

	@EmbeddedId
	private ArBrandInfoPk arBrandInfoPk;
	
	@Column(name="RANGEID")
	private Integer rangeId;
	
	
	@Column(name = "ACSURL1")
	private String acsurL1;
	@Column(name = "ACSURL10")
	private String acsurL10;
	@Column(name = "ACSURL2")
	private String acsurL2;
	@Column(name = "ACSURL3")
	private String acsurL3;
	@Column(name = "ACSURL4")
	private String acsurL4;
	@Column(name = "ACSURL5")
	private String acsurL5;
	@Column(name = "ACSURL6")
	private String acsurL6;
	@Column(name = "ACSURL7")
	private String acsurL7;
	@Column(name = "ACSURL8")
	private String acsurL8;
	@Column(name = "ACSURL9")
	private String acsurL9;

	@Column(name = "ARCOTOTPENABLED")
	private Integer  arcotOtpEnambled;
	
	@Column(name = "ARQCREQUIRED")
	private Integer arqcReequired;
	
	@Column(name = "AUTHFALLBACK")
	private Integer authFallBack;
	@Column(name = "AUTHMETHOD")
	private Integer authMethod;
	
	@Column(name = "AUTHPRIORITY")
	private String authPriority;


	@Column(name = "AUTHSELECTALLOWED")
	private Integer authselectedAllowed;

	@Column(name = "AUTHSERVERCHANNEL")
	private Integer authServerChannel;

	@Column(name = "AUTHSERVERHOST")
	private String authServerHost;
	
	@Column(name = "AUTHSERVERTRANSPORT")
	private String  authServerTransport;
	
	@Column(name = "AVSPOLICY")
	private String avsPolicy;
	
	
	@Column (name = "AUTHRESULTKEYIN")
	 private String authResultKeyIn;
	@Column(name = "BRANDINGURL1")
	private String brandInGurl1;
	
	@Column(name = "BRANDINGURL2")
	private String brandInGurl2;

	@Column(name="ISSUERDATAPOLICY")
	private Integer issuerDataPolicy;

	@Column(name="EXTERNALPROVIDER")
	private String externalProvider;

	@Column(name="FITERMPOLICYVERSION")
	private String filtermPolicyVersion;

	@Column(name="CARDTYPE")
	private String cardtype;

	@Column(name="EXTERNALSYSTEMURL")
	private String externalSystemurl;

	@Column(name="EXTERNALSYSTEMPATTERN")
	private String EXTERNALSYSTEMPATTERN;

	@Column(name="EXPERIANCUSTOMERID")
	private String experianCustomerId;

	@Column(name="EXPERIANCUSTOMERPASSWORD")
	private String experianCustomerPassword;
	
	@Column(name = "HSMVARIANT")
	private String hsmVariant;

	@Column(name="NSTRIKESACROSSSESSIONS")
	private Integer nstrikesAcrossSessions;

	 @Column(name ="PANLENGTH")
	 private Integer panLength;
	 
	 @Column(name ="PLUGINNAME")
	 private String pluginName;
	 
	@Column(name="MAXAUTHTRIES")
	private Integer maxAuthTries;

	@Column(name="MAXAUTHTRIESFORAUTOFYP")
	private Integer maxAuthTriesForAutoFyp;

	@Column(name="PLUGINURL")
	private String pluginUrl;

	@Column(name="STATUS")
	private Integer status;

	@Column(name="DATECREATED")
	private Date dateCreated;

	@Column(name="ENROLLOPTIONS")
	private Integer enrollOptions;

	@Column(name="ATTEMPTSMODE")
	private Integer attemptsMode;

	@Column(name="MAXDECLINE")
	private Integer maxDecline;

	@Column(name="MAXWELCOME")
	private Integer maxWelcome;
	
	@Column(name = "MDK")
	private String mdk;
	@Column(name = "MOBILEENABLED")
	private Integer mobileEnabled;
	
	@Column(name ="PLUGINVERSION")
	private String pluginVersion;
	

	@Column(name="VELOG")
	private Integer velog;

	@Column(name="BINKEYID")
	private Integer binKeyId;

	@Column(name="EACCESSREQUIRED")
	private Integer eAccessRequired;
	@Column(name="SOFTWAREARQC")
	private Integer softwareARQC;
	@Column(name="SOFTWAREENCRYPT")
	private Integer softWareEncypt;
	@Column(name = "SOFTWARESIGNING")
	private Integer softWareResigning;
	
	@Column(name="RECEIPTCERTFILE")
	private String receiptCertfile;
	@Column(name="RECEIPTURL")
	private String receiptURL;
	@Column(name="RECEIPTURL2")
	private String receiptURL2;
	@Column(name="RECEIPTSIGNINGKEY")
	private String receiptSigningKey;
	@Column(name="RECEIPTUSER")
	private String receiptUser;
	@Column(name="RECEIPTPASSWORD")
	private String receiptPassword;
	@Column(name="RECEIPTTYPE")
	private String receiptType;
	
	@Column(name ="SIGNINGCERTFILE")
	private byte[] signingCertFile;
	@Column(name = "SIGNINGCERTFILE_07")
	private byte[] signingCertFile_07;
	
	@Column(name="RECEIPTSERVERNAME")
	private String receiptServerName;
	@Column(name="FIBUSINESSID")
	private String fiBusinessId;
	@Column(name="FIBIN")
	private String fiBin;
	
	@Column(name="CVV2POLICY")
	private String cvv2Policy;

	@Column(name="KEYLABEL")
	private String keyLabel;

	@Column(name="CARDRANGENAME")
	private String cardRangeName;

	@Column(name="CVVDESKEYA")
	private String cvvDesKeyA;

	@Column(name="CVVDESKEYB")
	private String cvvDesKeyB;

	@Column(name="CONFIGID")
	private String configId;

	@Column(name="CVVKEYIND")
	private String cvvKeyInd;

	@Column(name="SIGNINGKEYIN")
	private String signingKeyIn;
	
	@Column(name ="SIGNINGKEYFILE")
	private byte[] signingKeyFile;
	

	@Column(name="CHIPKEYIN")
	private String chipKeyIn;


	@Column(name="AAVALGORITHM")
	private String aavAlgorithm;

	@Column(name="CHPASSWORDEXPIRYDURATION")
	private Integer chPasswordExpiryDuration;

	@Column(name="ENABLEATTEMPTSAFTERMAXDECLINES")
	private Integer enableAttemptsAfterMaxDeclines;

	@Column(name="CAPURLPARAMETERS")
	private String capUrlParameters;

	@Column(name="CERTID")
	private Integer certId;


	@Column(name="CONFIGLIST")
	private String configList;

	@Column(name ="SIGNINGKEYFILE_07")
	private byte[] signingKeyFile_07;
	
	@Column(name="TTPCONFIGID")
	private Integer ttpConfigId;

	@Column(name="ENABLESCA")
	private Integer enablesCa;

	@Column(name="TRUSTEDBENEFICIARY")
	private Integer trustedBenificiary;

	/**
	 * @return the arBrandInfoPk
	 */
	public ArBrandInfoPk getArBrandInfoPk() {
		return arBrandInfoPk;
	}

	/**
	 * @param arBrandInfoPk the arBrandInfoPk to set
	 */
	public void setArBrandInfoPk(ArBrandInfoPk arBrandInfoPk) {
		this.arBrandInfoPk = arBrandInfoPk;
	}

	

	

	/**
	 * @return the acsurL1
	 */
	public String getAcsurL1() {
		return acsurL1;
	}

	/**
	 * @param acsurL1 the acsurL1 to set
	 */
	public void setAcsurL1(String acsurL1) {
		this.acsurL1 = acsurL1;
	}

	/**
	 * @return the acsurL10
	 */
	public String getAcsurL10() {
		return acsurL10;
	}

	/**
	 * @param acsurL10 the acsurL10 to set
	 */
	public void setAcsurL10(String acsurL10) {
		this.acsurL10 = acsurL10;
	}

	/**
	 * @return the acsurL2
	 */
	public String getAcsurL2() {
		return acsurL2;
	}

	/**
	 * @param acsurL2 the acsurL2 to set
	 */
	public void setAcsurL2(String acsurL2) {
		this.acsurL2 = acsurL2;
	}

	/**
	 * @return the acsurL3
	 */
	public String getAcsurL3() {
		return acsurL3;
	}

	/**
	 * @param acsurL3 the acsurL3 to set
	 */
	public void setAcsurL3(String acsurL3) {
		this.acsurL3 = acsurL3;
	}

	/**
	 * @return the acsurL4
	 */
	public String getAcsurL4() {
		return acsurL4;
	}

	/**
	 * @param acsurL4 the acsurL4 to set
	 */
	public void setAcsurL4(String acsurL4) {
		this.acsurL4 = acsurL4;
	}

	/**
	 * @return the acsurL5
	 */
	public String getAcsurL5() {
		return acsurL5;
	}

	/**
	 * @param acsurL5 the acsurL5 to set
	 */
	public void setAcsurL5(String acsurL5) {
		this.acsurL5 = acsurL5;
	}

	/**
	 * @return the acsurL6
	 */
	public String getAcsurL6() {
		return acsurL6;
	}

	/**
	 * @param acsurL6 the acsurL6 to set
	 */
	public void setAcsurL6(String acsurL6) {
		this.acsurL6 = acsurL6;
	}

	/**
	 * @return the acsurL7
	 */
	public String getAcsurL7() {
		return acsurL7;
	}

	/**
	 * @param acsurL7 the acsurL7 to set
	 */
	public void setAcsurL7(String acsurL7) {
		this.acsurL7 = acsurL7;
	}

	/**
	 * @return the acsurL8
	 */
	public String getAcsurL8() {
		return acsurL8;
	}

	/**
	 * @param acsurL8 the acsurL8 to set
	 */
	public void setAcsurL8(String acsurL8) {
		this.acsurL8 = acsurL8;
	}

	/**
	 * @return the acsurL9
	 */
	public String getAcsurL9() {
		return acsurL9;
	}

	/**
	 * @param acsurL9 the acsurL9 to set
	 */
	public void setAcsurL9(String acsurL9) {
		this.acsurL9 = acsurL9;
	}

	/**
	 * @return the arcotOtpEnambled
	 */
	public Integer getArcotOtpEnambled() {
		return arcotOtpEnambled;
	}

	/**
	 * @param arcotOtpEnambled the arcotOtpEnambled to set
	 */
	public void setArcotOtpEnambled(Integer arcotOtpEnambled) {
		this.arcotOtpEnambled = arcotOtpEnambled;
	}

	/**
	 * @return the arqcReequired
	 */
	public Integer getArqcReequired() {
		return arqcReequired;
	}

	/**
	 * @param arqcReequired the arqcReequired to set
	 */
	public void setArqcReequired(Integer arqcReequired) {
		this.arqcReequired = arqcReequired;
	}

	/**
	 * @return the authFallBack
	 */
	public Integer getAuthFallBack() {
		return authFallBack;
	}

	/**
	 * @param authFallBack the authFallBack to set
	 */
	public void setAuthFallBack(Integer authFallBack) {
		this.authFallBack = authFallBack;
	}

	/**
	 * @return the authMethod
	 */
	public Integer getAuthMethod() {
		return authMethod;
	}

	/**
	 * @param authMethod the authMethod to set
	 */
	public void setAuthMethod(Integer authMethod) {
		this.authMethod = authMethod;
	}

	/**
	 * @return the authPriority
	 */
	public String getAuthPriority() {
		return authPriority;
	}

	/**
	 * @param authPriority the authPriority to set
	 */
	public void setAuthPriority(String authPriority) {
		this.authPriority = authPriority;
	}

	/**
	 * @return the authselectedAllowed
	 */
	public Integer getAuthselectedAllowed() {
		return authselectedAllowed;
	}

	/**
	 * @param authselectedAllowed the authselectedAllowed to set
	 */
	public void setAuthselectedAllowed(Integer authselectedAllowed) {
		this.authselectedAllowed = authselectedAllowed;
	}

	/**
	 * @return the authServerChannel
	 */
	public Integer getAuthServerChannel() {
		return authServerChannel;
	}

	/**
	 * @param authServerChannel the authServerChannel to set
	 */
	public void setAuthServerChannel(Integer authServerChannel) {
		this.authServerChannel = authServerChannel;
	}

	/**
	 * @return the authServerHost
	 */
	public String getAuthServerHost() {
		return authServerHost;
	}

	/**
	 * @param authServerHost the authServerHost to set
	 */
	public void setAuthServerHost(String authServerHost) {
		this.authServerHost = authServerHost;
	}

	/**
	 * @return the authServerTransport
	 */
	public String getAuthServerTransport() {
		return authServerTransport;
	}

	/**
	 * @param authServerTransport the authServerTransport to set
	 */
	public void setAuthServerTransport(String authServerTransport) {
		this.authServerTransport = authServerTransport;
	}

	/**
	 * @return the avsPolicy
	 */
	public String getAvsPolicy() {
		return avsPolicy;
	}

	/**
	 * @param avsPolicy the avsPolicy to set
	 */
	public void setAvsPolicy(String avsPolicy) {
		this.avsPolicy = avsPolicy;
	}

	/**
	 * @return the authResultKeyIn
	 */
	public String getAuthResultKeyIn() {
		return authResultKeyIn;
	}

	/**
	 * @param authResultKeyIn the authResultKeyIn to set
	 */
	public void setAuthResultKeyIn(String authResultKeyIn) {
		this.authResultKeyIn = authResultKeyIn;
	}


	/**
	 * @return the brandInGurl1
	 */
	public String getBrandInGurl1() {
		return brandInGurl1;
	}

	/**
	 * @param brandInGurl1 the brandInGurl1 to set
	 */
	public void setBrandInGurl1(String brandInGurl1) {
		this.brandInGurl1 = brandInGurl1;
	}

	/**
	 * @return the brandInGurl2
	 */
	public String getBrandInGurl2() {
		return brandInGurl2;
	}

	/**
	 * @param brandInGurl2 the brandInGurl2 to set
	 */
	public void setBrandInGurl2(String brandInGurl2) {
		this.brandInGurl2 = brandInGurl2;
	}

	/**
	 * @return the issuerDataPolicy
	 */
	public Integer getIssuerDataPolicy() {
		return issuerDataPolicy;
	}

	/**
	 * @param issuerDataPolicy the issuerDataPolicy to set
	 */
	public void setIssuerDataPolicy(Integer issuerDataPolicy) {
		this.issuerDataPolicy = issuerDataPolicy;
	}

	/**
	 * @return the externalProvider
	 */
	public String getExternalProvider() {
		return externalProvider;
	}

	/**
	 * @param externalProvider the externalProvider to set
	 */
	public void setExternalProvider(String externalProvider) {
		this.externalProvider = externalProvider;
	}

	/**
	 * @return the filtermPolicyVersion
	 */
	public String getFiltermPolicyVersion() {
		return filtermPolicyVersion;
	}

	/**
	 * @param filtermPolicyVersion the filtermPolicyVersion to set
	 */
	public void setFiltermPolicyVersion(String filtermPolicyVersion) {
		this.filtermPolicyVersion = filtermPolicyVersion;
	}

	/**
	 * @return the cardtype
	 */
	public String getCardtype() {
		return cardtype;
	}

	/**
	 * @param cardtype the cardtype to set
	 */
	public void setCardtype(String cardtype) {
		this.cardtype = cardtype;
	}

	/**
	 * @return the externalSystemurl
	 */
	public String getExternalSystemurl() {
		return externalSystemurl;
	}

	/**
	 * @param externalSystemurl the externalSystemurl to set
	 */
	public void setExternalSystemurl(String externalSystemurl) {
		this.externalSystemurl = externalSystemurl;
	}

	/**
	 * @return the eXTERNALSYSTEMPATTERN
	 */
	public String getEXTERNALSYSTEMPATTERN() {
		return EXTERNALSYSTEMPATTERN;
	}

	/**
	 * @param eXTERNALSYSTEMPATTERN the eXTERNALSYSTEMPATTERN to set
	 */
	public void setEXTERNALSYSTEMPATTERN(String eXTERNALSYSTEMPATTERN) {
		EXTERNALSYSTEMPATTERN = eXTERNALSYSTEMPATTERN;
	}

	/**
	 * @return the experianCustomerId
	 */
	public String getExperianCustomerId() {
		return experianCustomerId;
	}

	/**
	 * @param experianCustomerId the experianCustomerId to set
	 */
	public void setExperianCustomerId(String experianCustomerId) {
		this.experianCustomerId = experianCustomerId;
	}

	/**
	 * @return the experianCustomerPassword
	 */
	public String getExperianCustomerPassword() {
		return experianCustomerPassword;
	}

	/**
	 * @param experianCustomerPassword the experianCustomerPassword to set
	 */
	public void setExperianCustomerPassword(String experianCustomerPassword) {
		this.experianCustomerPassword = experianCustomerPassword;
	}

	/**
	 * @return the hsmVariant
	 */
	public String getHsmVariant() {
		return hsmVariant;
	}

	/**
	 * @param hsmVariant the hsmVariant to set
	 */
	public void setHsmVariant(String hsmVariant) {
		this.hsmVariant = hsmVariant;
	}

	/**
	 * @return the nstrikesAcrossSessions
	 */
	public Integer getNstrikesAcrossSessions() {
		return nstrikesAcrossSessions;
	}

	/**
	 * @param nstrikesAcrossSessions the nstrikesAcrossSessions to set
	 */
	public void setNstrikesAcrossSessions(Integer nstrikesAcrossSessions) {
		this.nstrikesAcrossSessions = nstrikesAcrossSessions;
	}

	/**
	 * @return the panLength
	 */
	public Integer getPanLength() {
		return panLength;
	}

	/**
	 * @param panLength the panLength to set
	 */
	public void setPanLength(Integer panLength) {
		this.panLength = panLength;
	}

	/**
	 * @return the pluginName
	 */
	public String getPluginName() {
		return pluginName;
	}

	/**
	 * @param pluginName the pluginName to set
	 */
	public void setPluginName(String pluginName) {
		this.pluginName = pluginName;
	}

	/**
	 * @return the maxAuthTries
	 */
	public Integer getMaxAuthTries() {
		return maxAuthTries;
	}

	/**
	 * @param maxAuthTries the maxAuthTries to set
	 */
	public void setMaxAuthTries(Integer maxAuthTries) {
		this.maxAuthTries = maxAuthTries;
	}

	/**
	 * @return the maxAuthTriesForAutoFyp
	 */
	public Integer getMaxAuthTriesForAutoFyp() {
		return maxAuthTriesForAutoFyp;
	}

	/**
	 * @param maxAuthTriesForAutoFyp the maxAuthTriesForAutoFyp to set
	 */
	public void setMaxAuthTriesForAutoFyp(Integer maxAuthTriesForAutoFyp) {
		this.maxAuthTriesForAutoFyp = maxAuthTriesForAutoFyp;
	}

	/**
	 * @return the pluginUrl
	 */
	public String getPluginUrl() {
		return pluginUrl;
	}

	/**
	 * @param pluginUrl the pluginUrl to set
	 */
	public void setPluginUrl(String pluginUrl) {
		this.pluginUrl = pluginUrl;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the dateCreated
	 */
	public Date getDateCreated() {
		return dateCreated;
	}

	/**
	 * @param dateCreated the dateCreated to set
	 */
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	/**
	 * @return the enrollOptions
	 */
	public Integer getEnrollOptions() {
		return enrollOptions;
	}

	/**
	 * @param enrollOptions the enrollOptions to set
	 */
	public void setEnrollOptions(Integer enrollOptions) {
		this.enrollOptions = enrollOptions;
	}

	/**
	 * @return the attemptsMode
	 */
	public Integer getAttemptsMode() {
		return attemptsMode;
	}

	/**
	 * @param attemptsMode the attemptsMode to set
	 */
	public void setAttemptsMode(Integer attemptsMode) {
		this.attemptsMode = attemptsMode;
	}

	/**
	 * @return the maxDecline
	 */
	public Integer getMaxDecline() {
		return maxDecline;
	}

	/**
	 * @param maxDecline the maxDecline to set
	 */
	public void setMaxDecline(Integer maxDecline) {
		this.maxDecline = maxDecline;
	}

	/**
	 * @return the maxWelcome
	 */
	public Integer getMaxWelcome() {
		return maxWelcome;
	}

	/**
	 * @param maxWelcome the maxWelcome to set
	 */
	public void setMaxWelcome(Integer maxWelcome) {
		this.maxWelcome = maxWelcome;
	}

	/**
	 * @return the mdk
	 */
	public String getMdk() {
		return mdk;
	}

	/**
	 * @param mdk the mdk to set
	 */
	public void setMdk(String mdk) {
		this.mdk = mdk;
	}

	/**
	 * @return the mobileEnabled
	 */
	public Integer getMobileEnabled() {
		return mobileEnabled;
	}

	/**
	 * @param mobileEnabled the mobileEnabled to set
	 */
	public void setMobileEnabled(Integer mobileEnabled) {
		this.mobileEnabled = mobileEnabled;
	}

	/**
	 * @return the pluginVersion
	 */
	public String getPluginVersion() {
		return pluginVersion;
	}

	/**
	 * @param pluginVersion the pluginVersion to set
	 */
	public void setPluginVersion(String pluginVersion) {
		this.pluginVersion = pluginVersion;
	}

	/**
	 * @return the velog
	 */
	public Integer getVelog() {
		return velog;
	}

	/**
	 * @param velog the velog to set
	 */
	public void setVelog(Integer velog) {
		this.velog = velog;
	}

	/**
	 * @return the binKeyId
	 */
	public Integer getBinKeyId() {
		return binKeyId;
	}

	/**
	 * @param binKeyId the binKeyId to set
	 */
	public void setBinKeyId(Integer binKeyId) {
		this.binKeyId = binKeyId;
	}

	/**
	 * @return the eAccessRequired
	 */
	public Integer geteAccessRequired() {
		return eAccessRequired;
	}

	/**
	 * @param eAccessRequired the eAccessRequired to set
	 */
	public void seteAccessRequired(Integer eAccessRequired) {
		this.eAccessRequired = eAccessRequired;
	}

	/**
	 * @return the softwareARQC
	 */
	public Integer getSoftwareARQC() {
		return softwareARQC;
	}

	/**
	 * @param softwareARQC the softwareARQC to set
	 */
	public void setSoftwareARQC(Integer softwareARQC) {
		this.softwareARQC = softwareARQC;
	}

	/**
	 * @return the softWareEncypt
	 */
	public Integer getSoftWareEncypt() {
		return softWareEncypt;
	}

	/**
	 * @param softWareEncypt the softWareEncypt to set
	 */
	public void setSoftWareEncypt(Integer softWareEncypt) {
		this.softWareEncypt = softWareEncypt;
	}

	/**
	 * @return the softWareResigning
	 */
	public Integer getSoftWareResigning() {
		return softWareResigning;
	}

	/**
	 * @param softWareResigning the softWareResigning to set
	 */
	public void setSoftWareResigning(Integer softWareResigning) {
		this.softWareResigning = softWareResigning;
	}

	/**
	 * @return the receiptCertfile
	 */
	public String getReceiptCertfile() {
		return receiptCertfile;
	}

	/**
	 * @param receiptCertfile the receiptCertfile to set
	 */
	public void setReceiptCertfile(String receiptCertfile) {
		this.receiptCertfile = receiptCertfile;
	}

	/**
	 * @return the receiptURL
	 */
	public String getReceiptURL() {
		return receiptURL;
	}

	/**
	 * @param receiptURL the receiptURL to set
	 */
	public void setReceiptURL(String receiptURL) {
		this.receiptURL = receiptURL;
	}

	/**
	 * @return the receiptURL2
	 */
	public String getReceiptURL2() {
		return receiptURL2;
	}

	/**
	 * @param receiptURL2 the receiptURL2 to set
	 */
	public void setReceiptURL2(String receiptURL2) {
		this.receiptURL2 = receiptURL2;
	}

	/**
	 * @return the receiptSigningKey
	 */
	public String getReceiptSigningKey() {
		return receiptSigningKey;
	}

	/**
	 * @param receiptSigningKey the receiptSigningKey to set
	 */
	public void setReceiptSigningKey(String receiptSigningKey) {
		this.receiptSigningKey = receiptSigningKey;
	}

	/**
	 * @return the receiptUser
	 */
	public String getReceiptUser() {
		return receiptUser;
	}

	/**
	 * @param receiptUser the receiptUser to set
	 */
	public void setReceiptUser(String receiptUser) {
		this.receiptUser = receiptUser;
	}

	/**
	 * @return the receiptPassword
	 */
	public String getReceiptPassword() {
		return receiptPassword;
	}

	/**
	 * @param receiptPassword the receiptPassword to set
	 */
	public void setReceiptPassword(String receiptPassword) {
		this.receiptPassword = receiptPassword;
	}

	/**
	 * @return the receiptType
	 */
	public String getReceiptType() {
		return receiptType;
	}

	/**
	 * @param receiptType the receiptType to set
	 */
	public void setReceiptType(String receiptType) {
		this.receiptType = receiptType;
	}

	/**
	 * @return the signingCertFile
	 */
	public byte[] getSigningCertFile() {
		return signingCertFile;
	}

	/**
	 * @param signingCertFile the signingCertFile to set
	 */
	public void setSigningCertFile(byte[] signingCertFile) {
		this.signingCertFile = signingCertFile;
	}

	/**
	 * @return the signingCertFile_07
	 */
	public byte[] getSigningCertFile_07() {
		return signingCertFile_07;
	}

	/**
	 * @param signingCertFile_07 the signingCertFile_07 to set
	 */
	public void setSigningCertFile_07(byte[] signingCertFile_07) {
		this.signingCertFile_07 = signingCertFile_07;
	}

	/**
	 * @return the receiptServerName
	 */
	public String getReceiptServerName() {
		return receiptServerName;
	}

	/**
	 * @param receiptServerName the receiptServerName to set
	 */
	public void setReceiptServerName(String receiptServerName) {
		this.receiptServerName = receiptServerName;
	}

	/**
	 * @return the fiBusinessId
	 */
	public String getFiBusinessId() {
		return fiBusinessId;
	}

	/**
	 * @param fiBusinessId the fiBusinessId to set
	 */
	public void setFiBusinessId(String fiBusinessId) {
		this.fiBusinessId = fiBusinessId;
	}

	/**
	 * @return the fiBin
	 */
	public String getFiBin() {
		return fiBin;
	}

	/**
	 * @param fiBin the fiBin to set
	 */
	public void setFiBin(String fiBin) {
		this.fiBin = fiBin;
	}

	/**
	 * @return the cvv2Policy
	 */
	public String getCvv2Policy() {
		return cvv2Policy;
	}

	/**
	 * @param cvv2Policy the cvv2Policy to set
	 */
	public void setCvv2Policy(String cvv2Policy) {
		this.cvv2Policy = cvv2Policy;
	}

	
	/**
	 * @return the signingKeyFile
	 */
	public byte[] getSigningKeyFile() {
		return signingKeyFile;
	}

	/**
	 * @param signingKeyFile the signingKeyFile to set
	 */
	public void setSigningKeyFile(byte[] signingKeyFile) {
		this.signingKeyFile = signingKeyFile;
	}


	/**
	 * @return the signingKeyFile_07
	 */
	public byte[] getSigningKeyFile_07() {
		return signingKeyFile_07;
	}

	/**
	 * @param signingKeyFile_07 the signingKeyFile_07 to set
	 */
	public void setSigningKeyFile_07(byte[] signingKeyFile_07) {
		this.signingKeyFile_07 = signingKeyFile_07;
	}

	public String getKeyLabel() {
		return keyLabel;
	}

	public void setKeyLabel(String keyLabel) {
		this.keyLabel = keyLabel;
	}

	public String getCardRangeName() {
		return cardRangeName;
	}

	public void setCardRangeName(String cardRangeName) {
		this.cardRangeName = cardRangeName;
	}

	public String getCvvDesKeyA() {
		return cvvDesKeyA;
	}

	public void setCvvDesKeyA(String cvvDesKeyA) {
		this.cvvDesKeyA = cvvDesKeyA;
	}

	public String getCvvDesKeyB() {
		return cvvDesKeyB;
	}

	public void setCvvDesKeyB(String cvvDesKeyB) {
		this.cvvDesKeyB = cvvDesKeyB;
	}

	public String getConfigId() {
		return configId;
	}

	public void setConfigId(String configId) {
		this.configId = configId;
	}

	public String getCvvKeyInd() {
		return cvvKeyInd;
	}

	public void setCvvKeyInd(String cvvKeyInd) {
		this.cvvKeyInd = cvvKeyInd;
	}

	public String getSigningKeyIn() {
		return signingKeyIn;
	}

	public void setSigningKeyIn(String signingKeyIn) {
		this.signingKeyIn = signingKeyIn;
	}

	public String getChipKeyIn() {
		return chipKeyIn;
	}

	public void setChipKeyIn(String chipKeyIn) {
		this.chipKeyIn = chipKeyIn;
	}

	public String getAavAlgorithm() {
		return aavAlgorithm;
	}

	public void setAavAlgorithm(String aavAlgorithm) {
		this.aavAlgorithm = aavAlgorithm;
	}

	public Integer getChPasswordExpiryDuration() {
		return chPasswordExpiryDuration;
	}

	public void setChPasswordExpiryDuration(Integer chPasswordExpiryDuration) {
		this.chPasswordExpiryDuration = chPasswordExpiryDuration;
	}

	public Integer getEnableAttemptsAfterMaxDeclines() {
		return enableAttemptsAfterMaxDeclines;
	}

	public void setEnableAttemptsAfterMaxDeclines(Integer enableAttemptsAfterMaxDeclines) {
		this.enableAttemptsAfterMaxDeclines = enableAttemptsAfterMaxDeclines;
	}

	public String getCapUrlParameters() {
		return capUrlParameters;
	}

	public void setCapUrlParameters(String capUrlParameters) {
		this.capUrlParameters = capUrlParameters;
	}

	public Integer getCertId() {
		return certId;
	}

	public void setCertId(Integer certId) {
		this.certId = certId;
	}

	public String getConfigList() {
		return configList;
	}

	public void setConfigList(String configList) {
		this.configList = configList;
	}

	public Integer getTtpConfigId() {
		return ttpConfigId;
	}

	public void setTtpConfigId(Integer ttpConfigId) {
		this.ttpConfigId = ttpConfigId;
	}

	public Integer getEnablesCa() {
		return enablesCa;
	}

	public void setEnablesCa(Integer enablesCa) {
		this.enablesCa = enablesCa;
	}

	public Integer getTrustedBenificiary() {
		return trustedBenificiary;
	}

	public void setTrustedBenificiary(Integer trustedBenificiary) {
		this.trustedBenificiary = trustedBenificiary;
	}

	
		}
