package com.arcot.tm.entities;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "ARDEVICE")

public class ArDevice {


	@EmbeddedId
	private ArDevicePk arDevicePk;
	
	
	@Column(name = "ACRURLID")
	private Integer acsUrlId;
	
	
	@Column(name = "FOLDERID")
	private Integer folderId;
	
	@Column(name = "HTTPUAID")
	private Integer httpUaId;

	/**
	 * @return the arDevicePk
	 */
	public ArDevicePk getArDevicePk() {
		return arDevicePk;
	}

	/**
	 * @param arDevicePk the arDevicePk to set
	 */
	public void setArDevicePk(ArDevicePk arDevicePk) {
		this.arDevicePk = arDevicePk;
	}

	/**
	 * @return the acsUrlId
	 */
	public Integer getAcsUrlId() {
		return acsUrlId;
	}

	/**
	 * @param acsUrlId the acsUrlId to set
	 */
	public void setAcsUrlId(Integer acsUrlId) {
		this.acsUrlId = acsUrlId;
	}

	/**
	 * @return the folderId
	 */
	public Integer getFolderId() {
		return folderId;
	}

	/**
	 * @param folderId the folderId to set
	 */
	public void setFolderId(Integer folderId) {
		this.folderId = folderId;
	}

	/**
	 * @return the httpUaId
	 */
	public Integer getHttpUaId() {
		return httpUaId;
	}

	/**
	 * @param httpUaId the httpUaId to set
	 */
	public void setHttpUaId(Integer httpUaId) {
		this.httpUaId = httpUaId;
	}

	

		
}
