package com.arcot.tm.entities;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Arrays;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="ARESADMIN")
public class ArESAdmin {

	@EmbeddedId
	private ArESAdminPK aresadminpk;

	@Column(name="PASSWORD")
	private String password;
	@Column(name="DATECREATED")
	private Date dateCreated;
	@Column(name="ADMINLEVEL")
	private Integer adminLevel;
	@Column(name="STATUS")
	private Integer status;
	@Column(name="FAILEDSTRIKES")
	private Integer failedStrikes;
	@Column(name="FIRSTFAILEDSTRIKETIME")
	private Date firstFailedStrikeTime; 
	
	@Column(name="LASTLOGONTIME")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss.SSS")
	private Timestamp lastLongTime;
	@Column(name="TEMPPASSWORDDURATION")
	private long tempPasswordDuration;
	@Column(name="STATUSCHANGEDATE")
	private Date statusChangeDate;
	@Column(name="ACCESSCONTROLLIST")
	private String accessControlList;
	@Column(name="STARTDATEOFFSET")
	private Integer startDateOffSet;
	@Column(name="RECNUMPERPAGE")
	private Integer recNumPerPage;
	@Column(name="DATEMODIFIED")
	private Date dateModified;
	@Column(name="REMARK")
	private String remark;
	@Column(name="TIMESTAMPFORMAT")
	private String timeStampFormat;
	@Column(name="REPORTTIMEZONELOCAL")
	private String reportTimeZoneLocal;
	@Column(name="PROFILEINFO")
	private String profileInfo;
	@Column(name="LOCALEID")
	private Integer localeId;
	@Column(name="AUTHORIZEDBANKS")
	private byte[] authorizedBanks;
	@Column(name="TEMPLATENAME")
	private String templateName;
	@Column(name="FIRSTNAME")
	private String firstName;
	@Column(name="LASTNAME")
	private String lastName;
	@Column(name="MIDDLENAME")
	private String middleName;
	@Column(name="FIRSTLOGONTIME")
	private Date firstLogIntegerime;
	@Column(name="BLOCKSIMULTLOGIN")
	private Integer blocksiMultlogin;
	@Column(name="HTTPSESSIONID")
	private String httpSessionId;
	@Column(name="MAXVALFORLASTPWDLIST")
	private Integer maxValForLastPwdList;
	@Column(name="LASTPWDLIST")
	private String lastPwdList;
	@Column(name="JOBTITLE")
	private String jobTitle;
	@Column(name="SUPERVISORNAME")
	private String superVisorName;
	@Column(name="EMAILADDRESS")
	private String emailAddress;
	@Column(name="PHONEMUMBER")
	private String phoneNumber;
	@Column(name="HIntegerQUESTION")
	private String hIntegerQuestion;
	@Column(name="HIntegerANSWER")
	private String hIntegerAnswer;
	@Column(name="UPDATEPROFILEATLOGIN")
	private Integer updateProfileAtLogin; 

	@Column(name="PROCESSORNAME")
	private String processorName;

	@Column(name="SUBPROCESSORNAME")
	private String subProcessorName;

	@Column(name="AUTHTYPE")
	private Integer authType;

	@Column(name="IS2FAAUTHENABLED")
	private Integer is2FAAuthenabled;

	@Column(name="PASSWORDEX")
	private String passwordEx;

	@Column(name="PASSWORDTYPE")
	private Integer passwordType;

	@Column(name="LASTPWDLISTEX")
	private String lastPwdListEx;

	@Column(name="PASSWORDLASTUPDATED")
	private Date passwordLastUpdated;

	@Column(name="COLLECTADMINCONTACTDETAILS")
	private Integer collectAdminContactDetails;

	@Column(name="DELIVERYCHANNEL")
	private Integer deliveryChannel;

	@Column(name="SECONDARYBANKID")
	private Integer secondaryBankId;
	
	/**
	 * @return the aresadminpk
	 */
	public ArESAdminPK getAresadminpk() {
		return aresadminpk;
	}

	/**
	 * @param aresadminpk the aresadminpk to set
	 */
	public void setAresadminpk(ArESAdminPK aresadminpk) {
		this.aresadminpk = aresadminpk;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the dateCreated
	 */
	public Date getDateCreated() {
		return dateCreated;
	}

	/**
	 * @param dateCreated the dateCreated to set
	 */
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	/**
	 * @return the adminLevel
	 */
	public Integer getAdminLevel() {
		return adminLevel;
	}

	/**
	 * @param adminLevel the adminLevel to set
	 */
	public void setAdminLevel(Integer adminLevel) {
		this.adminLevel = adminLevel;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the failedStrikes
	 */
	public Integer getFailedStrikes() {
		return failedStrikes;
	}

	/**
	 * @param failedStrikes the failedStrikes to set
	 */
	public void setFailedStrikes(Integer failedStrikes) {
		this.failedStrikes = failedStrikes;
	}

	/**
	 * @return the firstFailedStrikeTime
	 */
	public Date getFirstFailedStrikeTime() {
		return firstFailedStrikeTime;
	}

	/**
	 * @param firstFailedStrikeTime the firstFailedStrikeTime to set
	 */
	public void setFirstFailedStrikeTime(Date firstFailedStrikeTime) {
		this.firstFailedStrikeTime = firstFailedStrikeTime;
	}

	/**
	 * @return the lastLongTime
	 */
	public Timestamp getLastLongTime() {
		return lastLongTime;
	}

	/**
	 * @param timestamp the lastLongTime to set
	 */
	public void setLastLongTime(Timestamp timestamp) {
		this.lastLongTime = timestamp;
	}

	/**
	 * @return the tempPasswordDuration
	 */
	public long getTempPasswordDuration() {
		return tempPasswordDuration;
	}

	/**
	 * @param tempPasswordDuration the tempPasswordDuration to set
	 */
	public void setTempPasswordDuration(long tempPasswordDuration) {
		this.tempPasswordDuration = tempPasswordDuration;
	}

	/**
	 * @return the statusChangeDate
	 */
	public Date getStatusChangeDate() {
		return statusChangeDate;
	}

	/**
	 * @param statusChangeDate the statusChangeDate to set
	 */
	public void setStatusChangeDate(Date statusChangeDate) {
		this.statusChangeDate = statusChangeDate;
	}

	/**
	 * @return the accessControlList
	 */
	public String getAccessControlList() {
		return accessControlList;
	}

	/**
	 * @param accessControlList the accessControlList to set
	 */
	public void setAccessControlList(String accessControlList) {
		this.accessControlList = accessControlList;
	}

	/**
	 * @return the startDateOffSet
	 */
	public Integer getStartDateOffSet() {
		return startDateOffSet;
	}

	/**
	 * @param startDateOffSet the startDateOffSet to set
	 */
	public void setStartDateOffSet(Integer startDateOffSet) {
		this.startDateOffSet = startDateOffSet;
	}

	/**
	 * @return the recNumPerPage
	 */
	public Integer getRecNumPerPage() {
		return recNumPerPage;
	}

	/**
	 * @param recNumPerPage the recNumPerPage to set
	 */
	public void setRecNumPerPage(Integer recNumPerPage) {
		this.recNumPerPage = recNumPerPage;
	}

	/**
	 * @return the dateModified
	 */
	public Date getDateModified() {
		return dateModified;
	}

	/**
	 * @param dateModified the dateModified to set
	 */
	public void setDateModified(Date dateModified) {
		this.dateModified = dateModified;
	}

	/**
	 * @return the remark
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * @param remark the remark to set
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * @return the timeStampFormat
	 */
	public String getTimeStampFormat() {
		return timeStampFormat;
	}

	/**
	 * @param timeStampFormat the timeStampFormat to set
	 */
	public void setTimeStampFormat(String timeStampFormat) {
		this.timeStampFormat = timeStampFormat;
	}

	/**
	 * @return the reportTimeZoneLocal
	 */
	public String getReportTimeZoneLocal() {
		return reportTimeZoneLocal;
	}

	/**
	 * @param reportTimeZoneLocal the reportTimeZoneLocal to set
	 */
	public void setReportTimeZoneLocal(String reportTimeZoneLocal) {
		this.reportTimeZoneLocal = reportTimeZoneLocal;
	}

	/**
	 * @return the profileInfo
	 */
	public String getProfileInfo() {
		return profileInfo;
	}

	/**
	 * @param profileInfo the profileInfo to set
	 */
	public void setProfileInfo(String profileInfo) {
		this.profileInfo = profileInfo;
	}

	/**
	 * @return the localeId
	 */
	public Integer getLocaleId() {
		return localeId;
	}

	/**
	 * @param localeId the localeId to set
	 */
	public void setLocaleId(Integer localeId) {
		this.localeId = localeId;
	}

	/**
	 * @return the authorizedBanks
	 */
	public byte[] getAuthorizedBanks() {
		return authorizedBanks;
	}

	/**
	 * @param authorizedBanks the authorizedBanks to set
	 */
	public void setAuthorizedBanks(byte[] authorizedBanks) {
		this.authorizedBanks = authorizedBanks;
	}

	/**
	 * @return the templateName
	 */
	public String getTemplateName() {
		return templateName;
	}

	/**
	 * @param templateName the templateName to set
	 */
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * @param middleName the middleName to set
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	/**
	 * @return the firstLogIntegerime
	 */
	public Date getFirstLogIntegerime() {
		return firstLogIntegerime;
	}

	/**
	 * @param firstLogIntegerime the firstLogIntegerime to set
	 */
	public void setFirstLogIntegerime(Date firstLogIntegerime) {
		this.firstLogIntegerime = firstLogIntegerime;
	}

	/**
	 * @return the blocksiMultlogin
	 */
	public Integer getBlocksiMultlogin() {
		return blocksiMultlogin;
	}

	/**
	 * @param blocksiMultlogin the blocksiMultlogin to set
	 */
	public void setBlocksiMultlogin(Integer blocksiMultlogin) {
		this.blocksiMultlogin = blocksiMultlogin;
	}

	/**
	 * @return the httpSessionId
	 */
	public String getHttpSessionId() {
		return httpSessionId;
	}

	/**
	 * @param httpSessionId the httpSessionId to set
	 */
	public void setHttpSessionId(String httpSessionId) {
		this.httpSessionId = httpSessionId;
	}

	/**
	 * @return the maxValForLastPwdList
	 */
	public Integer getMaxValForLastPwdList() {
		return maxValForLastPwdList;
	}

	/**
	 * @param maxValForLastPwdList the maxValForLastPwdList to set
	 */
	public void setMaxValForLastPwdList(Integer maxValForLastPwdList) {
		this.maxValForLastPwdList = maxValForLastPwdList;
	}

	/**
	 * @return the lastPwdList
	 */
	public String getLastPwdList() {
		return lastPwdList;
	}

	/**
	 * @param lastPwdList the lastPwdList to set
	 */
	public void setLastPwdList(String lastPwdList) {
		this.lastPwdList = lastPwdList;
	}

	/**
	 * @return the jobTitle
	 */
	public String getJobTitle() {
		return jobTitle;
	}

	/**
	 * @param jobTitle the jobTitle to set
	 */
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	/**
	 * @return the superVisorName
	 */
	public String getSuperVisorName() {
		return superVisorName;
	}

	/**
	 * @param superVisorName the superVisorName to set
	 */
	public void setSuperVisorName(String superVisorName) {
		this.superVisorName = superVisorName;
	}

	/**
	 * @return the emailAddress
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * @param emailAddress the emailAddress to set
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the hIntegerQuestion
	 */
	public String getHIntegerQuestion() {
		return hIntegerQuestion;
	}

	/**
	 * @param hIntegerQuestion the hIntegerQuestion to set
	 */
	public void setHIntegerQuestion(String hIntegerQuestion) {
		this.hIntegerQuestion = hIntegerQuestion;
	}

	/**
	 * @return the hIntegerAnswer
	 */
	public String getHIntegerAnswer() {
		return hIntegerAnswer;
	}

	/**
	 * @param hIntegerAnswer the hIntegerAnswer to set
	 */
	public void setHIntegerAnswer(String hIntegerAnswer) {
		this.hIntegerAnswer = hIntegerAnswer;
	}

	/**
	 * @return the updateProfileAtLogin
	 */
	public Integer getUpdateProfileAtLogin() {
		return updateProfileAtLogin;
	}

	/**
	 * @param updateProfileAtLogin the updateProfileAtLogin to set
	 */
	public void setUpdateProfileAtLogin(Integer updateProfileAtLogin) {
		this.updateProfileAtLogin = updateProfileAtLogin;
	}

	/**
	 * @return the processorName
	 */
	public String getProcessorName() {
		return processorName;
	}

	/**
	 * @param processorName the processorName to set
	 */
	public void setProcessorName(String processorName) {
		this.processorName = processorName;
	}

	/**
	 * @return the subProcessorName
	 */
	public String getSubProcessorName() {
		return subProcessorName;
	}

	/**
	 * @param subProcessorName the subProcessorName to set
	 */
	public void setSubProcessorName(String subProcessorName) {
		this.subProcessorName = subProcessorName;
	}

	/**
	 * @return the authType
	 */
	public Integer getAuthType() {
		return authType;
	}

	/**
	 * @param authType the authType to set
	 */
	public void setAuthType(Integer authType) {
		this.authType = authType;
	}

	/**
	 * @return the is2FAAuthenabled
	 */
	public Integer getIs2FAAuthenabled() {
		return is2FAAuthenabled;
	}

	/**
	 * @param is2faAuthenabled the is2FAAuthenabled to set
	 */
	public void setIs2FAAuthenabled(Integer is2faAuthenabled) {
		is2FAAuthenabled = is2faAuthenabled;
	}

	/**
	 * @return the passwordEx
	 */
	public String getPasswordEx() {
		return passwordEx;
	}

	/**
	 * @param passwordEx the passwordEx to set
	 */
	public void setPasswordEx(String passwordEx) {
		this.passwordEx = passwordEx;
	}

	/**
	 * @return the passwordType
	 */
	public Integer getPasswordType() {
		return passwordType;
	}

	/**
	 * @param passwordType the passwordType to set
	 */
	public void setPasswordType(Integer passwordType) {
		this.passwordType = passwordType;
	}

	/**
	 * @return the lastPwdListEx
	 */
	public String getLastPwdListEx() {
		return lastPwdListEx;
	}

	/**
	 * @param lastPwdListEx the lastPwdListEx to set
	 */
	public void setLastPwdListEx(String lastPwdListEx) {
		this.lastPwdListEx = lastPwdListEx;
	}

	/**
	 * @return the passwordLastUpdated
	 */
	public Date getPasswordLastUpdated() {
		return passwordLastUpdated;
	}

	/**
	 * @param passwordLastUpdated the passwordLastUpdated to set
	 */
	public void setPasswordLastUpdated(Date passwordLastUpdated) {
		this.passwordLastUpdated = passwordLastUpdated;
	}

	/**
	 * @return the collectAdminContactDetails
	 */
	public Integer getCollectAdminContactDetails() {
		return collectAdminContactDetails;
	}

	/**
	 * @param collectAdminContactDetails the collectAdminContactDetails to set
	 */
	public void setCollectAdminContactDetails(Integer collectAdminContactDetails) {
		this.collectAdminContactDetails = collectAdminContactDetails;
	}

	/**
	 * @return the deliveryChannel
	 */
	public Integer getDeliveryChannel() {
		return deliveryChannel;
	}

	/**
	 * @param deliveryChannel the deliveryChannel to set
	 */
	public void setDeliveryChannel(Integer deliveryChannel) {
		this.deliveryChannel = deliveryChannel;
	}

	/**
	 * @return the secondaryBankId
	 */
	public Integer getSecondaryBankId() {
		return secondaryBankId;
	}

	/**
	 * @param secondaryBankId the secondaryBankId to set
	 */
	public void setSecondaryBankId(Integer secondaryBankId) {
		this.secondaryBankId = secondaryBankId;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ArESAdmin [aresadminpk=" + aresadminpk + ", password=" + password + ", dateCreated=" + dateCreated
				+ ", adminLevel=" + adminLevel + ", status=" + status + ", failedStrikes=" + failedStrikes
				+ ", firstFailedStrikeTime=" + firstFailedStrikeTime + ", lastLongTime=" + lastLongTime
				+ ", tempPasswordDuration=" + tempPasswordDuration + ", statusChangeDate=" + statusChangeDate
				+ ", accessControlList=" + accessControlList + ", startDateOffSet=" + startDateOffSet
				+ ", recNumPerPage=" + recNumPerPage + ", dateModified=" + dateModified + ", remark=" + remark
				+ ", timeStampFormat=" + timeStampFormat + ", reportTimeZoneLocal=" + reportTimeZoneLocal
				+ ", profileInfo=" + profileInfo + ", localeId=" + localeId + ", authorizedBanks="
				+ Arrays.toString(authorizedBanks) + ", templateName=" + templateName + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", middleName=" + middleName + ", firstLogIntegerime=" + firstLogIntegerime
				+ ", blocksiMultlogin=" + blocksiMultlogin + ", httpSessionId=" + httpSessionId
				+ ", maxValForLastPwdList=" + maxValForLastPwdList + ", lastPwdList=" + lastPwdList + ", jobTitle="
				+ jobTitle + ", superVisorName=" + superVisorName + ", emailAddress=" + emailAddress + ", phoneNumber="
				+ phoneNumber + ", hIntegerQuestion=" + hIntegerQuestion + ", hIntegerAnswer=" + hIntegerAnswer
				+ ", updateProfileAtLogin=" + updateProfileAtLogin + ", processorName=" + processorName
				+ ", subProcessorName=" + subProcessorName + ", authType=" + authType + ", is2FAAuthenabled="
				+ is2FAAuthenabled + ", passwordEx=" + passwordEx + ", passwordType=" + passwordType
				+ ", lastPwdListEx=" + lastPwdListEx + ", passwordLastUpdated=" + passwordLastUpdated
				+ ", collectAdminContactDetails=" + collectAdminContactDetails + ", deliveryChannel=" + deliveryChannel
				+ ", secondaryBankId=" + secondaryBankId + "]";
	}


}