package com.arcot.tm.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
@Embeddable
public class ArAdminLevelPk implements Serializable{
	

	@Column(name="ADMINLEVEL")
	private Integer  adminLevel;
	

	@Column(name="BANKID")
	private Integer  bankId;


	/**
	 * @return the adminLevel
	 */
	public Integer getAdminLevel() {
		return adminLevel;
	}


	/**
	 * @param adminLevel the adminLevel to set
	 */
	public void setAdminLevel(Integer adminLevel) {
		this.adminLevel = adminLevel;
	}


	/**
	 * @return the bankId
	 */
	public Integer getBankId() {
		return bankId;
	}


	/**
	 * @param bankId the bankId to set
	 */
	public void setBankId(Integer bankId) {
		this.bankId = bankId;
	}


}
