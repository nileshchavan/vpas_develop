package com.arcot.tm.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ArAminBankPk implements Serializable {

	@Column(name="BANKID")
	private Integer bankId;
	
	@Column(name="ADMINNAME")
	private String adminName;
	
	@Column(name="SECONDARYBANKID")
	private Integer secondaryBankId=0;

	/**
	 * @return the bankId
	 */
	public Integer getBankId() {
		return bankId;
	}

	/**
	 * @param bankId the bankId to set
	 */
	public void setBankId(Integer bankId) {
		this.bankId = bankId;
	}

	/**
	 * @return the adminName
	 */
	public String getAdminName() {
		return adminName;
	}

	/**
	 * @param adminName the adminName to set
	 */
	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}

	/**
	 * @return the secondaryBankId
	 */
	public Integer getSecondaryBankId() {
		return secondaryBankId;
	}

	/**
	 * @param secondaryBankId the secondaryBankId to set
	 */
	public void setSecondaryBankId(Integer secondaryBankId) {
		this.secondaryBankId = secondaryBankId;
	}
	
	
	
}
