package com.arcot.tm.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name = "ARESBANKENROLLCONFIG")
@Entity

public class AresBankEnrollingConfig {

	
	@EmbeddedId
	private AresBankEnrollingConfigPk aresBankEnrollingConfigPk; 
	
	
	@Column(name = "FIELDTYPE")
	private String fieldType;
	
	@Column(name = "FIELDOPTIONAL")
	private String fieldOptional="M";

	/**
	 * @return the aresBankEnrollingConfigPk
	 */
	public AresBankEnrollingConfigPk getAresBankEnrollingConfigPk() {
		return aresBankEnrollingConfigPk;
	}

	/**
	 * @param aresBankEnrollingConfigPk the aresBankEnrollingConfigPk to set
	 */
	public void setAresBankEnrollingConfigPk(AresBankEnrollingConfigPk aresBankEnrollingConfigPk) {
		this.aresBankEnrollingConfigPk = aresBankEnrollingConfigPk;
	}

	/**
	 * @return the fieldType
	 */
	public String getFieldType() {
		return fieldType;
	}

	/**
	 * @param fieldType the fieldType to set
	 */
	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}

	/**
	 * @return the fieldOptional
	 */
	public String getFieldOptional() {
		return fieldOptional;
	}

	/**
	 * @param fieldOptional the fieldOptional to set
	 */
	public void setFieldOptional(String fieldOptional) {
		this.fieldOptional = fieldOptional;
	}
	
	
}
