package com.arcot.tm.entities;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="ARADMINCOUNTRY")
public class ArAdminCountry {
	
	@EmbeddedId
	private ArAdminCountryPk arAdminCountryPk;
	
	@Column(name="SECONDARYBANKID")
	private Integer secondaryBankId;
	
	public ArAdminCountryPk getArAdminCountryPk() {
		return arAdminCountryPk;
	}
	public void setArAdminCountryPk(ArAdminCountryPk arAdminCountryPk) {
		this.arAdminCountryPk = arAdminCountryPk;
	}
	public Integer getSecondaryBankId() {
		return secondaryBankId;
	}
	public void setSecondaryBankId(Integer secondaryBankId) {
		this.secondaryBankId = secondaryBankId;
	}
	
}
