package com.arcot.tm.entities;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

@Table(name = "ARBANKINFO",uniqueConstraints=@UniqueConstraint(columnNames ={"BANKDIRNAME"}))
@Entity
public class ArBankInfo {
	
	@Column(name = "AMORGNAME")
	private String  AmorgName=null;

	@Column(name = "ARCOTEMVACCOUNTTYPE")
	private String  ArcotEmvacCountType;
	
	@Column(name = "ARCOTOTPENABLED")
	private Integer  ArcotOtpEnabled=0;
	
	@Column(name = "ARCOTOTPIMAGESRELATIVEPATH")
	private String  ArcotOtpImagesRelativePath=null;
	
	@Column(name = "ARCOTOTPPOLICYNAME")
	private String  ArcotOtpPolicyName;
	@Transient
	public static int BLOCK_MULTIPLE_LOGIN = 1;
	@Transient
	public static int DONOT_BLOCK_MULTIPLE_LOGIN = 0;
	
	@Column(name = "ARCOTOTPPROFILENAME")
	private String  ArcotOtpProfileName;
	
	@Column(name = "AUTHMINDERFAILOVEROPTION")
	private Integer  AuthMinderFailOverOption=0;
	

	@Column(name = "AUTHRESULTKEYIN")
	private String  AuthResultKeyIn;
	
	
	@Id
	@Column(name = "BANKID")
	private Integer bankId;
	
	@Column(name = "BANKKEY")
	private String  bankKey;
	 
	
	@Column(name = "BANKKEYIN")
	private String  bankKeyIn;
	
	@Column(name = "BANKNAME")
	private String  bankName;
	
	@Column(name = "BILLINGCODE")
	private Integer   billingCode=0;
	
	
	@Column(name = "BILLING_CATEGORY")
	private String  billingCategory;
	
	
	@Column(name = "BANKDIRNAME")
	private String  BankDirName;
	
	@Column(name = "BRANDRFINERMEDIATEPAGE")
	private Integer  brandRfIntermediatePage;
	
	@Column(name = "CALLOUTAUTHMETHOD")
	private Integer  callOutAuthMethod;
	
	@Column(name= "CASEPRIORITIZATION")
	private Integer casePrioritization;
	
	@Column(name= "CHPASSWORDEXPIRYDURATION")
	private Integer cHPasswordExpiryDuration=0;
	
	@Column(name= "CHPASSWORDEXPIRYPOLICY")
	private Integer cHPasswordExpiryPolicy=0;
	
	@Column(name= "CHPASSWORDPARAMS")
	private String cHPasswordParams;
	
	
	@Column(name= "COLLECTEXTENDEDDDNA")
	private Integer collectExtendedDDNA =0;
	
	@Column(name= "CONFIGLIST")
    private String configList;
	
	@Column(name= "COOKIETYPE")
	private Integer cookieType=0;
	
	@Column(name= "COUNTRY")
	private Integer country;

	@Transient
	public int RFINTERMEDIATEPAGE = 0; // 0 - Disabled, 1 - Enabled

	
	@Column(name= "CUSTOMERID")
	private String customerId;
	
	@Column(name= "CVVDESKEYA")
	private String cvvDesKeyA;
	
	@Column(name= "CVVDESKEYB")
	private String cvvDesKeyB;
	
	
	@Column(name= "CVVKEYIND")
	private String cvvKeyInd;
	
	@Column(name= "DATECREATED")
	private Date dateCreated;
	
	

	@Column(name= "DATEDELETED")
	private Date dateDeleted;
	
	@Column(name= "DATEFORMAT")
	private String dateFormat = "YY:MM:DD";
	
	
	@Column(name= "DATEORDER")
	private Integer  dateOrder=1;
	
	@Column(name= "DISPLAYOPTIONALINFO")
	private Integer displayOptionalInfo=0;
	
	@Column(name= "DONOTPROMPTBEHAVIOR")
	private Integer doNotPromptBehavior;
	
	@Column(name="DUALAUTHREQUIREDATTXCOMMIT")
	private Integer dualAuthRequiredAtXCommit;
	
	@Column(name= "ENABLEABRIDGEDADS")
	private Integer enableABridgeDADS=1;
	@Transient
	public int CHARSTOPROMPTFORPARTIALPASSWORDS = 0;
	
	@Column(name= "ENABLEABRIDGEDFYP")
	private Integer enableABridgeDFYP=0;

	@Column(name= "ENABLEARGUS")
	private Integer enableArgus=0;
	
	@Column(name= "ENABLECASEAUTOASSIGNMENT")
	private Integer enableCaseAutoAssignment;
	
	@Column(name= "ENABLERISKFORT")
	private Integer enableRiskFort=0;
	
	@Column(name= "ENABLESCA")
    private String enableSCA="NONE";
	
	@Column(name= "ENROLLMENTLOCKINGPOLICY")
	private Integer enrollmentLockingPolicy=0;
	
	@Column(name= "ENROLLMENTOFLOCKEDCARD")
	private Integer enrollmentOfLockedCard=0;
	
	@Column(name= "GDPRENABLE")
	private Integer gdprEnable=0;
	
	
	
	@Column(name= "HAWITHBACKUPSERVERS")
	private Integer haWithBackUpServers;
	
	@Column(name= "HINTREQUIRED")
	private Integer hintRequired = 0;
	
	@Column(name= "IPGSISON")
	private Integer ipgsison = 1;
	
	@Column(name= "IS2FAAUTHENABLED")
	private Integer is2FaAuthEnabled;
	
	@Column(name= "ISSUERDISPLAYNAME")
	private String issuerDisplayName;
	
	@Column(name= "LOCALEID")
	private Integer localeId;
	
	
	@Column(name= "LOGPREVIOUSTXNDETAILS")
	private Integer logPreviousXNDetails;
	
	@Column(name= "LOGRFSTATUS")
	private Integer logRFStatus;
	
	@Column(name= "LOGVEREQPAREQTIME")
	private Integer logVeReqPareqTime=0;
	
	@Column(name= "MAXCHANGEPWDFAILURES")
	private Integer maxChangePasswordFailures=0;
	
	@Column(name= "MAXTXNSWITHOUTAOTP")
	private Integer maxTXNSwithoutOtp;
	
	@Column(name= "MAXVALFORLASTPWDLIST")
	private Integer maxValForLastPwdList=0;
	
	@Column(name= "NAMEFIELDCOUNT")
	private Integer nameFieldCount=3;
	
	@Transient
	public static int BLOCK_ENROLLMENT = 1;
	
	
	@Column(name= "PAREQWAITTIMEBUFFER")
	private Integer paReqWaitTimeBuffer=120;
	
	@Column(name= "PARTIALPASSWORDPOLICY")
	private Integer partialPasswordPolicy=0;
	
	@Column(name= "PASSWORDSTORAGEALGORITHM")
	private Integer passwordStorageAlgorithm=1;
	@Column(name= "PASSWORDUSAGEPOLICY")
	private Integer passwordUsagePolicy = 0;
	
	@Column(name= "PERSISTTXNSTATE")
	private Integer persistTXNState; 
	
	@Column(name= "PIITYPE")
	private String piiType;
	
	@Column(name= "PROCESSORDATA")
	private String processorData;
	
	@Column(name= "PROCESSORINFO")
	private String processorInfo;
	
	@Column(name= "PROCESSORNAME")
	private String processorName;
	
	@Column(name= "RADOWNADVISE")
	private Integer raDownAdvise=0;
	
	@Column(name= "REPORTDATEFORMAT")
	private String reportDateFormat="MM-dd-yyy";
	
	@Column(name= "REPORTNUMBEROFRECORDSPERPAGE")
	private Integer reportNumberOfRecordsPerPage=10;
	
	@Column(name= "REPORTSTARTDATEOFFSET")
	private Integer reportStartDateOffset=-1;
	
	@Column(name= "REPORTTIMESTAMPFORMAT")
	private String reportTimeStampFormat="yyyy-MM-dd hh:mm:ss a z";
	
	@Column(name= "REPORTTIMEZONEDATABASE")
	private String reportTimeZoneDataBase="GMT";
	
	@Column(name= "REPORTTIMEZONELOCAL")
	private String reportTimeZoneLocal;
	
	@Column(name= "REREGISTERALLOWED")
	private Integer reRegisterAllowed=1;
	@Column(name= "RETAINSHA1PASSWORD")
	private Integer  retainSHA1Password=1;
	
	@Column(name= "RFIntegerERMEDIATEPAGE")
	private Integer rfIntegerermediatePage;
	
	@Column(name= "SENDTEMPPASSWORDTOCARDHOLDER")
	private Integer sendTempPasswordToCardHolder;
	
	@Column(name= "SIMULTANEOUSCHLOGIN")
	private Integer simultaneousChLogin;
	
	
	@Column(name= "STATUS")
	private Integer status=0;
	
	@Column(name= "STEP1HASOPTIONALFIELDS")
	private Integer step1HasOptionalFields=1;
	
	@Column(name= "SUBPROCESSORNAME")
	private String subProcessorName;
	
	@Column(name= "SUPPORTUPGRADEDCAPUI")
	private Integer supportUpgradedCAPUI;
	
	@Column(name= "SUPPRESSCHCONTACTDETAILSINPUT")
	private Integer suppressCHContactDetailsInput;
	
	@Column(name= "SYMBOLDISPLAY")
	private Integer symbolDisplay=1;
	
	@Column(name= "SYMBOLLINK")
	private Integer symbolLink=1;
	
	@Column(name= "TEMPLATE_NAME")
	private String templateName;
	
	@Column(name= "TEMPPASSWORDDURATION")
	private Integer tempPasswordDuration = 2;
	
	@Column(name= "TRUSTEDBENEFICIARY")
	private String trustedBeneficiary="NONE";
	
	/**
	 * @return the bLOCK_MULTIPLE_LOGIN
	 */
	public static int getBLOCK_MULTIPLE_LOGIN() {
		return BLOCK_MULTIPLE_LOGIN;
	}




	/**
	 * @param bLOCK_MULTIPLE_LOGIN the bLOCK_MULTIPLE_LOGIN to set
	 */
	public static void setBLOCK_MULTIPLE_LOGIN(int bLOCK_MULTIPLE_LOGIN) {
		BLOCK_MULTIPLE_LOGIN = bLOCK_MULTIPLE_LOGIN;
	}




	/**
	 * @return the dONOT_BLOCK_MULTIPLE_LOGIN
	 */
	public static int getDONOT_BLOCK_MULTIPLE_LOGIN() {
		return DONOT_BLOCK_MULTIPLE_LOGIN;
	}




	/**
	 * @param dONOT_BLOCK_MULTIPLE_LOGIN the dONOT_BLOCK_MULTIPLE_LOGIN to set
	 */
	public static void setDONOT_BLOCK_MULTIPLE_LOGIN(int dONOT_BLOCK_MULTIPLE_LOGIN) {
		DONOT_BLOCK_MULTIPLE_LOGIN = dONOT_BLOCK_MULTIPLE_LOGIN;
	}




	/**
	 * @return the cOLLECTMCDDNA
	



	/**
	 * @param cOLLECTMCDDNA the cOLLECTMCDDNA to set
	 */
	




	




	/**
	 * @return the rFINTERMEDIATEPAGE
	 */
	public int getRFINTERMEDIATEPAGE() {
		return RFINTERMEDIATEPAGE;
	}




	/**
	 * @param rFINTERMEDIATEPAGE the rFINTERMEDIATEPAGE to set
	 */
	public void setRFINTERMEDIATEPAGE(int rFINTERMEDIATEPAGE) {
		RFINTERMEDIATEPAGE = rFINTERMEDIATEPAGE;
	}




	/**
	 * @return the cHARSTOPROMPTFORPARTIALPASSWORDS
	 */
	public int getCHARSTOPROMPTFORPARTIALPASSWORDS() {
		return CHARSTOPROMPTFORPARTIALPASSWORDS;
	}




	/**
	 * @param cHARSTOPROMPTFORPARTIALPASSWORDS the cHARSTOPROMPTFORPARTIALPASSWORDS to set
	 */
	public void setCHARSTOPROMPTFORPARTIALPASSWORDS(int cHARSTOPROMPTFORPARTIALPASSWORDS) {
		CHARSTOPROMPTFORPARTIALPASSWORDS = cHARSTOPROMPTFORPARTIALPASSWORDS;
	}




	/**
	 * @return the bLOCK_ENROLLMENT
	 */
	public static int getBLOCK_ENROLLMENT() {
		return BLOCK_ENROLLMENT;
	}




	/**
	 * @param bLOCK_ENROLLMENT the bLOCK_ENROLLMENT to set
	 */
	public static void setBLOCK_ENROLLMENT(int bLOCK_ENROLLMENT) {
		BLOCK_ENROLLMENT = bLOCK_ENROLLMENT;
	}








	@Column(name= "TTPCONFIGID")
	private Integer ttpConfigId;
	
	
	@Column(name= "TWOSTEPLOGINENABLED")
	private Integer twoStepLoginEnabled;
	
	@Column(name= "UACHECKENABLED")
	private Integer uaCheckedEnabled;
	
	
	@Column(name= "UPGRADED")
	private Integer upgraded=1;
	
	@Column(name= "UPLOADKEY")
	private String  uploadKey;
	
	
	@Column(name= "USECVVFORCAVV")
	private Integer  useCvvForCAVV=0;
	
	
	@Column(name= "USERENCODING")
	private String  userEncoding;
	
	

	@Column(name= "USERIDENABLED")
	private Integer  userIdEnabled;
	
	
	

	@Column(name= "VELOG")
	private Integer  veLog=0;




	/**
	 * @return the amorgName
	 */
	public String getAmorgName() {
		return AmorgName;
	}




	/**
	 * @param amorgName the amorgName to set
	 */
	public void setAmorgName(String amorgName) {
		AmorgName = amorgName;
	}




	/**
	 * @return the arcotEmvacCountType
	 */
	public String getArcotEmvacCountType() {
		return ArcotEmvacCountType;
	}




	/**
	 * @param arcotEmvacCountType the arcotEmvacCountType to set
	 */
	public void setArcotEmvacCountType(String arcotEmvacCountType) {
		ArcotEmvacCountType = arcotEmvacCountType;
	}




	/**
	 * @return the arcotOtpEnabled
	 */
	public Integer getArcotOtpEnabled() {
		return ArcotOtpEnabled;
	}




	/**
	 * @param arcotOtpEnabled the arcotOtpEnabled to set
	 */
	public void setArcotOtpEnabled(Integer arcotOtpEnabled) {
		ArcotOtpEnabled = arcotOtpEnabled;
	}




	/**
	 * @return the arcotOtpImagesRelativePath
	 */
	public String getArcotOtpImagesRelativePath() {
		return ArcotOtpImagesRelativePath;
	}




	/**
	 * @param arcotOtpImagesRelativePath the arcotOtpImagesRelativePath to set
	 */
	public void setArcotOtpImagesRelativePath(String arcotOtpImagesRelativePath) {
		ArcotOtpImagesRelativePath = arcotOtpImagesRelativePath;
	}




	/**
	 * @return the arcotOtpPolicyName
	 */
	public String getArcotOtpPolicyName() {
		return ArcotOtpPolicyName;
	}




	/**
	 * @param arcotOtpPolicyName the arcotOtpPolicyName to set
	 */
	public void setArcotOtpPolicyName(String arcotOtpPolicyName) {
		ArcotOtpPolicyName = arcotOtpPolicyName;
	}




	/**
	 * @return the arcotOtpProfileName
	 */
	public String getArcotOtpProfileName() {
		return ArcotOtpProfileName;
	}




	/**
	 * @param arcotOtpProfileName the arcotOtpProfileName to set
	 */
	public void setArcotOtpProfileName(String arcotOtpProfileName) {
		ArcotOtpProfileName = arcotOtpProfileName;
	}




	/**
	 * @return the authMinderFailOverOption
	 */
	public Integer getAuthMinderFailOverOption() {
		return AuthMinderFailOverOption;
	}




	/**
	 * @param authMinderFailOverOption the authMinderFailOverOption to set
	 */
	public void setAuthMinderFailOverOption(Integer authMinderFailOverOption) {
		AuthMinderFailOverOption = authMinderFailOverOption;
	}




	/**
	 * @return the authResultKeyIn
	 */
	public String getAuthResultKeyIn() {
		return AuthResultKeyIn;
	}




	/**
	 * @param authResultKeyIn the authResultKeyIn to set
	 */
	public void setAuthResultKeyIn(String authResultKeyIn) {
		AuthResultKeyIn = authResultKeyIn;
	}




	/**
	 * @return the bankId
	 */
	public Integer getBankId() {
		return bankId;
	}




	/**
	 * @param bankId the bankId to set
	 */
	public void setBankId(Integer bankId) {
		this.bankId = bankId;
	}




	/**
	 * @return the bankKey
	 */
	public String getBankKey() {
		return bankKey;
	}




	/**
	 * @param bankKey the bankKey to set
	 */
	public void setBankKey(String bankKey) {
		this.bankKey = bankKey;
	}




	/**
	 * @return the bankKeyIn
	 */
	public String getBankKeyIn() {
		return bankKeyIn;
	}




	/**
	 * @param bankKeyIn the bankKeyIn to set
	 */
	public void setBankKeyIn(String bankKeyIn) {
		this.bankKeyIn = bankKeyIn;
	}




	/**
	 * @return the bankName
	 */
	public String getBankName() {
		return bankName;
	}




	/**
	 * @param bankName the bankName to set
	 */
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}




	/**
	 * @return the billingCode
	 */
	public Integer getBillingCode() {
		return billingCode;
	}




	/**
	 * @param billingCode the billingCode to set
	 */
	public void setBillingCode(Integer billingCode) {
		this.billingCode = billingCode;
	}




	/**
	 * @return the billingCategory
	 */
	public String getBillingCategory() {
		return billingCategory;
	}




	/**
	 * @param billingCategory the billingCategory to set
	 */
	public void setBillingCategory(String billingCategory) {
		this.billingCategory = billingCategory;
	}




	



	/**
	 * @return the brandRfIntermediatePage
	 */
	public Integer getBrandRfIntermediatePage() {
		return brandRfIntermediatePage;
	}




	/**
	 * @param brandRfIntermediatePage the brandRfIntermediatePage to set
	 */
	public void setBrandRfIntermediatePage(Integer brandRfIntermediatePage) {
		this.brandRfIntermediatePage = brandRfIntermediatePage;
	}




	/**
	 * @return the callOutAuthMethod
	 */
	public Integer getCallOutAuthMethod() {
		return callOutAuthMethod;
	}




	/**
	 * @param callOutAuthMethod the callOutAuthMethod to set
	 */
	public void setCallOutAuthMethod(Integer callOutAuthMethod) {
		this.callOutAuthMethod = callOutAuthMethod;
	}




	/**
	 * @return the casePrioritization
	 */
	public Integer getCasePrioritization() {
		return casePrioritization;
	}




	/**
	 * @param casePrioritization the casePrioritization to set
	 */
	public void setCasePrioritization(Integer casePrioritization) {
		this.casePrioritization = casePrioritization;
	}




	/**
	 * @return the cHPasswordExpiryDuration
	 */
	public Integer getcHPasswordExpiryDuration() {
		return cHPasswordExpiryDuration;
	}




	/**
	 * @param cHPasswordExpiryDuration the cHPasswordExpiryDuration to set
	 */
	public void setcHPasswordExpiryDuration(Integer cHPasswordExpiryDuration) {
		this.cHPasswordExpiryDuration = cHPasswordExpiryDuration;
	}




	/**
	 * @return the cHPasswordExpiryPolicy
	 */
	public Integer getcHPasswordExpiryPolicy() {
		return cHPasswordExpiryPolicy;
	}




	/**
	 * @param cHPasswordExpiryPolicy the cHPasswordExpiryPolicy to set
	 */
	public void setcHPasswordExpiryPolicy(Integer cHPasswordExpiryPolicy) {
		this.cHPasswordExpiryPolicy = cHPasswordExpiryPolicy;
	}




	/**
	 * @return the cHPasswordParams
	 */
	public String getcHPasswordParams() {
		return cHPasswordParams;
	}




	/**
	 * @param cHPasswordParams the cHPasswordParams to set
	 */
	public void setcHPasswordParams(String cHPasswordParams) {
		this.cHPasswordParams = cHPasswordParams;
	}




	/**
	 * @return the collectExtendedDDNA
	 */
	public Integer getCollectExtendedDDNA() {
		return collectExtendedDDNA;
	}




	/**
	 * @param collectExtendedDDNA the collectExtendedDDNA to set
	 */
	public void setCollectExtendedDDNA(Integer collectExtendedDDNA) {
		this.collectExtendedDDNA = collectExtendedDDNA;
	}




	/**
	 * @return the configList
	 */
	public String getConfigList() {
		return configList;
	}




	/**
	 * @param configList the configList to set
	 */
	public void setConfigList(String configList) {
		this.configList = configList;
	}




	/**
	 * @return the cookieType
	 */
	public Integer getCookieType() {
		return cookieType;
	}




	/**
	 * @param cookieType the cookieType to set
	 */
	public void setCookieType(Integer cookieType) {
		this.cookieType = cookieType;
	}




	/**
	 * @return the country
	 */
	public Integer getCountry() {
		return country;
	}




	/**
	 * @param country the country to set
	 */
	public void setCountry(Integer country) {
		this.country = country;
	}




	/**
	 * @return the customerId
	 */
	public String getCustomerId() {
		return customerId;
	}




	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}




	/**
	 * @return the cvvDesKeyA
	 */
	public String getCvvDesKeyA() {
		return cvvDesKeyA;
	}




	/**
	 * @param cvvDesKeyA the cvvDesKeyA to set
	 */
	public void setCvvDesKeyA(String cvvDesKeyA) {
		this.cvvDesKeyA = cvvDesKeyA;
	}




	/**
	 * @return the cvvDesKeyB
	 */
	public String getCvvDesKeyB() {
		return cvvDesKeyB;
	}




	/**
	 * @param cvvDesKeyB the cvvDesKeyB to set
	 */
	public void setCvvDesKeyB(String cvvDesKeyB) {
		this.cvvDesKeyB = cvvDesKeyB;
	}




	/**
	 * @return the cvvKeyInd
	 */
	public String getCvvKeyInd() {
		return cvvKeyInd;
	}




	/**
	 * @param cvvKeyInd the cvvKeyInd to set
	 */
	public void setCvvKeyInd(String cvvKeyInd) {
		this.cvvKeyInd = cvvKeyInd;
	}




	/**
	 * @return the dateCreated
	 */
	public Date getDateCreated() {
		return dateCreated;
	}




	/**
	 * @param dateCreated the dateCreated to set
	 */
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}




	/**
	 * @return the dateDeleted
	 */
	public Date getDateDeleted() {
		return dateDeleted;
	}




	/**
	 * @param dateDeleted the dateDeleted to set
	 */
	public void setDateDeleted(Date dateDeleted) {
		this.dateDeleted = dateDeleted;
	}




	/**
	 * @return the dateFormat
	 */
	public String getDateFormat() {
		return dateFormat;
	}




	/**
	 * @param dateFormat the dateFormat to set
	 */
	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}




	/**
	 * @return the dateOrder
	 */
	public Integer getDateOrder() {
		return dateOrder;
	}




	/**
	 * @param dateOrder the dateOrder to set
	 */
	public void setDateOrder(Integer dateOrder) {
		this.dateOrder = dateOrder;
	}




	/**
	 * @return the displayOptionalInfo
	 */
	public Integer getDisplayOptionalInfo() {
		return displayOptionalInfo;
	}




	/**
	 * @param displayOptionalInfo the displayOptionalInfo to set
	 */
	public void setDisplayOptionalInfo(Integer displayOptionalInfo) {
		this.displayOptionalInfo = displayOptionalInfo;
	}




	/**
	 * @return the doNotPromptBehavior
	 */
	public Integer getDoNotPromptBehavior() {
		return doNotPromptBehavior;
	}




	/**
	 * @param doNotPromptBehavior the doNotPromptBehavior to set
	 */
	public void setDoNotPromptBehavior(Integer doNotPromptBehavior) {
		this.doNotPromptBehavior = doNotPromptBehavior;
	}




	/**
	 * @return the dualAuthRequiredAtXCommit
	 */
	public Integer getDualAuthRequiredAtXCommit() {
		return dualAuthRequiredAtXCommit;
	}




	/**
	 * @param dualAuthRequiredAtXCommit the dualAuthRequiredAtXCommit to set
	 */
	public void setDualAuthRequiredAtXCommit(Integer dualAuthRequiredAtXCommit) {
		this.dualAuthRequiredAtXCommit = dualAuthRequiredAtXCommit;
	}




	/**
	 * @return the enableABridgeDADS
	 */
	public Integer getEnableABridgeDADS() {
		return enableABridgeDADS;
	}




	/**
	 * @param enableABridgeDADS the enableABridgeDADS to set
	 */
	public void setEnableABridgeDADS(Integer enableABridgeDADS) {
		this.enableABridgeDADS = enableABridgeDADS;
	}




	/**
	 * @return the enableABridgeDFYP
	 */
	public Integer getEnableABridgeDFYP() {
		return enableABridgeDFYP;
	}




	/**
	 * @param enableABridgeDFYP the enableABridgeDFYP to set
	 */
	public void setEnableABridgeDFYP(Integer enableABridgeDFYP) {
		this.enableABridgeDFYP = enableABridgeDFYP;
	}




	/**
	 * @return the enableArgus
	 */
	public Integer getEnableArgus() {
		return enableArgus;
	}




	/**
	 * @param enableArgus the enableArgus to set
	 */
	public void setEnableArgus(Integer enableArgus) {
		this.enableArgus = enableArgus;
	}




	/**
	 * @return the enableCaseAutoAssignment
	 */
	public Integer getEnableCaseAutoAssignment() {
		return enableCaseAutoAssignment;
	}




	/**
	 * @param enableCaseAutoAssignment the enableCaseAutoAssignment to set
	 */
	public void setEnableCaseAutoAssignment(Integer enableCaseAutoAssignment) {
		this.enableCaseAutoAssignment = enableCaseAutoAssignment;
	}




	/**
	 * @return the enableRiskFort
	 */
	public Integer getEnableRiskFort() {
		return enableRiskFort;
	}




	/**
	 * @param enableRiskFort the enableRiskFort to set
	 */
	public void setEnableRiskFort(Integer enableRiskFort) {
		this.enableRiskFort = enableRiskFort;
	}




	/**
	 * @return the enableSCA
	 */
	public String getEnableSCA() {
		return enableSCA;
	}




	/**
	 * @param enableSCA the enableSCA to set
	 */
	public void setEnableSCA(String enableSCA) {
		this.enableSCA = enableSCA;
	}




	/**
	 * @return the enrollmentLockingPolicy
	 */
	public Integer getEnrollmentLockingPolicy() {
		return enrollmentLockingPolicy;
	}




	/**
	 * @param enrollmentLockingPolicy the enrollmentLockingPolicy to set
	 */
	public void setEnrollmentLockingPolicy(Integer enrollmentLockingPolicy) {
		this.enrollmentLockingPolicy = enrollmentLockingPolicy;
	}




	/**
	 * @return the enrollmentOfLockedCard
	 */
	public Integer getEnrollmentOfLockedCard() {
		return enrollmentOfLockedCard;
	}




	/**
	 * @param enrollmentOfLockedCard the enrollmentOfLockedCard to set
	 */
	public void setEnrollmentOfLockedCard(Integer enrollmentOfLockedCard) {
		this.enrollmentOfLockedCard = enrollmentOfLockedCard;
	}




	/**
	 * @return the gdprEnable
	 */
	public Integer getGdprEnable() {
		return gdprEnable;
	}




	/**
	 * @param gdprEnable the gdprEnable to set
	 */
	public void setGdprEnable(Integer gdprEnable) {
		this.gdprEnable = gdprEnable;
	}




	/**
	 * @return the haWithBackUpServers
	 */
	public Integer getHaWithBackUpServers() {
		return haWithBackUpServers;
	}




	/**
	 * @param haWithBackUpServers the haWithBackUpServers to set
	 */
	public void setHaWithBackUpServers(Integer haWithBackUpServers) {
		this.haWithBackUpServers = haWithBackUpServers;
	}




	/**
	 * @return the hIntegerRequired
	 */
	public Integer getHintRequired() {
		return hintRequired;
	}




	
	
	
	
	
	
	
	/**
	 * @param hIntegerRequired the hIntegerRequired to set
	 */
	public void setHintRequired(Integer hintRequired) {
		this.hintRequired = hintRequired;
	}




	/**
	 * @return the ipgsison
	 */
	public Integer getIpgsison() {
		return ipgsison;
	}




	/**
	 * @param ipgsison the ipgsison to set
	 */
	public void setIpgsison(Integer ipgsison) {
		this.ipgsison = ipgsison;
	}




	/**
	 * @return the is2FaAuthEnabled
	 */
	public Integer getIs2FaAuthEnabled() {
		return is2FaAuthEnabled;
	}




	/**
	 * @param is2FaAuthEnabled the is2FaAuthEnabled to set
	 */
	public void setIs2FaAuthEnabled(Integer is2FaAuthEnabled) {
		this.is2FaAuthEnabled = is2FaAuthEnabled;
	}




	/**
	 * @return the issuerDisplayName
	 */
	public String getIssuerDisplayName() {
		return issuerDisplayName;
	}




	/**
	 * @param issuerDisplayName the issuerDisplayName to set
	 */
	public void setIssuerDisplayName(String issuerDisplayName) {
		this.issuerDisplayName = issuerDisplayName;
	}




	/**
	 * @return the localeId
	 */
	public Integer getLocaleId() {
		return localeId;
	}




	/**
	 * @param localeId the localeId to set
	 */
	public void setLocaleId(Integer localeId) {
		this.localeId = localeId;
	}




	/**
	 * @return the logPreviousXNDetails
	 */
	public Integer getLogPreviousXNDetails() {
		return logPreviousXNDetails;
	}




	/**
	 * @param logPreviousXNDetails the logPreviousXNDetails to set
	 */
	public void setLogPreviousXNDetails(Integer logPreviousXNDetails) {
		this.logPreviousXNDetails = logPreviousXNDetails;
	}




	/**
	 * @return the logRFStatus
	 */
	public Integer getLogRFStatus() {
		return logRFStatus;
	}




	/**
	 * @param logRFStatus the logRFStatus to set
	 */
	public void setLogRFStatus(Integer logRFStatus) {
		this.logRFStatus = logRFStatus;
	}




	/**
	 * @return the logVeReqPareqTime
	 */
	public Integer getLogVeReqPareqTime() {
		return logVeReqPareqTime;
	}




	/**
	 * @param logVeReqPareqTime the logVeReqPareqTime to set
	 */
	public void setLogVeReqPareqTime(Integer logVeReqPareqTime) {
		this.logVeReqPareqTime = logVeReqPareqTime;
	}




	/**
	 * @return the maxChangePasswordFailures
	 */
	public Integer getMaxChangePasswordFailures() {
		return maxChangePasswordFailures;
	}




	/**
	 * @param maxChangePasswordFailures the maxChangePasswordFailures to set
	 */
	public void setMaxChangePasswordFailures(Integer maxChangePasswordFailures) {
		this.maxChangePasswordFailures = maxChangePasswordFailures;
	}




	/**
	 * @return the maxTXNSwithoutOtp
	 */
	public Integer getMaxTXNSwithoutOtp() {
		return maxTXNSwithoutOtp;
	}




	/**
	 * @param maxTXNSwithoutOtp the maxTXNSwithoutOtp to set
	 */
	public void setMaxTXNSwithoutOtp(Integer maxTXNSwithoutOtp) {
		this.maxTXNSwithoutOtp = maxTXNSwithoutOtp;
	}




	/**
	 * @return the maxValForLastPwdList
	 */
	public Integer getMaxValForLastPwdList() {
		return maxValForLastPwdList;
	}




	/**
	 * @param maxValForLastPwdList the maxValForLastPwdList to set
	 */
	public void setMaxValForLastPwdList(Integer maxValForLastPwdList) {
		this.maxValForLastPwdList = maxValForLastPwdList;
	}




	/**
	 * @return the nameFieldCount
	 */
	public Integer getNameFieldCount() {
		return nameFieldCount;
	}




	/**
	 * @param nameFieldCount the nameFieldCount to set
	 */
	public void setNameFieldCount(Integer nameFieldCount) {
		this.nameFieldCount = nameFieldCount;
	}




	/**
	 * @return the paReqWaitTimeBuffer
	 */
	public Integer getPaReqWaitTimeBuffer() {
		return paReqWaitTimeBuffer;
	}




	/**
	 * @param paReqWaitTimeBuffer the paReqWaitTimeBuffer to set
	 */
	public void setPaReqWaitTimeBuffer(Integer paReqWaitTimeBuffer) {
		this.paReqWaitTimeBuffer = paReqWaitTimeBuffer;
	}




	/**
	 * @return the partialPasswordPolicy
	 */
	public Integer getPartialPasswordPolicy() {
		return partialPasswordPolicy;
	}




	/**
	 * @param partialPasswordPolicy the partialPasswordPolicy to set
	 */
	public void setPartialPasswordPolicy(Integer partialPasswordPolicy) {
		this.partialPasswordPolicy = partialPasswordPolicy;
	}




	/**
	 * @return the passwordStorageAlgorithm
	 */
	public Integer getPasswordStorageAlgorithm() {
		return passwordStorageAlgorithm;
	}




	/**
	 * @param passwordStorageAlgorithm the passwordStorageAlgorithm to set
	 */
	public void setPasswordStorageAlgorithm(Integer passwordStorageAlgorithm) {
		this.passwordStorageAlgorithm = passwordStorageAlgorithm;
	}




	/**
	 * @return the passwordUsagePolicy
	 */
	public Integer getPasswordUsagePolicy() {
		return passwordUsagePolicy;
	}




	/**
	 * @param passwordUsagePolicy the passwordUsagePolicy to set
	 */
	public void setPasswordUsagePolicy(Integer passwordUsagePolicy) {
		this.passwordUsagePolicy = passwordUsagePolicy;
	}




	/**
	 * @return the persistTXNState
	 */
	public Integer getPersistTXNState() {
		return persistTXNState;
	}




	/**
	 * @param persistTXNState the persistTXNState to set
	 */
	public void setPersistTXNState(Integer persistTXNState) {
		this.persistTXNState = persistTXNState;
	}




	/**
	 * @return the piiType
	 */
	public String getPiiType() {
		return piiType;
	}




	/**
	 * @param piiType the piiType to set
	 */
	public void setPiiType(String piiType) {
		this.piiType = piiType;
	}




	/**
	 * @return the processorData
	 */
	public String getProcessorData() {
		return processorData;
	}




	/**
	 * @param processorData the processorData to set
	 */
	public void setProcessorData(String processorData) {
		this.processorData = processorData;
	}




	/**
	 * @return the processorInfo
	 */
	public String getProcessorInfo() {
		return processorInfo;
	}




	/**
	 * @param processorInfo the processorInfo to set
	 */
	public void setProcessorInfo(String processorInfo) {
		this.processorInfo = processorInfo;
	}




	/**
	 * @return the processorName
	 */
	public String getProcessorName() {
		return processorName;
	}




	/**
	 * @param processorName the processorName to set
	 */
	public void setProcessorName(String processorName) {
		this.processorName = processorName;
	}




	/**
	 * @return the raDownAdvise
	 */
	public Integer getRaDownAdvise() {
		return raDownAdvise;
	}




	/**
	 * @param raDownAdvise the raDownAdvise to set
	 */
	public void setRaDownAdvise(Integer raDownAdvise) {
		this.raDownAdvise = raDownAdvise;
	}




	/**
	 * @return the reportDateFormat
	 */
	public String getReportDateFormat() {
		return reportDateFormat;
	}




	/**
	 * @param reportDateFormat the reportDateFormat to set
	 */
	public void setReportDateFormat(String reportDateFormat) {
		this.reportDateFormat = reportDateFormat;
	}







	/**
	 * @return the reportNumberOfRecordsPerPage
	 */
	public Integer getReportNumberOfRecordsPerPage() {
		return reportNumberOfRecordsPerPage;
	}




	/**
	 * @param reportNumberOfRecordsPerPage the reportNumberOfRecordsPerPage to set
	 */
	public void setReportNumberOfRecordsPerPage(Integer reportNumberOfRecordsPerPage) {
		this.reportNumberOfRecordsPerPage = reportNumberOfRecordsPerPage;
	}




	/**
	 * @return the reportStartDateOffset
	 */
	public Integer getReportStartDateOffset() {
		return reportStartDateOffset;
	}




	/**
	 * @param reportStartDateOffset the reportStartDateOffset to set
	 */
	public void setReportStartDateOffset(Integer reportStartDateOffset) {
		this.reportStartDateOffset = reportStartDateOffset;
	}




	/**
	 * @return the reportTimeStampFormat
	 */
	public String getReportTimeStampFormat() {
		return reportTimeStampFormat;
	}




	/**
	 * @param reportTimeStampFormat the reportTimeStampFormat to set
	 */
	public void setReportTimeStampFormat(String reportTimeStampFormat) {
		this.reportTimeStampFormat = reportTimeStampFormat;
	}




	/**
	 * @return the reportTimeZoneDataBase
	 */
	public String getReportTimeZoneDataBase() {
		return reportTimeZoneDataBase;
	}




	/**
	 * @param reportTimeZoneDataBase the reportTimeZoneDataBase to set
	 */
	public void setReportTimeZoneDataBase(String reportTimeZoneDataBase) {
		this.reportTimeZoneDataBase = reportTimeZoneDataBase;
	}




	/**
	 * @return the reportTimeZoneLocal
	 */
	public String getReportTimeZoneLocal() {
		return reportTimeZoneLocal;
	}




	/**
	 * @param reportTimeZoneLocal the reportTimeZoneLocal to set
	 */
	public void setReportTimeZoneLocal(String reportTimeZoneLocal) {
		this.reportTimeZoneLocal = reportTimeZoneLocal;
	}




	/**
	 * @return the reRegisterAllowed
	 */
	public Integer getReRegisterAllowed() {
		return reRegisterAllowed;
	}




	/**
	 * @param reRegisterAllowed the reRegisterAllowed to set
	 */
	public void setReRegisterAllowed(Integer reRegisterAllowed) {
		this.reRegisterAllowed = reRegisterAllowed;
	}




	/**
	 * @return the retainSHA1Password
	 */
	public Integer getRetainSHA1Password() {
		return retainSHA1Password;
	}




	/**
	 * @param retainSHA1Password the retainSHA1Password to set
	 */
	public void setRetainSHA1Password(Integer retainSHA1Password) {
		this.retainSHA1Password = retainSHA1Password;
	}




	/**
	 * @return the rfIntegerermediatePage
	 */
	public Integer getRfIntegerermediatePage() {
		return rfIntegerermediatePage;
	}




	/**
	 * @param rfIntegerermediatePage the rfIntegerermediatePage to set
	 */
	public void setRfIntegerermediatePage(Integer rfIntegerermediatePage) {
		this.rfIntegerermediatePage = rfIntegerermediatePage;
	}




	/**
	 * @return the sendTempPasswordToCardHolder
	 */
	public Integer getSendTempPasswordToCardHolder() {
		return sendTempPasswordToCardHolder;
	}




	/**
	 * @param sendTempPasswordToCardHolder the sendTempPasswordToCardHolder to set
	 */
	public void setSendTempPasswordToCardHolder(Integer sendTempPasswordToCardHolder) {
		this.sendTempPasswordToCardHolder = sendTempPasswordToCardHolder;
	}




	/**
	 * @return the simultaneousChLogin
	 */
	public Integer getSimultaneousChLogin() {
		return simultaneousChLogin;
	}




	/**
	 * @param simultaneousChLogin the simultaneousChLogin to set
	 */
	public void setSimultaneousChLogin(Integer simultaneousChLogin) {
		this.simultaneousChLogin = simultaneousChLogin;
	}




	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}




	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}




	/**
	 * @return the step1HasOptionalFields
	 */
	public Integer getStep1HasOptionalFields() {
		return step1HasOptionalFields;
	}




	/**
	 * @param step1HasOptionalFields the step1HasOptionalFields to set
	 */
	public void setStep1HasOptionalFields(Integer step1HasOptionalFields) {
		this.step1HasOptionalFields = step1HasOptionalFields;
	}




	/**
	 * @return the subProcessorName
	 */
	public String getSubProcessorName() {
		return subProcessorName;
	}




	/**
	 * @param subProcessorName the subProcessorName to set
	 */
	public void setSubProcessorName(String subProcessorName) {
		this.subProcessorName = subProcessorName;
	}




	/**
	 * @return the supportUpgradeadCAPUI
	 */
	public Integer getSupportUpgradedCAPUI() {
		return supportUpgradedCAPUI;
	}




	/**
	 * @param supportUpgradeadCAPUI the supportUpgradeadCAPUI to set
	 */
	public void setSupportUpgradedCAPUI(Integer supportUpgradeadCAPUI) {
		this.supportUpgradedCAPUI = supportUpgradeadCAPUI;
	}




	/**
	 * @return the suppressCHContactDetailsInput
	 */
	public Integer getSuppressCHContactDetailsInput() {
		return suppressCHContactDetailsInput;
	}




	/**
	 * @param suppressCHContactDetailsInput the suppressCHContactDetailsInput to set
	 */
	public void setSuppressCHContactDetailsInput(Integer suppressCHContactDetailsInput) {
		this.suppressCHContactDetailsInput = suppressCHContactDetailsInput;
	}




	/**
	 * @return the symbolDisplay
	 */
	public Integer getSymbolDisplay() {
		return symbolDisplay;
	}




	/**
	 * @param symbolDisplay the symbolDisplay to set
	 */
	public void setSymbolDisplay(Integer symbolDisplay) {
		this.symbolDisplay = symbolDisplay;
	}




	/**
	 * @return the symbolLink
	 */
	public Integer getSymbolLink() {
		return symbolLink;
	}




	/**
	 * @param symbolLink the symbolLink to set
	 */
	public void setSymbolLink(Integer symbolLink) {
		this.symbolLink = symbolLink;
	}




	/**
	 * @return the templateName
	 */
	public String getTemplateName() {
		return templateName;
	}




	/**
	 * @param templateName the templateName to set
	 */
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}




	/**
	 * @return the tempPasswordDuration
	 */
	public Integer getTempPasswordDuration() {
		return tempPasswordDuration;
	}




	/**
	 * @param tempPasswordDuration the tempPasswordDuration to set
	 */
	public void setTempPasswordDuration(Integer tempPasswordDuration) {
		this.tempPasswordDuration = tempPasswordDuration;
	}




	/**
	 * @return the trustedBeneficiary
	 */
	public String getTrustedBeneficiary() {
		return trustedBeneficiary;
	}




	/**
	 * @param trustedBeneficiary the trustedBeneficiary to set
	 */
	public void setTrustedBeneficiary(String trustedBeneficiary) {
		this.trustedBeneficiary = trustedBeneficiary;
	}




	/**
	 * @return the ttpConfigId
	 */
	public Integer getTtpConfigId() {
		return ttpConfigId;
	}




	/**
	 * @param ttpConfigId the ttpConfigId to set
	 */
	public void setTtpConfigId(Integer ttpConfigId) {
		this.ttpConfigId = ttpConfigId;
	}




	/**
	 * @return the twoStepLoginEnabled
	 */
	public Integer getTwoStepLoginEnabled() {
		return twoStepLoginEnabled;
	}




	/**
	 * @param twoStepLoginEnabled the twoStepLoginEnabled to set
	 */
	public void setTwoStepLoginEnabled(Integer twoStepLoginEnabled) {
		this.twoStepLoginEnabled = twoStepLoginEnabled;
	}




	/**
	 * @return the uaCheckedEnabled
	 */
	public Integer getUaCheckedEnabled() {
		return uaCheckedEnabled;
	}




	/**
	 * @param uaCheckedEnabled the uaCheckedEnabled to set
	 */
	public void setUaCheckedEnabled(Integer uaCheckedEnabled) {
		this.uaCheckedEnabled = uaCheckedEnabled;
	}




	/**
	 * @return the upgraded
	 */
	public Integer getUpgraded() {
		return upgraded;
	}




	/**
	 * @param upgraded the upgraded to set
	 */
	public void setUpgraded(Integer upgraded) {
		this.upgraded = upgraded;
	}




	/**
	 * @return the uploadKey
	 */
	public String getUploadKey() {
		return uploadKey;
	}




	/**
	 * @param uploadKey the uploadKey to set
	 */
	public void setUploadKey(String uploadKey) {
		this.uploadKey = uploadKey;
	}




	/**
	 * @return the useCvvForCAVV
	 */
	public Integer getUseCvvForCAVV() {
		return useCvvForCAVV;
	}




	/**
	 * @param useCvvForCAVV the useCvvForCAVV to set
	 */
	public void setUseCvvForCAVV(Integer useCvvForCAVV) {
		this.useCvvForCAVV = useCvvForCAVV;
	}




	/**
	 * @return the userEncoding
	 */
	public String getUserEncoding() {
		return userEncoding;
	}




	/**
	 * @param userEncoding the userEncoding to set
	 */
	public void setUserEncoding(String userEncoding) {
		this.userEncoding = userEncoding;
	}




	/**
	 * @return the userIdEnabled
	 */
	public Integer getUserIdEnabled() {
		return userIdEnabled;
	}




	/**
	 * @param userIdEnabled the userIdEnabled to set
	 */
	public void setUserIdEnabled(Integer userIdEnabled) {
		this.userIdEnabled = userIdEnabled;
	}




	/**
	 * @return the veLog
	 */
	public Integer getVeLog() {
		return veLog;
	}




	/**
	 * @param veLog the veLog to set
	 */
	public void setVeLog(Integer veLog) {
		this.veLog = veLog;
	}




	/**
	 * @return the bankDirName
	 */
	public String getBankDirName() {
		return BankDirName;
	}




	/**
	 * @param bankDirName the bankDirName to set
	 */
	public void setBankDirName(String bankDirName) {
		BankDirName = bankDirName;
	}
	
	
	
	
	
	
	
	
	
	

}
