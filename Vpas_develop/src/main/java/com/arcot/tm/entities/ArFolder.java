package com.arcot.tm.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Table(name = "ARFOLDER",uniqueConstraints = @UniqueConstraint(columnNames ={"FOLDER"}))
@Entity
public class ArFolder {
	
	@Column(name = "FOLDER")
	private String folder;
	
	@Id
	@Column(name = "FOLDERID")
	private Integer folderId;
	
	@Column(name = "SECONDARYFOLDERID")
	private Integer secondaryFolderId;

	/**
	 * @return the folder
	 */
	public String getFolder() {
		return folder;
	}

	/**
	 * @param folder the folder to set
	 */
	public void setFolder(String folder) {
		this.folder = folder;
	}

	/**
	 * @return the folderId
	 */
	public Integer getFolderId() {
		return folderId;
	}

	/**
	 * @param folderId the folderId to set
	 */
	public void setFolderId(Integer folderId) {
		this.folderId = folderId;
	}

	/**
	 * @return the secondaryFolderId
	 */
	public Integer getSecondaryFolderId() {
		return secondaryFolderId;
	}

	/**
	 * @param secondaryFolderId the secondaryFolderId to set
	 */
	public void setSecondaryFolderId(Integer secondaryFolderId) {
		this.secondaryFolderId = secondaryFolderId;
	}

}
