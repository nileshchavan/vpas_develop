package com.arcot.tm.entities;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="ARADMINLEVEL")
public class ArAdminLevel {
	
	@EmbeddedId	
	private ArAdminLevelPk arAdminLevelPk;
	
	
	
	@Column(name="ALLOWPASSWORDRESET")
	private Integer  allowPasswordReset;
	
	@Column(name = "PASSWORD_RENEWALFREQUENCY")
	private Integer passwordRenewalFrequency;
	
	@Column(name="COLLECTADMINCONTACTDETAILS")
	private Integer  collectAdminContactDetails;
	
	@Column(name="DELIVERYCHANNEL")
	private Integer  deliveryChannel;
	
	@Column(name="LOGINSESSIONTIMEOUT")
	private Integer  loginSessionTimeOut;
	
	@Column(name="MAXINACTIVITYPERIOD")
	private Integer  maxInActivityPeriod;
	
	
	@Column(name="MAXTRIESACROSSSESSION")
	private Integer  maxTriesAcrossSession;
	
	@Column(name="MAXTRIESPERSESSION")
	private Integer  maxTriesPerSession;
	
	
	@Column(name="PASSWORDCHANGEVELOCITY")
	private Integer  passwordChangeVELOCITY;

	@Column(name = "PASSWORDUNLOCKDURATION")
	private Integer passwordUnlockDuration;
	
	@Column(name = "PASSWORDPARAMS")
	private String passwordParams;
	
	@Column(name = "PASSWORDSTORAGEALGORITHM")
	private Integer passwordStorageAlgorithm;
	
	@Column(name = "RETAINOLDFORMATPASSWORD")
	private Integer retainOldFormatPassword;
	
	
	
	/**
	 * @return the retainOldFormatPassword
	 */
	public Integer getRetainOldFormatPassword() {
		return retainOldFormatPassword;
	}


	/**
	 * @param retainOldFormatPassword the retainOldFormatPassword to set
	 */
	public void setRetainOldFormatPassword(Integer retainOldFormatPassword) {
		this.retainOldFormatPassword = retainOldFormatPassword;
	}


	/**
	 * @return the passwordStorageAlgorithm
	 */
	public Integer getPasswordStorageAlgorithm() {
		return passwordStorageAlgorithm;
	}


	/**
	 * @param passwordStorageAlgorithm the passwordStorageAlgorithm to set
	 */
	public void setPasswordStorageAlgorithm(Integer passwordStorageAlgorithm) {
		this.passwordStorageAlgorithm = passwordStorageAlgorithm;
	}


	/**
	 * @return the passwordParams
	 */
	public String getPasswordParams() {
		return passwordParams;
	}


	/**
	 * @param passwordParams the passwordParams to set
	 */
	public void setPasswordParams(String passwordParams) {
		this.passwordParams = passwordParams;
	}


	/**
	 * @return the passwordUnlockDuration
	 */
	public Integer getPasswordUnlockDuration() {
		return passwordUnlockDuration;
	}


	/**
	 * @param passwordUnlockDuration the passwordUnlockDuration to set
	 */
	public void setPasswordUnlockDuration(Integer passwordUnlockDuration) {
		this.passwordUnlockDuration = passwordUnlockDuration;
	}


	/**
	 * @return the arAdminLevelPk
	 */
	public ArAdminLevelPk getArAdminLevelPk() {
		return arAdminLevelPk;
	}


	/**
	 * @param arAdminLevelPk the arAdminLevelPk to set
	 */
	public void setArAdminLevelPk(ArAdminLevelPk arAdminLevelPk) {
		this.arAdminLevelPk = arAdminLevelPk;
	}


	/**
	 * @return the allowPasswordReset
	 */
	public Integer getAllowPasswordReset() {
		return allowPasswordReset;
	}


	/**
	 * @param allowPasswordReset the allowPasswordReset to set
	 */
	public void setAllowPasswordReset(Integer allowPasswordReset) {
		this.allowPasswordReset = allowPasswordReset;
	}


	/**
	 * @return the collectAdminContactDetails
	 */
	public Integer getCollectAdminContactDetails() {
		return collectAdminContactDetails;
	}


	/**
	 * @param collectAdminContactDetails the collectAdminContactDetails to set
	 */
	public void setCollectAdminContactDetails(Integer collectAdminContactDetails) {
		this.collectAdminContactDetails = collectAdminContactDetails;
	}


	/**
	 * @return the deliveryChannel
	 */
	public Integer getDeliveryChannel() {
		return deliveryChannel;
	}


	/**
	 * @param deliveryChannel the deliveryChannel to set
	 */
	public void setDeliveryChannel(Integer deliveryChannel) {
		this.deliveryChannel = deliveryChannel;
	}


	/**
	 * @return the loginSessionTimeOut
	 */
	public Integer getLoginSessionTimeOut() {
		return loginSessionTimeOut;
	}


	/**
	 * @param loginSessionTimeOut the loginSessionTimeOut to set
	 */
	public void setLoginSessionTimeOut(Integer loginSessionTimeOut) {
		this.loginSessionTimeOut = loginSessionTimeOut;
	}


	/**
	 * @return the maxInActivityPeriod
	 */
	public Integer getMaxInActivityPeriod() {
		return maxInActivityPeriod;
	}


	/**
	 * @param maxInActivityPeriod the maxInActivityPeriod to set
	 */
	public void setMaxInActivityPeriod(Integer maxInActivityPeriod) {
		this.maxInActivityPeriod = maxInActivityPeriod;
	}


	/**
	 * @return the maxTriesAcrossSession
	 */
	public Integer getMaxTriesAcrossSession() {
		return maxTriesAcrossSession;
	}


	/**
	 * @param maxTriesAcrossSession the maxTriesAcrossSession to set
	 */
	public void setMaxTriesAcrossSession(Integer maxTriesAcrossSession) {
		this.maxTriesAcrossSession = maxTriesAcrossSession;
	}


	/**
	 * @return the maxTriesPerSession
	 */
	public Integer getMaxTriesPerSession() {
		return maxTriesPerSession;
	}


	/**
	 * @param maxTriesPerSession the maxTriesPerSession to set
	 */
	public void setMaxTriesPerSession(Integer maxTriesPerSession) {
		this.maxTriesPerSession = maxTriesPerSession;
	}


	/**
	 * @return the passwordChangeVELOCITY
	 */
	public Integer getPasswordChangeVELOCITY() {
		return passwordChangeVELOCITY;
	}


	/**
	 * @param passwordChangeVELOCITY the passwordChangeVELOCITY to set
	 */
	public void setPasswordChangeVELOCITY(Integer passwordChangeVELOCITY) {
		this.passwordChangeVELOCITY = passwordChangeVELOCITY;
	}


	/**
	 * @return the passwordRenewalFrequency
	 */
	public Integer getPasswordRenewalFrequency() {
		return passwordRenewalFrequency;
	}


	/**
	 * @param passwordRenewalFrequency the passwordRenewalFrequency to set
	 */
	public void setPasswordRenewalFrequency(Integer passwordRenewalFrequency) {
		this.passwordRenewalFrequency = passwordRenewalFrequency;
	}
	

}
