package com.arcot.tm.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ARCOUNTRY")
public class ArCountry {
	
	@Id
	@Column(name="COUNTRYCODE")
	private int countryCode;
	
	@Column(name="COUNTRYTYPE")
	private String countryType;
	
	@Column(name="COUNTRYNAME")
	private String countryName;

	public int getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(int countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryType() {
		return countryType;
	}

	public void setCountryType(String countryType) {
		this.countryType = countryType;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	
	
}
