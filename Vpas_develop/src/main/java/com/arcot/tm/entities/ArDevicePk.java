package com.arcot.tm.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;


@Embeddable
public class ArDevicePk implements Serializable {
	
	@Column(name = "BANKID")
	private Integer bankId;
	
	@Column(name = "RANGEID")
	private Integer rangeId;
	
	
	@Column(name = "HTTPACCEPTID")
	private Integer  httpAcceptId;
	
	@Column(name = "DEVCATEGORY")
	private Integer devCategory;
	
	@Column(name = "LOCALEID")
	private Integer localeId;

	/**
	 * @return the bankId
	 */
	public Integer getBankId() {
		return bankId;
	}

	/**
	 * @param bankId the bankId to set
	 */
	public void setBankId(Integer bankId) {
		this.bankId = bankId;
	}

	/**
	 * @return the rangeId
	 */
	public Integer getRangeId() {
		return rangeId;
	}

	/**
	 * @param rangeId the rangeId to set
	 */
	public void setRangeId(Integer rangeId) {
		this.rangeId = rangeId;
	}

	/**
	 * @return the httpAcceptId
	 */
	public Integer getHttpAcceptId() {
		return httpAcceptId;
	}

	/**
	 * @param httpAcceptId the httpAcceptId to set
	 */
	public void setHttpAcceptId(Integer httpAcceptId) {
		this.httpAcceptId = httpAcceptId;
	}

	/**
	 * @return the devCategory
	 */
	public Integer getDevCategory() {
		return devCategory;
	}

	/**
	 * @param devCategory the devCategory to set
	 */
	public void setDevCategory(Integer devCategory) {
		this.devCategory = devCategory;
	}

	/**
	 * @return the localeId
	 */
	public Integer getLocaleId() {
		return localeId;
	}

	/**
	 * @param localeId the localeId to set
	 */
	public void setLocaleId(Integer localeId) {
		this.localeId = localeId;
	}

	
}
