package com.arcot.tm.entities;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="ARADMINBANK")
public class ArAdminBank {
	
	@EmbeddedId
	private ArAdminBankPk arAdminBankPk;
	
	@Column(name="ADMINNAMEEX")
	private String adminNameEx;

	/**
	 * @return the arAdminBankPk
	 */
	public ArAdminBankPk getArAdminBankPk() {
		return arAdminBankPk;
	}

	/**
	 * @param arAdminBankPk the arAdminBankPk to set
	 */
	public void setArAdminBankPk(ArAdminBankPk arAdminBankPk) {
		this.arAdminBankPk = arAdminBankPk;
	}

	/**
	 * @return the adminNameEx
	 */
	public String getAdminNameEx() {
		return adminNameEx;
	}

	/**
	 * @param adminNameEx the adminNameEx to set
	 */
	public void setAdminNameEx(String adminNameEx) {
		this.adminNameEx = adminNameEx;
	}
	
	

}