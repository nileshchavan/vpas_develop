package com.arcot.tm.service;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Vector;

import javax.servlet.ServletContext;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.multipart.MultipartFile;

import com.arcot.tm.entities.ArAdminBank;
import com.arcot.tm.entities.ArAdminBankPk;
import com.arcot.tm.entities.ArAdminLevel;
import com.arcot.tm.entities.ArAdminLevelPk;
import com.arcot.tm.entities.ArBankInfo;
import com.arcot.tm.entities.ArBrandInfo;
import com.arcot.tm.entities.ArDevice;
import com.arcot.tm.entities.ArDevicePk;
import com.arcot.tm.entities.ArESAdmin;
import com.arcot.tm.entities.AresBankEnrollingConfig;
import com.arcot.tm.entities.AresBankEnrollingConfigPk;
import com.arcot.tm.model.AdminLevelParams;
import com.arcot.tm.model.AdminLevels;
import com.arcot.tm.model.Admins;
import com.arcot.tm.model.ArBankInfoDto;
import com.arcot.tm.model.ArBranddto;
import com.arcot.tm.model.LocaleFolderDto;
import com.arcot.tm.repository.AdminRepository;
import com.arcot.tm.repository.ArAdminLevelRepository;
import com.arcot.tm.repository.ArBankInfoRepository;
import com.arcot.tm.repository.ArBrandInfoRepository;
import com.arcot.tm.repository.ArDeviceRepository;
import com.arcot.tm.repository.AresBankEnrollConfigRepository;
import com.arcot.tm.repository.LocaleRepository;
import com.arcot.tm.util.EnrollmentFieldValues;



/**
 * This class is used to perform global admin operations
 * @author  Vizag Team
 *
 */
@Service
public class GlobalAdminService implements ServletContextAware{

	@Autowired
	private AdminRepository adminRepository;

	@Autowired
	private ArBankInfoRepository bankInfoRepository;

	@Autowired
	private ArBrandInfoRepository arBrandInfoRepository;
	
	 @Autowired
	    private ArDeviceRepository arDeviceRepository;
	    
	    @Autowired
	    private ArAdminLevelRepository arAdminLevelRepository;
	    
	    @Autowired
	    private AresBankEnrollConfigRepository enrollConfigRepository;
	    
	    @Autowired
	    private LocaleRepository localeRepository;
	    
	    @Autowired
	    private ServletContext servletContext;
	    
	    @Override
	    public void setServletContext(ServletContext servletContext) {
	    	// TODO Auto-generated method stub
	    	this.servletContext = servletContext;
	    }
	    
	    @Bean
	    public ServletContext getServletContext() {
	      return servletContext;
	    }
		/**
		 * Creating new Issuer
		 * @param arBankInfo
		 * @return
		 */
		public ArBankInfo createIssuer(ArBankInfoDto arBankInfoDto) {
			
			
			
			
			ModelMapper modelMapper = new ModelMapper();
			ArBankInfo arBankInfo = new ArBankInfo();
			modelMapper.map(arBankInfoDto, arBankInfo);
			
			AdminLevelParams alp = new AdminLevelParams();
			arBankInfo.setDateCreated(new Date(System.currentTimeMillis()));
			ArBankInfo bankInfoInserted = bankInfoRepository.save(arBankInfo);
			
			ArBankInfo resetedData = setDetailsBasedOnBankKeyAndBankDirName(bankInfoInserted);
			
			EnrollmentFieldValues efv = new EnrollmentFieldValues();
			Vector data = efv.getMandatoryFieldsList();
			int iRecords = data.size();
			for (int i = 0; i < iRecords; i++) {

				String fieldName = (String) data.elementAt(i);
				if (fieldName.equalsIgnoreCase("HINTQ") || fieldName.equalsIgnoreCase("HINTANSWER"))
					continue;
				AresBankEnrollingConfig aresBankEnrollingConfig = new AresBankEnrollingConfig();
				AresBankEnrollingConfigPk aresBankEnrollingConfigPk = new AresBankEnrollingConfigPk();
				aresBankEnrollingConfigPk.setBankId(arBankInfo.getBankId());
				aresBankEnrollingConfigPk.setFieldName(fieldName);
				aresBankEnrollingConfig.setFieldType(efv.getUIFieldType((String) data.elementAt(i)));
				
				aresBankEnrollingConfig.setAresBankEnrollingConfigPk(aresBankEnrollingConfigPk);
				
				insertESEnrollConfigString(aresBankEnrollingConfig);
				
				List<LocaleFolderDto>localeFolderDto=null;
				 localeFolderDto = localeRepository.getFolderIdFromLocale();
				HashMap folderIdMap = new HashMap();
				
				
				for(LocaleFolderDto localeFolderDtoVar : localeFolderDto )
				{
					folderIdMap.put(localeFolderDtoVar.getLocaleId(), localeFolderDtoVar.getFolderId());

				}
				
				ArBrandInfo arBrandInfo = new ArBrandInfo();
				
				
				
				
				ArDevice arDevice = new ArDevice();
				ArDevicePk arDevicePk = new ArDevicePk();
				
				arDevicePk.setBankId(resetedData.getBankId());
				arDevice.setHttpUaId(0);
				arDevicePk.setHttpAcceptId(0);
				arDevicePk.setDevCategory(0);
				arDevicePk.setLocaleId(resetedData.getLocaleId());
				
				for(LocaleFolderDto localeFolderDtoVar : localeFolderDto )
				{
					arDevice.setFolderId(localeFolderDtoVar.getFolderId());
				}
				
				
				arDevice.setAcsUrlId(1);
				arDevicePk.setRangeId(0);
				arDevice.setArDevicePk(arDevicePk);
				insertDevice(arDevice);
				
				ArAdminLevel arAdminLevel = new ArAdminLevel();
				ArAdminLevelPk arAdminLevelPk = new ArAdminLevelPk();
				
				// For AdminLevel 3 & 4
				for (int adminLevel = 3; adminLevel < 5; adminLevel++) {

					arAdminLevelPk.setBankId(resetedData.getBankId());
					arAdminLevelPk.setAdminLevel(adminLevel);
					arAdminLevel.setMaxTriesPerSession(3);
					arAdminLevel.setMaxTriesAcrossSession(6);
					arAdminLevel.setPasswordRenewalFrequency(1000 * 60 * 60 * 24 * 60);
					arAdminLevel.setPasswordUnlockDuration(0);
					arAdminLevel.setMaxInActivityPeriod( 1000 * 60 * 60 * 24 * 60);
					arAdminLevel.setAllowPasswordReset(0);
					arAdminLevel.setPasswordParams(null);
					arAdminLevel.setLoginSessionTimeOut(AdminLevelParams.LOGIN_SESSION_TIMEOUT_DEFAULT);
					arAdminLevel.setCollectAdminContactDetails(alp.getCollectAdminContactDetails());
					arAdminLevel.setDeliveryChannel(alp.getTwoFactorDeliveryChannelType());
					
					arAdminLevel.setArAdminLevelPk(arAdminLevelPk);
					
					List< ArESAdmin> arAdmin = adminRepository.findBySecondaryBankId(resetedData.getBankId());
					
					ArESAdmin updatedAdmin =null;
					for(ArESAdmin admin:arAdmin) {
						admin.setDeliveryChannel(arBankInfoDto.getAdminDto().getDeliveryChannel());
						admin.setCollectAdminContactDetails(arBankInfoDto.getAdminDto().getCollectAdminContactDetails());
					 updatedAdmin = adminRepository.save(admin);
					}
					List<Admins> adminsForBank = arBankInfoDto.getSelectedAdminList();
					
					ArAdminBank arAdminBank = new ArAdminBank();
					ArAdminBankPk arAdminBankPk = new ArAdminBankPk();
					
					if (adminsForBank != null) {
						int adminCount = adminsForBank.size();
						for (Admins admin : adminsForBank) {
							arAdminBankPk.setBankId(resetedData.getBankId());
							if (admin.getAdminName().indexOf("-")> 0)
								admin.setAdminName(admin.getAdminName().substring(0, (admin.getAdminName().indexOf("-") - 1)));
							arAdminBankPk.setAdminName(admin.getAdminName());
							arAdminBankPk.setSecondaryBankId(resetedData.getBankId());
							arAdminBank.setAdminNameEx(updatedAdmin.getAresadminpk().getName());;
						}
					}
					setConstants(alp,updatedAdmin);
				}
				
			}
			return arBankInfo;
		}


	
	/**
	 * This method is used for getting admins by level
	 * @param level
	 * @return
	 */
	public List<ArESAdmin> getAllAdminsByLevel(int level){

		ArrayList<ArESAdmin> adminList = new ArrayList<ArESAdmin>();

		adminList = adminRepository.findByAdminLevel(level);

		return adminList;
	}

	/**
	 * This method is used to get user by user Name
	 * @param name
	 * @return user object
	 */
	public ArESAdmin getUserByUsername(String name) {

		Optional<ArESAdmin> optional = adminRepository.findByUserName(name);
		ArESAdmin user = null;
		if(optional.isPresent())
			user = optional.get();
		if (user != null)

			return user;
		else
			return null;
	}



	public int createCardRangeForIssuer(ArBranddto arBranddto) {

		ModelMapper modelMapper = new ModelMapper();

		ArBrandInfo arBrandInfo = new ArBrandInfo();

		if(arBranddto.getPanLength()!=null && (arBranddto.getPanLength()>=13 || arBranddto.getPanLength() <=19)) {

			if(arBranddto.getFiBusinessId().length() == 8) {

				if(arBranddto.getFiBin().length() == 6) {
					

					if(arBranddto.getPanLength() == arBranddto.getArBrandInfoPk().getBeginRange().length()&&
							arBranddto.getPanLength() == arBranddto.getArBrandInfoPk().getEndRange().length() && Long.parseLong(arBranddto.getArBrandInfoPk().getBeginRange())<=Long.parseLong(arBranddto.getArBrandInfoPk().getEndRange()) ) {
						
						if(arBranddto.getMobileEnabled() == 1 || arBranddto.getMobileEnabled() == 0) {
							modelMapper.map(arBranddto, arBrandInfo);
							arBrandInfoRepository.save(arBrandInfo);
						return 1; 
					}
						else{
							//mobile enabled should be 0 r 1
							return -5;
						}
						}
					else {
						//invalid begin range or end range
						return -4;
					}
				}
				else {
					//fiBinId must be of 6 digits
					return -3;
				}
			}

			else {

				//businessId must be of 8 digits
				return -2;
			}

		}

		else {
			//pan length must be between 13 and 19
			return -1;
		}



	}

	/*
	 * 
	 */
	public List<ArBrandInfo> getAllCardRangeGroup(int bankId) {
		return arBrandInfoRepository.findAllCardRangeGroup(bankId);
	}

	/**
	 * This method is used to get fi information based on range id 
	 * @param rangeId
	 * @return
	 */
	public ArBrandInfo getAdminByRangeId(Integer rangeId) {
		Optional<ArBrandInfo> optional =  arBrandInfoRepository.findByRangeId(rangeId);
		if(optional.isPresent())
		{
			return optional.get();
		}
		else
		{
			return null;
		}
	}

	/**
	 * This method is used for storing file information in add fi
	 * @param rangeId
	 * @param signingCertificate
	 * @return
	 */
	public int uploadSigningCertificate(Integer rangeId, MultipartFile signingCertificate) {
		//Checking of the presence of the file from the controller 
		if(!signingCertificate.isEmpty()) {
			ArBrandInfo arBrandInfo = getAdminByRangeId(rangeId);
			//Checking of the object based on rangeid 
			if(arBrandInfo != null)
			{
				try {
					//converting multi-part data into byte array
					byte[] byteArray=signingCertificate.getBytes();
					arBrandInfo.setSigningCertFile(byteArray);
					//Saving multi-part data as bytearray in database 
					ArBrandInfo savedArBrandInfo=arBrandInfoRepository.save(arBrandInfo);
					//Whether file is saved successfully or not 
					if(savedArBrandInfo != null)
						return 1;
					else
						return -3;
				} catch (IOException e) {
					return -3;
				}
			}
			else
			{
				return -2;
			}
		}
		else
		{
			return -1;
		}
	}

public AresBankEnrollingConfig insertESEnrollConfigString(AresBankEnrollingConfig aresBankEnrollingConfig) {
		
		AresBankEnrollingConfig savedRecord = enrollConfigRepository.save(aresBankEnrollingConfig);
		return savedRecord;
	}
	
 public ArDevice insertDevice(ArDevice arDevice)
 {
	 ArDevice savedRecordOfArDevice = arDeviceRepository.save(arDevice);
	return savedRecordOfArDevice;
 }
	
 public ArAdminLevel insertAdminLevel(ArAdminLevel arAdminLevel)
 {
	 ArAdminLevel savedRecordOfArAdminLevel = arAdminLevelRepository.save(arAdminLevel);
	 
	return savedRecordOfArAdminLevel;
 }
 
 
 public ArBankInfo setDetailsBasedOnBankKeyAndBankDirName(ArBankInfo arBankInfo ) {
	 
		ArBankInfo bankInfo = bankInfoRepository.getDetailsBasedOnBankKeyAndBankDirName(arBankInfo.getBankKey(), arBankInfo.getBankDirName());
		
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.map(bankInfo, arBankInfo);
		return bankInfo;
		
		
		
 }
 
 public void setConstants(AdminLevelParams alp,ArESAdmin admin) {
	 
	 
//	 AdminLevels al = (AdminLevels) this.getServletContext().getAttribute("AL");
	 AdminLevels al = new AdminLevels();
	 alp.AdminLevel = 3;
		alp.BankId = (int) admin.getSecondaryBankId();
		alp.MaxTriesPerSession = 3; // 0 => infinit tries
		alp.MaxTriesAcrossSession = 6; // 0 => infinit tries
		alp.PasswordRenewalFrequency = 1000 * 60 * 60 * 24 * 60; // specified
																	// in
																	// msec,
																	// 0
																	// =>
																	// Password
																	// Never
																	// Expires
		alp.PasswordUnlockDuration = 0; // specified in msec, 0 =>
										// Not to be Unlocked
										// automatically
		alp.MaxInactivityPeriod = 1000 * 60 * 60 * 24 * 60; // specified
																// in
																// msec,
																// 0
																// =>
																// infinite
																// Inactivity
																// period
		alp.MaxLength = -1; // -ve => No constraint on Max length
		alp.MinLength = -1; // -ve => No constraint on Min length
		alp.MinAlphabet = -1; // -ve => No constraint on Min
								// Alphabet
		alp.MinNumeric = -1; //

		al.addLevelParamObject(alp);

		alp.AdminLevel = 4;
		alp.BankId = (int) admin.getSecondaryBankId();
		alp.MaxTriesPerSession = 3; // 0 => infinit tries
		alp.MaxTriesAcrossSession = 6; // 0 => infinit tries
		alp.PasswordRenewalFrequency = 1000 * 60 * 60 * 24 * 60; // specified
																	// in
																	// msec,
																	// 0
																	// =>
																	// Password
																	// Never
																	// Expires
		alp.PasswordUnlockDuration = 0; // specified in msec, 0 =>
										// Not to be Unlocked
										// automatically
		alp.MaxInactivityPeriod = 1000 * 60 * 60 * 24 * 60; // specified
																// in
																// msec,
																// 0
																// =>
																// infinite
																// Inactivity
																// period
		alp.MaxLength = -1; // -ve => No constraint on Max length
		alp.MinLength = -1; // -ve => No constraint on Min length
		alp.MinAlphabet = -1; // -ve => No constraint on Min
								// Alphabet
		alp.MinNumeric = -1;

		al.addLevelParamObject(alp);
 }

}