package com.arcot.tm.service;


import java.sql.Timestamp;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.arcot.tm.entities.ArESAdmin;
import com.arcot.tm.payload.LoginRequest;
import com.arcot.tm.payload.MasterLoginRequest;
import com.arcot.tm.repository.AdminRepository;

/**
 * This class is used for performing authentication services
 * @author Vizag Team
 *
 */
@Service
@Transactional
public class AuthService {

	@Autowired
	private GlobalAdminService adminService;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private MasterAdminService masterAdminService;
	

	/**
	 * This method allows admin to get logged in with valid credentials
	 * @param loginRequest
	 * @return
	 */
	public int login(LoginRequest loginRequest)
	{
		int result=0;

		int count=0;
		
		//Validating the password and user name
		
		//Checking whether the credentials is empty or null and returning the response status
		if (loginRequest.getPassword().equals("") || loginRequest.getUsernameOrEmail().equals(" ")) {
			return -1;
		}
		for(int i=0;i<loginRequest.getUsernameOrEmail().length();i++)
		{
			if(loginRequest.getUsernameOrEmail().charAt(i)!=' ')
			{
				count=1;
			}
		}
		if(count==0)
		{
			return -1;
		}
		count = 0;
		for(int i=0;i<loginRequest.getPassword().length();i++)
		{
			if(loginRequest.getPassword().charAt(i)!=' ')
			{
				count=1;
			}
		}
		if(count==0)
		{
			return -1;
		}
		
		ArESAdmin admin = adminService.getUserByUsername(loginRequest.getUsernameOrEmail());
		
	
		
		if(admin!=null && loginRequest!=null)
		{
			if(admin.getPassword().equals(loginRequest.getPassword()))
			{
				if(admin.getStatus()==-1) {
					result=-3; 
				}else {
					admin.setLastLongTime(new Timestamp(System.currentTimeMillis()));
					adminRepository.save(admin);
					result=1;
				}
			}
			else
			{
				admin.setFailedStrikes(admin.getFailedStrikes()+1);
				adminRepository.save(admin);
				result = -4;
			}
		}
		else
		{
			result=-2;
		}

		return result;

	}
	/**
	 * This method performs masterAdminLogin and validates for all negative cases
	 * @param masterLoginRequest
	 * @param admin
	 * @return result
	 */
	public int masterAdminLogin( MasterLoginRequest masterLoginRequest,ArESAdmin admin) {
		
		//Initializing the result variable to send the integer response
		int result=0;

		//Initializing the count variable to check number of chars other than ""
		int count=0;

		//Validating the password and user name

		//Checking whether the credentials is empty or null and returning the response status
		//Response code:-1 For password null and empty cases
		if (masterLoginRequest.getPassword1().equals("") && masterLoginRequest.getPassword2().equals(" ")) {
			return -1;
		}
		for(int i=0;i<masterLoginRequest.getPassword1().length();i++)
		{
			if(masterLoginRequest.getPassword1().charAt(i)!=' ')
			{
				count=1;
			}
		}

		int count2 = 0;
		for(int i=0;i<masterLoginRequest.getPassword2().length();i++)
		{
			if(masterLoginRequest.getPassword2().charAt(i)!=' ')
			{
				count2=1;
			}
		}
		if(count2==0 && count==0)
		{
			return -1;
		}

		//Validating the password from request object and admin object from database
		if(admin!=null && masterLoginRequest!=null)
		{

			String password = masterLoginRequest.getPassword1() + masterLoginRequest.getPassword2();
			
			if(admin.getPassword().equals(password))
			{
				//Response code:-3 For Account blocked Case
				if(admin.getStatus()==-1) {
					result=-3; 
				}else {
					//Response code:1 For Successful login case
					admin.setLastLongTime(new Timestamp(System.currentTimeMillis()));
					adminRepository.save(admin);
					result=1;
				}
			}
			else
			{
				//Response code:-4 For Incorrect password case
				admin.setFailedStrikes(admin.getFailedStrikes()+1);
				adminRepository.save(admin);
				result = -4;
			}
		}
		else
		{
			//Response code:-2 For Account doesn't exist case
			result=-2;
		}

		return result;
	}

}
