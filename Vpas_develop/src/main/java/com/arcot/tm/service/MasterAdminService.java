package com.arcot.tm.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.arcot.tm.entities.ArAdminBank;
import com.arcot.tm.entities.ArAdminBankPk;
import com.arcot.tm.entities.ArAdminCountry;
import com.arcot.tm.entities.ArAdminCountryPk;
import com.arcot.tm.entities.ArBankInfo;
import com.arcot.tm.entities.ArCountry;
import com.arcot.tm.entities.ArESAdmin;
import com.arcot.tm.model.ArBankInfoDto;
import com.arcot.tm.model.ArESAdminDto;
import com.arcot.tm.repository.AdminRepository;
import com.arcot.tm.repository.ArAdminBankRepository;
import com.arcot.tm.repository.ArAdminCountryRepository;
import com.arcot.tm.repository.ArBankInfoRepository;
import com.arcot.tm.repository.ArcountryRepository;

/**
 * This class is used for performing master admin operations
 * @author Vizag Team
 *
 */
@Service
@Transactional
public class MasterAdminService {

	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private ArBankInfoRepository arBankInfoRepository;
	
	@Autowired
	private ArAdminBankRepository arAdminBankRepository;
	
	@Autowired
	private ArcountryRepository arCountryRepository;
	
	@Autowired
	private ArAdminCountryRepository arAdminCountryRepository;
	
	/**
	 * This method creates global admin
	 * @param referenceAdmin
	 * @return saved object
	 */
	public int createGlobalAdmin(ArESAdminDto referenceAdmin) {
		// TODO dummy values given for acs
		ModelMapper modelMapper = new ModelMapper();
		
		ArESAdmin globalAdmin= new ArESAdmin();
		
		globalAdmin.setDateCreated(new Date(System.currentTimeMillis()));
		
		globalAdmin.setDateModified((new Date(System.currentTimeMillis())));// TODO remove date modified after updation of user
		
		modelMapper.map(referenceAdmin,globalAdmin);
		
		ArESAdmin savedAdmin = adminRepository.save(globalAdmin);
		
		ArAdminBank savedResult = null;
		
		ArAdminCountry arAdminCountryResult = null;
		
		for(ArBankInfoDto arBankInfoDto : referenceAdmin.getIssuers())
		{
			ArAdminBank arAdminBank = new ArAdminBank();
			ArAdminBankPk adminBankMap = new ArAdminBankPk();
			adminBankMap.setAdminName(globalAdmin.getAresadminpk().getName());
			adminBankMap.setBankId(arBankInfoDto.getAdminBankPk().getBankId());
			adminBankMap.setSecondaryBankId(arBankInfoDto.getAdminBankPk().getSecondaryBankId());
			arAdminBank.setArAdminBankPk(adminBankMap);
			savedResult = arAdminBankRepository.save(arAdminBank);
		}
		
		List<ArCountry> arAdminSelectedCountriesList = referenceAdmin.getSelectedCountriesList();
		
		for(ArCountry arCountryVar: arAdminSelectedCountriesList)
		{
			ArAdminCountry arAdminCountry=new ArAdminCountry();
			ArAdminCountryPk arAdminCountryPk = new ArAdminCountryPk();
			arAdminCountryPk.setCountryCode(arCountryVar.getCountryCode());
			arAdminCountryPk.setName(referenceAdmin.getAresadminpk().getName());
			arAdminCountryPk.setBankId(referenceAdmin.getAresadminpk().getBankId());
			arAdminCountry.setArAdminCountryPk(arAdminCountryPk);
			//TODO remove secondary bank id
			arAdminCountry.setSecondaryBankId(1);
			arAdminCountryResult= arAdminCountryRepository.save(arAdminCountry);
			
		}
		
		if(savedAdmin!=null && savedResult!=null && arAdminCountryResult!=null)
		{
			return 1;
		}
		else
		{
			return -1;
		}
	}
	
	/**
	 * This method gets all the issuers list
	 * @return
	 */
	public List<ArBankInfoDto> getAllIssuers()
	{
		List<ArBankInfo> arBankInfos = arBankInfoRepository.findAll();
		if(arBankInfos.isEmpty())
		{
			return null;
		}
		else
		{
			ModelMapper modelMapper = new ModelMapper();
			List<ArBankInfoDto> issuers = new ArrayList<>();
			for(ArBankInfo arBankInfo : arBankInfos)
			{
				ArBankInfoDto issuer = new ArBankInfoDto();
				modelMapper.map(arBankInfo, issuer);
				issuers.add(issuer);
			}
			return issuers;
		}
	}
	/**
	 * This method gets all the Countries List from repository
	 * @return
	 */
	public List<ArCountry> getAllCountries() {
		
		List<ArCountry> countriesList = arCountryRepository.findAll();
		
		return countriesList;
	}
	/**
	 * This method gets all the get Master Admin from repository
	 * @return
	 */
	public ArESAdmin getMasterAdminByLevel() {
		
		int masterAdminLevel=1;
		
		Optional<ArESAdmin> optional = adminRepository.findMasterAdminByLevel(masterAdminLevel);
		ArESAdmin user = null;
		if(optional.isPresent()) {
			user = optional.get();
			}
		if (user != null)
			return user;
		else
			return null;
	}

}