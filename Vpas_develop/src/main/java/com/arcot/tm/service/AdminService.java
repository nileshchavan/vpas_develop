package com.arcot.tm.service;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.arcot.tm.entities.ArAdminBank;
import com.arcot.tm.entities.ArBankInfo;
import com.arcot.tm.entities.ArCountry;
import com.arcot.tm.entities.ArESAdmin;
import com.arcot.tm.entities.ArLocale;
import com.arcot.tm.model.ArLocaleDescriptionDto;
import com.arcot.tm.repository.AdminRepository;
import com.arcot.tm.repository.ArAdminBankRepository;
import com.arcot.tm.repository.ArBankInfoRepository;
import com.arcot.tm.repository.AresCountryRepository;
import com.arcot.tm.repository.LocaleRepository;

/**
 * This class is used for performing admin operations
 * @author  Vizag Team
 *
 */
@Service
public class AdminService {

    @Autowired
    private AdminRepository adminRepository;
    
    @Autowired
    private ArBankInfoRepository bankInfoRepository;
    
    @Autowired
    private AresCountryRepository arCountryRepository;
    
    @Autowired
    private LocaleRepository localeRepository;
    
    @Autowired
    private ArAdminBankRepository adminBankRepository;
  
    		 
    /**
     * This method is used to get userdetails by username
     * @param name
     * @return
     */
	public ArESAdmin getUserByUsername(String name) {
		Optional<ArESAdmin> optional = adminRepository.findByUserName(name);
		ArESAdmin user = null;
		if(optional.isPresent())
			user = optional.get();
		if (user != null)
			return user;
		else
			return null;
	}
	
	/**
	 * Creating new Issuer
	 * @param arBankInfo
	 * @return arBankInfo
	 */
	public ArBankInfo createIssuer(ArBankInfo arBankInfo) {
		 bankInfoRepository.save(arBankInfo);
		return arBankInfo;
	}
	
	/**
	 * saving details in admin bank
	 * @param arAdminBank
	 */
	public void saveInAdminBank(ArAdminBank arAdminBank) {
		adminBankRepository.save(arAdminBank);
	}
	/**
	 * gets all the admin list based on bankId, adminLevel,name
	 * @param bankId,adminLevel,name
	 * @return adminList
	 */
	public List<ArAdminBank> getAllAdminsOfIssuer(int bankId,String name){
		
		List<ArAdminBank> adminList = null;
		int status = -1;
		
		adminList = adminBankRepository.findAllAdminInfoWithSecondaryAdmin(bankId,name);
		
		
		return adminList;
	}

	
	/**
	 * gets all the countries
	 * @return countryList
	 */
	public List<ArCountry> getAllCountry() {
		// TODO Auto-generated method stub
		List<ArCountry> countryList = (List<ArCountry>) arCountryRepository.findAll();
		return countryList;
	}
	/**
	 * gets all the Locales
	 * @return countryList
	 */
	public List<ArLocaleDescriptionDto> getAllLocale() {
		List<ArLocale> localeList = null;
		List<ArLocaleDescriptionDto> localeDescList = new ArrayList<ArLocaleDescriptionDto>();
		ArLocaleDescriptionDto localeDescription = null;
		  FileReader reader = null;
		    try {
		    	reader = new FileReader("/Vpas_develop/src/main/resources/application.properties");
		    	Properties p=new Properties();  
		        p.load(reader);
		        System.out.println(p.getProperty("S8173"));
		        localeList = (List<ArLocale>) localeRepository.findAll();
		        for(ArLocale locale : localeList) {
		        	localeDescription = new ArLocaleDescriptionDto();
		        	localeDescription.setLocaleId(locale.getLocaleId());
		        	localeDescription.setDescription( p.getProperty(locale.getDescription()));
		        	localeDescList.add(localeDescription);
//		        	localeDetails.put(locale.getLocaleId(), p.getProperty(locale.getDescription()));
		        }
		    }catch(Exception e) {
		    	e.printStackTrace();
		    }
		// TODO Auto-generated method stub
		
		
		
		
		
		return localeDescList;
	}
	
	
	
}