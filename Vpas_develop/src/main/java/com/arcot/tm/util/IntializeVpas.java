package com.arcot.tm.util;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

import com.arcot.tm.model.AdminLevels;
import com.arcot.tm.model.AdminOperations;



public final class IntializeVpas {

	
	
	@Bean
	public static void loadAdminLevelCache(ServletContext application) throws ArcotException {
		AdminLevels al = null;
		al = (AdminLevels) application.getAttribute("AL");
		if (al == null) {
			al = new AdminLevels();
			AdminOperations.populateAdminLevels(al);
			application.setAttribute("AL", al);
			
			//For 2FA Backward compatibility, few manipulation is required on admin level params
			AdminOperations.setAdminLevelCache(al);

		}
	}

	@Bean
	public static boolean reloadAdminLevelCache(ServletContext application) {
		try {
			AdminLevels altemp = new AdminLevels();
			if (AdminOperations.populateAdminLevels(altemp) != 0) {
				return false;
			}

			application.removeAttribute("AL");
			application.setAttribute("AL", altemp);
			
			//For 2FA Backward compatibility, few manipulation is required on admin level params
			AdminOperations.setAdminLevelCache(altemp);

		} catch (Exception e) {
			return false;
		}
		return true;
	}
}
