package com.arcot.tm.util;

import java.util.HashMap;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Vector;

public class EnrollmentFieldValues {

	private int UI_LABEL = 0;
	private int UI_FIELD_NAMES = 1;
	private int UI_FIELD_TYPES = 2;
	private int UI_SCR_NUM = 3;
	private int UI_FIELD_SEQ = 4;
	private int SCR1_ELEMENTS = 15;
	private int SCR3_ELEMENTS = 4;


	private HashMap fieldValuesHashMap = new HashMap();
	private Vector mandatoryFields = new Vector();

	public EnrollmentFieldValues() {

		fieldValuesHashMap.put("CARDNUMBER", "S1001|cardNumber|text|S1|1");
		fieldValuesHashMap.put("CARDHOLDERNAME", "S1011|name|text|S1|2");
		fieldValuesHashMap.put("DATEEXPIRED", "S1013|expMonth|select|S1|3");
		fieldValuesHashMap.put("VERIFICATIONCODE", "S1014|cvv2|password|S1|4");
		fieldValuesHashMap.put("ADDRESS", "S1015|streetAddress|text|S1|5");
		fieldValuesHashMap.put("CITY", "S1016|city|text|S1|6");
		fieldValuesHashMap.put("STATE", "S1017|state|select|S1|7");
		fieldValuesHashMap.put("ZIP", "S1018|zip|text|S1|8");
		fieldValuesHashMap.put("EMAILADDR", "S1019|emailAddress|text|S1|9");
		fieldValuesHashMap.put("SSN", "S1020|ssn|text|S1|10");
		fieldValuesHashMap.put("DOB", "S1021|dob|text|S1|11");
		fieldValuesHashMap.put("DLNUMBER", "S1022|driverLicenseNum|text|S1|12");
		fieldValuesHashMap.put("DLSTATE", "S1017|driverLicenseState|select|S1|13");
		fieldValuesHashMap.put("HOMEPHONE", "S1023|homePhone|text|S1|14");
		fieldValuesHashMap.put("ISPHONELISTED", "S8302|isPhoneListed|checkbox|S1|15");
		fieldValuesHashMap.put("SECRETCODE", "S1043|pin|password|S3|1");
		fieldValuesHashMap.put("HINTQ", "S6936|hintQuestion|text|S3|2");
		fieldValuesHashMap.put("HINTANSWER", "S1047|hintAnswer|password|S3|3");
		fieldValuesHashMap.put("SHOPPERID", "S1054|shopperID|text|S3|4");

		mandatoryFields.add("CARDNUMBER");
		mandatoryFields.add("CARDHOLDERNAME");
		mandatoryFields.add("DATEEXPIRED");
		mandatoryFields.add("SECRETCODE");
		//mandatoryFields.add("SHOPPERID");
		mandatoryFields.add("HINTQ");
		mandatoryFields.add("HINTANSWER");

	} // end of constructor


/**
	Returns relevant value for given Database field name.
*/
	private String getValue(String databaseFiledNames, int locationInString) {

		int counter = 0;
		String dataValue = new String();

		String keyValue = (String) fieldValuesHashMap.get(databaseFiledNames);
		StringTokenizer stringToken = new StringTokenizer(keyValue, "|");

		while(stringToken.hasMoreTokens()) {

			dataValue = stringToken.nextToken();
			if (locationInString == counter)
				break;

			counter++;
		} //end while loop

		return dataValue;
	} //end of "getValue" method


/**
	Returns UI Label for Database field name.
*/
	public String getUILabel(String databaseFiledNames) {

		return getValue(databaseFiledNames, UI_LABEL);

	} //end of "getUILabel" method


/**
	Returns UI Field Name for Database field name.
*/
	public String getUIFieldNames(String databaseFiledNames) {

		return getValue(databaseFiledNames, UI_FIELD_NAMES);

	} //end of "getUIFieldNames" method


/**
	Returns UI Field Type for Database field name.
*/
	public String getUIFieldType(String databaseFiledNames) {

		return getValue(databaseFiledNames, UI_FIELD_TYPES);

	} //end of "getUIFieldType" method


/**
	Returns UI Field Sequence for Database field name.
*/
	public String getUIFieldSequence(String databaseFiledNames) {

		return getValue(databaseFiledNames, UI_FIELD_SEQ);

	} //end of "getUIFieldType" method


/**
	Returns all fields for database
*/
	private Vector getAllFields() {

		Set fieldKeySet = fieldValuesHashMap.keySet();
		Vector fieldVector = new Vector(fieldKeySet);

		return fieldVector;

	} //end of "getAllFields" method


/**
	Returns all fields for specific screen
*/
	private Vector getScreenFieldList(String screenName, int screenElements) {

		String fieldName = new String();
		int screenLoc = 0;

		Vector screenFields = new Vector();
		Vector allFields = getAllFields();

		for (int i = 0; i < screenElements; i++)
			screenFields.add("");

		for (int i = 0; i < allFields.size(); i++) {
			fieldName = (String) allFields.elementAt(i);
			if (getValue(fieldName, UI_SCR_NUM).equals(screenName)) {
				screenLoc = Integer.parseInt(getValue(fieldName, UI_FIELD_SEQ));
				screenFields.set(screenLoc - 1, fieldName);
			} //end of if check
		} //end of for loop

		return screenFields;
	} //end of "getScreenThreeFieldList" method


/**
	Returns all fields for first screen
*/
	public Vector getScreenOneFieldList() {

		return getScreenFieldList("S1", SCR1_ELEMENTS);

	} //end of "getScreenOneFieldList" method


/**
	Returns all fields for third screen
*/
	public Vector getScreenThreeFieldList() {

		return getScreenFieldList("S3", SCR3_ELEMENTS);

	} //end of "getScreenThreeFieldList" method


/**
	Returns all fields for all the screen
*/
	public Vector getAllScreenFieldsList() {

		Vector scr1 = getScreenOneFieldList();
		Vector scr3 = getScreenThreeFieldList();

		scr1.addAll(scr3);

		return scr1;
	} //end of "getAllScreenFieldsList" method


/**
	Returns all mandatory fields for all the screen
*/
	public Vector getMandatoryFieldsList() {

		return mandatoryFields;
	} //end of "getMandatoryFieldsList" method


/**
	Returns difference of two vectors
*/
	public Vector getDiffVector(Vector a_vecFirst, Vector a_vecSecond) {

		Vector m_vecResult = new Vector();
      	int m_iVecSize = a_vecFirst.size();

      	for(int i=0 ; i<m_iVecSize ; i++) {
			if(!(a_vecSecond.contains(a_vecFirst.elementAt(i)))) {
				m_vecResult.add(a_vecFirst.elementAt(i));
			} //end of if check
      	} //end of for loop

      	return m_vecResult ;
  	} //end of "getDiffVector" method


/**
	Returns the given in sorted order
*/
	public Vector getFieldsInSortedOrder(Vector listOfFields) {

		int temp = 0;
		int seqNumber = 0;
		int numberOfElements = listOfFields.size();
		int[] screenSeq = new int[numberOfElements];
		String databaseFieldName = "";
		HashMap listOfFieldsMap = new HashMap();
		Vector sortedVector = new Vector(numberOfElements);

		for (int i = 0; i < numberOfElements; i++) {
			databaseFieldName = (String) listOfFields.elementAt(i);
			seqNumber = Integer.parseInt(getUIFieldSequence(databaseFieldName));
			screenSeq[i] = seqNumber;
			listOfFieldsMap.put(new Integer(seqNumber), databaseFieldName);
		} //end of for loop

		for (int i = 0; i < screenSeq.length - 1; i++) {
			for (int j = i + 1; j < screenSeq.length; j++) {
				if (screenSeq[i] > screenSeq[j]) {
					temp = screenSeq[i];
					screenSeq[i] = screenSeq[j];
					screenSeq[j] = temp;
				} //end of if check
			} //end of for loop "j"
		} //end of for loop "i"

		for (int i = 0; i < screenSeq.length; i++) {

			sortedVector.add((String) listOfFieldsMap.get(new Integer(screenSeq[i])));
		} //end of for loop

		return sortedVector;
	} //end of "getAllScreenFieldsList" method

} // end of "EnrollmentFieldValues" class
