package com.arcot.tm.util;

public class ArcotException extends Exception {
	private String errCode = null;
	private String[] args = null;

	public ArcotException(String errCode, String message) {
		super(message);
		this.errCode = errCode;
	}

	public ArcotException(String errCode, String[] args, String message) {
		super(message);
		this.errCode = errCode;
		this.args = args;
	}

	public ArcotException(String errCode) {
		super("No message available here");
		this.errCode = errCode;
	}

	public ArcotException(String errCode, String[] args) {
		this.errCode = errCode;
		this.args = args;
	}

	public ArcotException(String message, Throwable cause) {
		super(message, cause);
		if (cause instanceof ArcotException) {
			errCode = ((ArcotException) cause).getErrorCode();
		}
	}

	public ArcotException(String errCode, String message, Throwable cause) {
		super(message, cause);
		this.errCode = errCode;
	}

	public ArcotException(String errCode, String[] args, String message, Throwable cause) {
		super(message, cause);
		this.errCode = errCode;
		this.args = args;
	}

	public String getErrorCode() {
		return this.errCode;
	}

	public String getErrorMessage() {
		return this.getMessage();
	}

	public Object[] getArgs() {
		return args;
	}

	public static final String AE_REQUEST_FALED = "E8344"; // = Sorry, we are unable to process your request at this time. Please contact an Administrator.

	public static final String AE_NO_ROWS_FOUND = "E7601"; // = No matching records found

	public static final String AE_ERROR_FETCHING_RECORD = "E7602"; // = Error while retrieving the records

	public static final String AE_GENERAL_ERROR = "E8341"; // = General Error

	public static final String AE_DATABASE_DOWN = "E7000"; // = Sorry, we are unable to process your registration at this time because database service is temporarily
															// unavailable.<br>Please try again later.

	public static final String AE_SESSION_TIMEOUT = "E7001"; // = Sorry, registration session has timed out.<br>Please start again.

	public static final String AE_INVAILD_VISA_CARD = "E8337"; // = Verified by VISA service is available only to VISA customers. If you have entered the card number incorrectly,
																// please enter the correct VISA card number in the space below.

	// ??public static final String AE__EXPDATE = "E7003"; //= The card expiration date must be the same or later than today's date.

	public static final String AE_INVALID_EXPDATE = "E7004"; // = The card expiration date is incorrect.<br>Please enter the month and year separated by '/'.

	public static final String AE_EXPDATE_MISSING = "E7005"; // = The card expiration date is required.

	public static final String AE_INVALID_BIRTHDATE = "E7006"; // = Birth date must be the same or earlier than today's date.

	// ??public static final String AE__BIRTHDATE = "E7007"; //= Birth date must be the same or earlier than the card expiration date.

	public static final String AE_INCORRECT_BIRTHDATE = "E7008"; // = Birth date is incorrect. Please enter the month, day, and year separated by '/'.

	public static final String AE_INVALID_REQ_FIELDS = "E7009"; // = Some of the required fields are not filled in or invalid. Please enter or correct them and submit again.

	public static final String AE_MISSING_FIELDS = "E7010"; // = Some of the required fields are not filled in. Please enter them and submit again.

	public static final String AE_INVALID_FIELDS = "E7011"; // = Some of the fields are invalid. Please re-enter them and submit again.

	public static final String AE_CARD_NOT_ELIGIBLE = "E8338"; // = Sorry, we are unable to process your registration at this time because your card is not yet eligible for
																// Verified by VISA service. <br>Please try again with another card.

	public static final String AE_SERVICE_UNAVAILABLE = "E7014"; // = Sorry, we are unable to process your registration at this time because the service is temporarily unavailable.
																	// <br>Please try again later.

	public static final String AE_CARD_NOT_CONFIRMED = "E7015"; // = Sorry, we are unable to process your registration at this time because the card information you entered could
																// not be confirmed. <br>Please try again with correct card information.

	public static final String AE_ADDRESS_NOT_CONFIRMED = "E7016"; // = Sorry, we are unable to process your registration at this time because the address you entered could not be
																	// confirmed. <br>Please try again with correct card billing address.

	public static final String AE_CODE_NOT_CONFIRMED = "E7017"; // = Sorry, we are unable to process your registration at this time because the three digit card verification code
																// you entered could not be confirmed. <br>Please try again with correct card verification code.

	public static final String AE_ANSWERS_NOT_COMPLETE = "E7018"; // = Sorry, we are unable to process your registration at this time because you did not answer sufficient number
																	// of issuer questions correctly. <br>Please try again with correct answers.

	public static final String AE_IPGS_FALURE = "E7019"; // = Sorry, we are unable to process your registration at this time because the external verification system is temporarily
															// unavailable at this time. <br>Please try again later.

	public static final String AE_IPGS_NOT_CONFIRMED = "E7020"; // = Sorry, we are unable to process your registration at this time because the external verification system could
																// not confirm your information. <br>Please try again later with correct information.

	public static final String AE_PWDS_NOT_MATCHED = "E7021"; // = Your passwords do not match. Please try entering them again.

	public static final String AE_INCOMPLETE_HINT = "E7022"; // = Hint/Response is required.

	public static final String AE_HINT_NOT_FOUND = "E7023"; // = Hint is required.

	public static final String AE_RES_NOT_FOUND = "E7024"; // = Response is required.

	public static final String AE_RES_CONTAINS_PWD = "E7025"; // = Response can not contain password.

	public static final String AE_REQ_FIELDS_MISSING = "E7026"; // = All fields are required. Please enter all fields.

	public static final String AE_INCORRECT_CARD_INFO = "E7027"; // = The card information you entered can not be found in the database. Please try again with correct information.

	public static final String AE_PWD_EXPIRED = "E7028"; // = The temporary password has expired. Please contact Customer Service to get a new password.

	public static final String AE_RES_NOT_MATCHED = "E7029"; // = Your responses do not match. Please try entering them again.

	public static final String AE_SUSPENDED_PRIV = "E7030"; // = Your privilege to access this area has been suspended. Please contact a customer service representative.

	public static final String AE_PWD_NOT_IN_DATABASE = "E7031"; // = The password you entered does not match with the database. Please try again with correct password.

	public static final String AE_RES_CONTAINS_HINT = "E7032"; // = Response can not contain hint.

	public static final String AE_EMPTY_CARDNUMBER = "E7033"; // = Card number is not filled in. Please enter it and submit again.

	public static final String AE_INVALID_CARDNUMBER = "E7034"; // = Card number is invalid. Please re-enter it and submit again.

	// ??public static final String AE_ = "E7035"; //= The cardnumber and cardholder name combination that you entered can not be found in the database. Please re-enter and submit
	// again.

	/*
	 * ############################################ # Error messages for 'Update Info' section # ############################################
	 */

	public static final String AE_NO_EMAIL = "E7100"; // = Email address required

	public static final String AE_INVALID_EMAIL = "E7101"; // = Invalid email address

	public static final String AE_NO_PWD = "E7102"; // = Password required

	public static final String AE_PWD_TOBE_VERIFIED = "E7103"; // = Password needs to be verified

	public static final String AE_INVALID_PWD_LENGTH = "E7104"; // = Password length should be between 6 and 10

	public static final String AE_PWD_NOT_VERIFIED = "E7105"; // = Password is not verified correctly

	public static final String AE_NO_HINT_FOR_UPDATE = "E7106"; // = Hint required

	public static final String AE_NO_RES = "E7107"; // = Response required

	public static final String AE_INVALID_PWD = "E7108"; // = Password should have letters and digits only!

	public static final String AE_NO_PAM = "E7126"; // = Personal Assurance Message required

	public static final String AE_NO_NAME = "E7110"; // = Name required

	public static final String AE_NO_CARDNUMBER = "E7111"; // = Card number required

	public static final String AE_INVALID_CARDNUMBER_LENGTH = "E7112"; // = Invalid card number. Card number length should be between 13 and 19

	public static final String AE_DATABASE_NOT_AVAILABLE = "E7113"; // = Sorry! Our database is temporarily unavailable!

	public static final String AE_INCORRECT_LOGIN = "E7114"; // = Sorry! Wrong login name for this card!

	public static final String AE_CARD_NOT_ENROLLED = "E7115"; // = Sorry! This card is not enrolled!

	public static final String AE_INCORRECT_PWD = "E7116"; // = Sorry! Wrong password!

	public static final String AE_UPDINFO_RES_CONTAINS_PWD = "E7117"; // = Response can not contain password!

	public static final String AE_UPDINFO_RES_CONTAINS_HINT = "E7118"; // = Response can not contain hint!

	// new 4.0 Update Info error message #

	public static final String AE_UPDATE_PROFILE_FAILED = "E7119"; // = A database error occurred while your profile was being updated. Please try again later.

	public static final String AE_UPDINFO_SUCCESS = "E7120"; // = Your information has been successfully updated.

	public static final String AE_DEACTIVATE_FAILED = "E7121"; // = A database error occurred while your account was being deactivated. Please try again later.

	public static final String AE_SERVICE_CANCELLED = "E8339"; // = Your Verified by VISA Service has been cancelled.

	public static final String AE_VERIFY_PWD_FAILED = "E7123"; // = We are unable to verify your password. Please contact a customer service representative.

	public static final String AE_NO_CHANGE = "E7124"; // = You didn't make any change.

	public static final String AE_UPDINFO_DATABASE_DOWN = "E7125"; // = The database is currently unavailable. Please try again later.

	// ###########################################
	// # Error messages for 'Web Admin' section #
	// ############################################

	public static final String AE_WEBADMIN_NO_PWD1 = "E7200"; // = Password Required

	public static final String AE_WEBADMIN_INVALID_PWD1 = "E7201"; // = Invalid Password

	public static final String AE_WEBADMIN_NO_LOGIN = "E7202"; // = Login Name Required

	public static final String AE_WEBADMIN_NO_PWD2 = "E7203"; // = Password Required

	public static final String AE_WEBADMIN_INCORRECT_PWD_LENGTH = "E7204"; // = Password should be 6 to 10 characters long

	public static final String AE_WEBADMIN_USER_ADDED = "E7205"; // = User %s added

	public static final String AE_WEBADMIN_USER_ALREADY_EXISTS = "E7206"; // = User Name %s already exists

	public static final String AE_WEBADMIN_USER_DELETED = "E7207"; // = User %s deleted

	public static final String AE_WEBADMIN_NO_ADMINUSER = "E7208"; // = No Administrative Users present

	public static final String AE_WEBADMIN_LOGIN_REQUIRED = "E7209"; // = Login Name Required

	public static final String AE_WEBADMIN_INVALID_LOGIN = "E7210"; // = Invalid Login Name

	public static final String AE_WEBADMIN_PWD_REQUIRED = "E7211"; // = Password Required

	public static final String AE_WEBADMIN_INVALID_PWD2 = "E7212"; // = Invalid Password

	public static final String AE_WEBADMIN_NO_DATE = "E7213"; // = Enter a date

	public static final String AE_WEBADMIN_INVALID_DATE = "E7214"; // = Invalid date:%s

	public static final String AE_WEBADMIN_NO_ENROLLMENT = "E7215"; // = No Enrollments on %s

	public static final String AE_WEBADMIN_INVALID_DAYS = "E7216"; // = Invalid value for number of days

	public static final String AE_NO_ENROLLMENT_FAILURE = "E7217"; // = No Enrollment Failures in the last %s day(s)

	public static final String AE_NO_ENROLLMENT_SUCCESS = "E7218"; // = No Successful Enrollments in the last %s day(s)

	public static final String AE_WEBADMIN_NO_CARDNUMBER = "E7219"; // = Enter a card number

	public static final String AE_WEBADMIN_NO_STATUS = "E7220"; // = Individual status not found

	public static final String AE_WEBADMIN_NO_CARDS_EXPIRING = "E7221"; // = There are no cards expiring in %s

	public static final String AE_WEBADMIN_ENTER_CARDNUMBER = "E7222"; // = Enter a card number

	public static final String AE_WEBADMIN_NO_CARDHOLDER = "E7223"; // = Enter a Card Holder Name

	public static final String AE_WEBADMIN_NO_EMAIL = "E7224"; // = Enter customer's Email Address

	public static final String AE_WEBADMIN_ENTER_PWD = "E7225"; // = Enter a Password for the cardholder to use

	public static final String AE_WEBADMIN_INVALID_PWD_LENGTH = "E7226"; // = Password should be 6 to 10 characters long

	public static final String AE_CARDHOLDER_DEACTIVATED = "E7227"; // = Cardholder(s) %s deactivated

	public static final String AE_NO_ENROLLED_CARDHOLDER = "E7228"; // = No cardholders enrolled with card number %s

	public static final String AE_WEBADMIN_CARDHOLDER_ADDED = "E7229"; // = Cardholder %s added

	public static final String AE_WEBADMIN_CHOOSE_FOR_DEACTIVATING = "E7230"; // = Choose cardholders to deactivate

	public static final String AE_WEBADMIN_NO_MORE_EXPIRY = "E7231"; // = No more cards expiring by %s

	public static final String AE_WEBADMIN_NEEDTO_LOGIN = "E7232"; // = You need to login

	public static final String AE_WEBADMIN_INVALID_CARDNUMBER = "E7233"; // = Invalid Card Number

	public static final String AE_WEBADMIN_INVALID_EXPIRY_DATE = "E7234"; // = Invalid Expiration date

	public static final String AE_WEBADMIN_CARD_EXPIRED = "E7235"; // = Card expired

	public static final String AE_WEBADMIN_CARD_NOTIN_RANGE = "E7236"; // = Card out of range

	public static final String AE_WEBADMIN_ALREADY_EXISTS = "E7237"; // = Card number and name already exists

	public static final String AE_WEBADMIN_INVALID_CHAR = "E7238"; // = Special Characters not allowed in %s

	public static final String AE_WEBADMIN_NOT_ENROLLED = "E7239"; // = Cannot reset password for a cardholder who is not enrolled

	public static final String AE_WEBADMIN_DATABASE_DOWN = "E7240"; // = We are sorry, the Database Service is currently not available, please try after some time

	public static final String AE_WEBADMIN_INCORRECT_LOGIN = "E7241"; // = Login name should have minimum 4 letters and should not start with _

	public static final String AE_WEBADMIN_DATABASE_CONNECTION_FULL = "E7262"; // = We are sorry, the server is experiencing a high volume of requests at this time, please try
																				// again later.

	public static final String AE_WEBADMIN_PWD_NOT_MATCHING = "E7242"; // = Passwords do not match. Please try entering them again

	public static final String AE_WEBADMIN_CREATED_TEMPPWD = "E7243"; // = Temporary password created for cardholder %s

	public static final String AE_NO_DEACT_IN_RANGE = "E7244"; // = No Deactivated cards from %s to %s

	public static final String AE_NO_MANUALREG_IN_RANGE = "E7245"; // = No Manually registered cards from %s to %s

	public static final String AE_NO_FAILED_TRANS_IN_RANGE = "E7246"; // = No Failed Transactions from %s to %s

	public static final String AE_NO_DEACT_FOR_CARD = "E7247"; // = No Deactivation record found for card %s

	public static final String AE_NO_MANUALREG_FOR_CARD = "E7248"; // = No Manually registered record found for card %s

	public static final String AE_NO_FAILED_TRANS_FOR_CARD = "E7249"; // = No Failed transactions record found for card %s

	public static final String AE_INVALID_FAILURE_COUNT = "E7250"; // = Invalid value for number of failures

	public static final String AE_NO_MATCH_FOR_FAILED_TRANS = "E7251"; // = No card holder has %s or more failed transaction(s)

	public static final String AE_INVALID_DATE_RANGE = "E7252"; // = Invalid date range

	public static final String AE_INVALID_DATE = "E7253"; // = Enter a date not later than current date

	public static final String AE_NO_DATA_FOUND = "E7254"; // = No card holder has %s failed transaction(s)

	// ??public static final String AE_ = "E7255"; //= No card holder has %s or less failed transaction(s)

	// ??public static final String AE_ = "E7256"; //= No card holder has more than %s failed transaction(s)

	// ??public static final String AE_ = "E7257"; //= No card holder has less than %s failed transaction(s)

	public static final String AE_NO_SUCCESS_IN_RANGE = "E7258"; // = No successful enrollment from %s to %s

	public static final String AE_NO_FAILURES_IN_RANGE = "E7259"; // = No failed enrollment from %s to %s

	public static final String AE_NO_ENROLLMENT_IN_RANGE = "E7260"; // = No enrollment from %s to %s

	public static final String AE_NO_SIMULTANEOUS_ACCESS = "E7261"; // = The same user can not access multiple reports at the same time.

	// for issuer answers in admin

	public static final String AE_ISSANS_DATABASE_ERROR = "E7300"; // = Database error, please try later.

	public static final String AE_ISSANS_INVALID_CARDNUMBER = "E7301"; // = Card number is not valid, please try another one.

	public static final String AE_ISSANS_NO_DATA = "E7302"; // = Can't find data for the card with the name you picked, please try again.

	public static final String AE_ISSANS_NO_QUESTION = "E7303"; // = Can't find question with such id.

	public static final String AE_ISSANS_NO_CARDNUMBER = "E7304"; // = Enter a card number.

	public static final String AE_ISSANS_INVALID_LENGTH = "E7305"; // = Length of card number should be between 13 and 19.

	public static final String AE_ISSANS_NO_ANSWER = "E7306"; // = Answer can't be empty.

	public static final String AE_ISSANS_UPDATED = "E7307"; // = Issuer answers have been updated successfully.

	public static final String AE_ISSANS_INVALID_PWD = "E7308"; // = Password should have letters and digits only.

	public static final String AE_UNAUTHORIZED_BANK_ACCESS = "E7812"; // = Not allowed to access the information of a bank which you do not control

	public static final String AE_BANK_ID_REQUIRED = "E7814"; // BANKID parameter is required as per report.xml but missing in parameter list

	// for admin trail logs
	// No activity log from <from date> to <to date>
	public static final String AE_NO_ACT_LOG = "E7400"; // = No activity log from %s to %s
	// No report access log from <from date> to <to date>
	public static final String AE_NO_REP_LOG = "E7401"; // = No report access log from %s to %s

	public static final String AE_DIFF_PWD = "E7402"; // = New Password should be different from Old Password
	public static final String AE_PWD_CHANGED = "E7403"; // = Password changed
	public static final String AE_INCORRECT_OLD_PWD = "E7404"; // = Old password is incorrect
	public static final String AE_NO_OLD_PWD = "E7405"; // = Old Password Required
	public static final String AE_NO_NEW_PWD = "E7406"; // = New Password Required
	public static final String AE_CONFIRM_NEW_PWD = "E7407"; // = Please confirm New Password

	// for admin in vpas3.0
	public static final String AE_REP_EXPIRED = "E7510"; // = Report has expired, please submit your query again.
	public static final String AE_SUBMIT_QUERY = "E7511"; // = Please submit your query.
	public static final String AE_CHOOSE_REPORT = "E7512"; // = Please choose a report.

	// for Administrator Profile
	public static final String AE_PROFILE_UPDATED = "E7520"; // = User %s's access control profile updated
	public static final String AE_NO_PRIVILEDGE = "E7521"; // = You have no privilege to access this page
	public static final String AE_NO_START_DATE = "E7522"; // = Please specify the start date
	public static final String AE_PREFERENCES_UPDATED = "E7523"; // = Your preferences have been updated
	// ###############################
	// # end of #
	// # new messages in VPAS4.0 #
	// # part 1 #
	// # translated by 7/10/2001 #
	// # #
	// ################################
	// ################################
	// # beginngin of #
	// # new messages in VPAS4.0 #
	// # part 2 #
	// # #
	// ################################

	// Successful Transaction
	public static final String AE_NO_SUCC_TRANS_IN_RANGE = "E7530"; // = No Successful Transactions from %s to %s
	public static final String AE_NO_SUCC_TRANS_FOR_CARD = "E7531"; // = No Successful transactions found for card %s

	public static final String AE_DIFF_LANG = "E7535"; // = This card has been registered in a different language. Please register your card in the same language as before.
	public static final String AE_CARD_ALREADY_REGISTERED = "E7536"; // = This cardholder/card is already registered.

	// ViewAdminReport.jsp
	public static final String AE_REFINE_SEARCH = "E7540"; // = Not all matching records are retrieved from the database. To retrieve all matching records, please refine your
															// search creteria.
	// admin/resetPassword.jsp
	public static final String AE_INCORRECT_EXPIRY_DATE = "E7542"; // = Incorrect expiration date for this card

	// Enrollment Customization by bank super admin.
	public static final String AE_CUS_UPDATED = "E7550"; // = Enrollment Customization Updated
	public static final String AE_NO_VERIFICATION_CODE = "E7551"; // = Please select the Verification Code since the bank has opted for this policy
	public static final String AE_NO_ADDRESS = "E7552"; // = Please select the Address since the bank has opted for this policy
	public static final String AE_NO_ZIP = "E7553"; // = Please select Zip since the bank has opted for this policy

	// Adding User Agent.
	public static final String AE_NO_USERAGENT = "E7560"; // = User Agent Required
	public static final String AE_USERAGENT_ALREADY_EXISTS = "E7561"; // = User Agent already exists
	public static final String AE_HTTPUAID_ALREADY_EXISTS = "E7562"; // = HTTPUAID already exists
	public static final String AE_USERAGENT_ADDED = "E7563"; // = User Agent %s added

	// Adding Folder.
	public static final String AE_NO_FOLDERNAME = "E7570"; // = Folder Name Required
	public static final String AE_FOLDERNAME_ALREADY_EXISTS = "E7571"; // = Folder Name already exists
	public static final String AE_FOLDERID_ALREADY_EXISTS = "E7572"; // = FolderID already exists
	public static final String AE_FOLDER_ADDED = "E7573"; // = Folder %s added

	// Adding Device.
	public static final String AE_ADDDEVICE_NO_BANK = "E7580"; // = Select Bank from list
	public static final String AE_ADDDEVICE_NO_USERAGENT = "E7581"; // = User Agent Required
	public static final String AE_ADDDEVICE_NO_ACCEPT = "E7582"; // = Accept String Required
	public static final String AE_ADDDEVICE_NO_DEVCATEGORY = "E7583"; // = Device Category Required
	public static final String AE_ADDDEVICE_NO_LOCALE = "E7584"; // = Locale Required
	public static final String AE_ADDDEVICE_NO_ACSURL = "E7585"; // = ACS URL Required
	public static final String AE_DEVICE_ALREADY_EXISTS = "E7586"; // = Device already exists
	public static final String AE_DEVICE_ADDED = "E7587"; // = Device added}

	public static final String AE_CONFIG_ALREADY_EXISTS = "E8317"; // = CallOut Config Already Exists
	public static final String AE_ISSUERCALLOUTCONFIG_ALREADY_EXISTS = "E8326"; // Issuer CallOut configuration already exists
	public static final String AE_ISSUER_DIRECTORY_ALREADY_EXISTS = "E8332"; // ES Issuer Directory already exists
	public static final String AE_ADMINFORBANK_ALREADY_EXISTS = "E8104"; // Administrator for the Issuer already exists
	// upload not included in errorMessages.Properties as no Localization is done for Upload
	public static final String AE_SCRAMBLE_ERROR = "E9001"; // = Scramble type invalid for this parameter

	// generic
	public static final String AE_RESULT_SET_EMPTY = "E7601"; // = empty result set
	public static final String AE_NO_RECORD_FOUND_TO_UPDATE = "E9224"; // = No record found to update. Update returned zero records

	public static final String AE_WEBADMIN_ADMIN_ALREADY_EXISTS = "E8340"; // = Admin already exists
	public static final String AE_WEBADMIN_ADMIN_ALREADY_DELETED = "E9220";
	public static final String AE_WEBADMIN_TEMPLATE_ALREADY_EXISTS = "E9204"; // = Template already exists

	public static final String AE_RA_GROUPNAME_ALREADY_EXISTS = "E7739";
	public static final String AE_RA_RULENAME_ALREADY_EXISTS = "E7740";
	public static final String AE_RA_RULEMNEMONIC_ALREADY_EXISTS = "E7741";
	public static final String AE_SAML_IDP_NAME_ALREADY_EXISTS = "E7857";
	public static final String AE_SAML_ISSUER_NAME_ALREADY_EXISTS = "E7858";
	public static final String AE_ZERO_RECORDS_UPDATED = "E9561"; // = No records updated
	public static final String AE_ZERO_RECORDS_INSERTED = "ZERO_RECORDS_INSERTED"; // = Zero records updated

	public static final String AE_ISSUER_ANSWER_ALREADY_EXISTS = "ISSUER_ANSWER_ALREADY_EXISTS";
	public static final String AE_CARDHOLDER_ALREADY_EXISTS = "CARDHOLDER_ALREADY_EXISTS";

	public static final String AE_BANKIDINDEXTODECRYPT_MISSING_IN_REPORT = "E9614";
	public static final String AE_EXCEEDED_REPORT_MAX_RECORDS = "E9616"; // = We are sorry, the server is experiencing a high volume of requests at this time, please try again
																			// later.

	public static final String AE_REPORT_TBL_SPC_NOT_PRESENT = "E9618"; // = We are sorry, the Database Service is currently not available, please try after some time

	public static final String AE_RANGE_SIGNING_CERTIFICATE_ALREADY_EXISTS = "E9701";
	public static final String AE_SIGNING_CERTIFICATE_UPLOAD_EXCEPTION = "E9702";

	/*
	 * constants for TTP Configurations
	 */

	public static final String AE_TTP_IDP_NAME_ALREADY_EXISTS = "E12100";
	public static final String AE_TTP_CONFIGID_REFERENCED = "E12104";

	public static final String AE_INVALID_PARAM = "E12102";
	
	public static final String TD_RA_ORG_NAME_FK_VIOLATION="TD_RA_ORG_NAME_FK_VIOLATION";
	
	public static final String AE_NO_MIGRATION_DATA_AVLB="No Migration Data Available";
}