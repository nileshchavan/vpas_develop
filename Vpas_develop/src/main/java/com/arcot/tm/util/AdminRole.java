package com.arcot.tm.util;

public enum AdminRole {

	ISSUER(3),GLOBAL(2),MASTER(1),CSR(4);
	
	private int value;

	public int getValue() {
		return value;
	}
	
	private AdminRole(int value) {
       this.value = value;
	}
}
