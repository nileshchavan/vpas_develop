//package com.arcot.tm.util;
//
//import java.sql.Timestamp;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.Locale;
//import java.util.TimeZone;
//
//import com.arcot.dboperations.DBHandler;
//import com.arcot.dboperations.admin.AdminSelectOperations;
//import com.arcot.logger.ArcotLogger;
//
//public class TimeUtil
//{
//	public static TimeZone	GMTTimeZone;
//
//	private static float localTimeZoneGMTOffsetInDays;
//	private static float localTimeZoneGMTOffsetInDays_DST; //DST - Daylight Saving Time
//
//	static
//	{
//		GMTTimeZone = TimeZone.getTimeZone("GMT");
//
//		// Birla this is the time zone where ES/Admin is running
//		/*SimpleTimeZone localTimeZone = (SimpleTimeZone)TimeZone.getDefault() ;
//		localTimeZoneGMTOffsetInDays = ( (float)localTimeZone.getRawOffset() )/(1000*60*60*24);*/
//
//		//Changed to take care of daylight saving time also.
//		Calendar cl = Calendar.getInstance();
//		localTimeZoneGMTOffsetInDays = ( (float)cl.get(Calendar.ZONE_OFFSET))/(1000*60*60*24);
//		localTimeZoneGMTOffsetInDays_DST = ( (float)cl.get(Calendar.DST_OFFSET))/(1000*60*60*24);
//	}
//
//		public static final Calendar
//	getGMTCalendar()
//	{
//		return Calendar.getInstance(GMTTimeZone, Locale.US);
//	}
//
//		public static Timestamp
//	getTimestamp()
//	{
//		return new Timestamp(System.currentTimeMillis());
//	}
//
//	public static float getTimeZoneOffsetInDays()
//	{
//		return localTimeZoneGMTOffsetInDays;
//	}
//
//	public static String getTimeZoneBasedSysdateString()
//	{
//		return getTimeZoneBasedSysdateString(DBHandler.dbType);
//	}
//
//	// Returns current date time in GMT
//	public static String getTimeZoneBasedSysdateString(int dbType) {
//		if (dbType == ArcotConstants.ORACLE) {
//			return "CAST(SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) AS DATE)";
//		}
//		else if (dbType == ArcotConstants.DB2) {
//			float offset = getTimeZoneOffsetInDays();
//			String adjust = "";
//			if (offset > 0) {
//				adjust = "-" + String.valueOf(offset * 60 * 60 * 24) + " SECONDS -"
//						+ String.valueOf(localTimeZoneGMTOffsetInDays_DST * 60 * 60 * 24) + " SECONDS";
//			}
//			else {
//				adjust = "+" + String.valueOf(Math.abs(offset) * 60 * 60 * 24) + " SECONDS -"
//						+ String.valueOf(localTimeZoneGMTOffsetInDays_DST * 60 * 60 * 24) + " SECONDS";
//			}
//			return "(current timestamp" + adjust + ")";
//		}
//		throw new IllegalArgumentException("Invalid dbType");
//	}
//	
//	public static Date getTimeZoneBasedSysDate()
//	{
//		String dbTimeAsString = null;
//		Date dbTime = null;
//		try {
//			dbTimeAsString = AdminSelectOperations.getDBTime();
//			SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
//			formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
//			dbTime = formatter.parse(dbTimeAsString);
//		} catch (ArcotException e) {
//			ArcotLogger.logError("Error in getTimeZoneBasedSysDate()",e);
//			throw new RuntimeException(e);
//		} catch (ParseException e) {
//			ArcotLogger.logError("Error in getTimeZoneBasedSysDate()",e);
//			throw new RuntimeException(e);
//		}
//		return dbTime;
//	}
//	
//	// get the UTC date from DB ( without setting any timezone )
//	public static Date getDBSysDateAsUTC()
//	{
//		String dbTimeAsString = null;
//		Date dbTime = null;
//		try {
//			dbTimeAsString = AdminSelectOperations.getDBTime();
//			SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
//			dbTime = formatter.parse(dbTimeAsString);
//		} catch (ArcotException e) {
//			ArcotLogger.logError("Error in getDBSysDateAsUTC()",e);
//			throw new RuntimeException(e);
//		} catch (ParseException e) {
//			ArcotLogger.logError("Error in getDBSysDateAsUTC()",e);
//			throw new RuntimeException(e);
//		}
//		return dbTime;
//	}
//	
//	public static java.sql.Timestamp getMiliSecondToDate(long milisecond){
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSS");
//		java.sql.Date  resultdate =new java.sql.Date(milisecond);
//		java.sql.Timestamp resultTimestamp =new Timestamp(resultdate.getTime());
//		return resultTimestamp;
//	}
//	
//	
//	/*
//	 * Input	- duration in days
//	 * Output	- duration in milliseconds
//	 */
//	public static float convertToMilliSeconds(float days){
//		return (days*24*60*60*1000);
//	}
//	
//	public static Date getGMTTimeZoneBasedSysDateFromDB()
//	{
//		String dbTimeAsString = null;
//		Date dbTime = null;
//		try {
//			dbTimeAsString = AdminSelectOperations.getDBTime();
//			SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
////			formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
//			dbTime = formatter.parse(dbTimeAsString);
//		} catch (ArcotException e) {
//			ArcotLogger.logError("Error in getTimeZoneBasedSysDate()",e);
//			throw new RuntimeException(e);
//		} catch (ParseException e) {
//			ArcotLogger.logError("Error in getTimeZoneBasedSysDate()",e);
//			throw new RuntimeException(e);
//		}
//		return dbTime;
//	}
//}
