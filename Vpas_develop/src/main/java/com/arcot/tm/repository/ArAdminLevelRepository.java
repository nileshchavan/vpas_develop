package com.arcot.tm.repository;

import org.springframework.data.repository.CrudRepository;

import com.arcot.tm.entities.ArAdminLevel;

public interface ArAdminLevelRepository extends CrudRepository<ArAdminLevel, Integer>{

}
