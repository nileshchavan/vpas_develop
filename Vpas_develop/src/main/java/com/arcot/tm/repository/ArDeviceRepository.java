package com.arcot.tm.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.arcot.tm.entities.ArDevice;

@Repository
public interface ArDeviceRepository extends CrudRepository<ArDevice, Integer>{

	
}
