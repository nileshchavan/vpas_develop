package com.arcot.tm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.arcot.tm.entities.ArFolder;
import com.arcot.tm.model.LocaleFolderDto;

import com.arcot.tm.entities.ArLocale;

@Repository
public interface LocaleRepository extends CrudRepository<ArLocale, Integer> {

	
	@Query("select new com.arcot.tm.model.LocaleFolderDto(a.localeId,b.folderId) FROM ArLocale a,ArFolder b WHERE ltrim(rtrim(a.locale)) = ltrim(rtrim(b.folder))")
	 List<LocaleFolderDto> getFolderIdFromLocale();
}
