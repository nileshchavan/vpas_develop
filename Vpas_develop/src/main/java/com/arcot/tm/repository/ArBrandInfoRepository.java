package com.arcot.tm.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.arcot.tm.entities.ArBankInfo;
import com.arcot.tm.entities.ArBrandInfo;

public interface ArBrandInfoRepository extends JpaRepository<ArBrandInfo, Integer> {
	
	@Query("select cardRangeName,configId from ArBrandInfo ar where ar.arBrandInfoPk.bankId =:bankId")
	List<ArBrandInfo> findAllCardRangeGroup(int bankId);

	@Query("from ArBrandInfo ar where ar.rangeId =:rangeId")
	Optional<ArBrandInfo> findByRangeId(Integer rangeId);
}
