package com.arcot.tm.repository;

import org.springframework.data.repository.CrudRepository;

import com.arcot.tm.entities.ArCountry;

public interface AresCountryRepository extends CrudRepository<ArCountry, Integer> {

}
