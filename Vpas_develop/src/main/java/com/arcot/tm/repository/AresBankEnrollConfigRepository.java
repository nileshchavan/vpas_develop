package com.arcot.tm.repository;

import org.springframework.data.repository.CrudRepository;

import com.arcot.tm.entities.AresBankEnrollingConfig;

public interface AresBankEnrollConfigRepository extends CrudRepository<AresBankEnrollingConfig, Integer> {

}
