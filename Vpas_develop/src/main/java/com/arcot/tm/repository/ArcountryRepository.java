package com.arcot.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.arcot.tm.entities.ArCountry;

public interface ArcountryRepository extends JpaRepository<ArCountry, Integer> {

}
