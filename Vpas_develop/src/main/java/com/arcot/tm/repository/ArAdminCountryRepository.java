package com.arcot.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.arcot.tm.entities.ArAdminCountry;

public interface ArAdminCountryRepository extends JpaRepository<ArAdminCountry, Integer> {

}
