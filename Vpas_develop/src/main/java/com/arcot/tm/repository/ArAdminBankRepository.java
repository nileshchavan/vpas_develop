package com.arcot.tm.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.arcot.tm.entities.ArAdminBank;

public interface ArAdminBankRepository extends JpaRepository<ArAdminBank, Integer> {

	@Query("FROM ArAdminBank a WHERE a.arAdminBankPk.secondaryBankId = :bankId AND a.adminNameEx != :adminName ORDER BY adminName ASC")
	List<ArAdminBank> findAllAdminInfoWithSecondaryAdmin(int bankId,String adminName);
}