package com.arcot.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.arcot.tm.entities.ArBankInfo;

@Repository
public interface ArBankInfoRepository extends JpaRepository<ArBankInfo, Integer> {

	

	@Query("FROM ArBankInfo WHERE bankKey = :bankKey AND bankDirName = :bankDirName")
	ArBankInfo getDetailsBasedOnBankKeyAndBankDirName(String bankKey,String bankDirName);
	
	ArBankInfo findByBankName(String bankName);
}
