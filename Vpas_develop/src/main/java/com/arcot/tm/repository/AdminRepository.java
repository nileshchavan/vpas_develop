package com.arcot.tm.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.arcot.tm.entities.ArESAdmin;

public interface AdminRepository extends JpaRepository<ArESAdmin, Integer> {

	@Query("from ArESAdmin a where a.aresadminpk.name =:name")
	Optional<ArESAdmin> findByUserName(String name);

	ArrayList<ArESAdmin> findByAdminLevel(int level);
	
	@Query("from ArESAdmin a where a.adminLevel =:adminLevel")
	Optional<ArESAdmin> findMasterAdminByLevel(int adminLevel);

	List< ArESAdmin> findBySecondaryBankId(Integer bankId);
}